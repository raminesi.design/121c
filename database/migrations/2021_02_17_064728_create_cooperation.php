<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCooperation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cooperation', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('userId');
            $table->string('avatar');
            $table->string('expertise');
            $table->string('description');
            $table->string('comment')->nullable();
            $table->string('ip');
            $table->timestamp('date')->nullable();
            $table->enum('status' , ['pending' , 'accepted' , 'failed'])->default('pending');
            $table->timestamps();
            $table->foreign('userId')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cooperation');
    }
}
