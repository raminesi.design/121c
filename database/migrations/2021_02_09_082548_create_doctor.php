<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('userId');
            $table->string('avatar')->nullable();
            $table->string('account_number')->nullable();
            $table->string('code');
            $table->text('description');
            $table->integer('countService')->default(0);
            $table->integer('countSuccessful')->default(0);
            $table->integer('countFailed')->default(0);
            $table->double('rate')->default(0);
            $table->enum('status' , ['enable' , 'disable'])->default('enable');
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->foreign('userId')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor');
    }
}
