<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpertise extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expertise', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->double('price_video_min')->default(0);
            $table->double('price_video_max')->default(0);
            $table->double('price_voice_min')->default(0);
            $table->double('price_voice_max')->default(0);
            $table->double('price_text_min')->default(0);
            $table->double('price_text_max')->default(0);
            $table->integer('sort')->default(0);
            $table->enum('status' , ['enable' , 'disable'])->default('enable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expertise');
    }
}
