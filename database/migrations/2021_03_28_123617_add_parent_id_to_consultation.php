<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentIdToConsultation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultation', function (Blueprint $table) {
            $table->unsignedBigInteger('parentId')->nullable();
            $table->unsignedBigInteger('childeId')->nullable();
            $table->foreign('parentId')->references('id')->on('consultation');
            $table->foreign('childeId')->references('id')->on('consultation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultation', function (Blueprint $table) {
            $table->dropColumn('parentId');
            $table->dropColumn('childeId');
        });
    }
}
