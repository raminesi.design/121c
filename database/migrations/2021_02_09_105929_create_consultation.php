<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsultation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultation', function (Blueprint $table) {
            $table->id();
            $table->string('channel');
            $table->unsignedBigInteger('userId');
            $table->unsignedBigInteger('doctorId');
            $table->unsignedBigInteger('expertiseId');
            $table->enum('type' , ['video' , 'voice' , 'text'])->default('video');
            $table->unsignedBigInteger('scheduleId');
            $table->date('date');
            $table->enum('status' , ['created' , 'pending' , 'accepted' , 'doing' , 'done' , 'canceled' , 'rejected' ])->default('created')->nullable();
            $table->float('rate')->nullable();
            $table->text('comment')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
            $table->foreign('userId')->references('id')->on('users');
            $table->foreign('doctorId')->references('id')->on('doctor');
            $table->foreign('expertiseId')->references('id')->on('expertise');
            $table->foreign('scheduleId')->references('id')->on('schedule');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultation');
    }
}
