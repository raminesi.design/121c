<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorExpertise extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_expertise', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('doctorId');
            $table->unsignedBigInteger('expertiseId');
            $table->double('price_video')->default(0);
            $table->double('price_voice')->default(0);
            $table->double('price_text')->default(0);
            $table->enum('status' , ['enable' , 'disable'])->default('enable');
            $table->timestamps();
            $table->foreign('doctorId')->references('id')->on('doctor');
            $table->foreign('expertiseId')->references('id')->on('expertise');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_expertise');
    }
}
