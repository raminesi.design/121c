<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsultationFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultation_file', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('userId');
            $table->unsignedBigInteger('consultationId');
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('file');
            $table->enum('sender' , ['doctor' , 'user'])->default('user');
            $table->timestamps();
            $table->foreign('userId')->references('id')->on('users');
            $table->foreign('consultationId')->references('id')->on('consultation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultation_file');
    }
}
