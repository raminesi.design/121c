<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsultationText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultation_text', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('consultationId');
            $table->unsignedBigInteger('userId');
            $table->text('message');
            $table->enum('status' , ['enable','disable'])->default('enable');
            $table->timestamps();
            $table->foreign('userId')->references('id')->on('users');
            $table->foreign('consultationId')->references('id')->on('consultation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultation_text');
    }
}
