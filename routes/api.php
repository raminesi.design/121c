<?php

use App\Http\Controllers\Api\doctor\v1\BankController as V1BankController;
use App\Http\Controllers\Api\doctor\v1\ConsultationController as V1ConsultationController;
use App\Http\Controllers\Api\doctor\v1\LoginController as V1LoginController;
use App\Http\Controllers\Api\doctor\v1\NotificationsController as V1NotificationsController;
use App\Http\Controllers\Api\doctor\v1\PrescriptionController;
use App\Http\Controllers\Api\doctor\v1\ProfileController;
use App\Http\Controllers\Api\doctor\v1\ScheduleController;
use App\Http\Controllers\Api\doctor\v1\SupportController as V1SupportController;
use App\Http\Controllers\Api\general\v1\ContactController;
use App\Http\Controllers\Api\general\v1\CooperationController;
use App\Http\Controllers\Api\general\v1\DoctorsController;
use App\Http\Controllers\Api\user\v1\BankController;
use App\Http\Controllers\Api\user\v1\ChatController;
use App\Http\Controllers\Api\user\v1\ConsultationController;
use App\Http\Controllers\Api\user\v1\FamilyController;
use App\Http\Controllers\Api\user\v1\InsurancesController;
use App\Http\Controllers\Api\user\v1\LoginController;
use App\Http\Controllers\Api\user\v1\NotificationsController;
use App\Http\Controllers\Api\user\v1\SupportController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('Api')->group(function () {
    Route::namespace('general')->prefix('general')->group(function () {
        Route::namespace('v1')->prefix('v1')->group(function () {
            Route::get('/expertises', [DoctorsController::class, 'expertises']);
            Route::get('/doctors', [DoctorsController::class, 'doctors']);
            Route::post('/contact', [ContactController::class, 'contact']);
            Route::post('/cooperation', [CooperationController::class, 'cooperation']);
        });
    });
});

Route::namespace('Api')->group(function () {
    Route::namespace('user')->prefix('user')->group(function () {
        Route::namespace('v1')->prefix('v1')->group(function () {
            Route::post('/login', [LoginController::class, 'login']);
            Route::get('/countries', [LoginController::class, 'countries']);
            Route::post('/register', [LoginController::class, 'register']);
            Route::middleware('auth:api')->group(function () {
                Route::get('/profile', [LoginController::class, 'profile']);
                Route::post('/profile', [LoginController::class, 'updateUser']);
                Route::post('/password', [LoginController::class, 'password']);

                Route::get('/family', [FamilyController::class, 'family']);
                Route::get('/family/info', [FamilyController::class, 'familyInfo']);
                Route::post('/family', [FamilyController::class, 'add']);
                Route::post('/family/edit', [FamilyController::class, 'edit']);
                Route::delete('/family', [FamilyController::class, 'delete']);

                Route::get('/expertise', [ConsultationController::class, 'expertise']);
                Route::get('/doctors', [ConsultationController::class, 'doctors']);
                Route::get('/schedule', [ConsultationController::class, 'schedule']);
                Route::post('/order', [ConsultationController::class, 'createOrder']);
                Route::get('/order/check', [ConsultationController::class, 'orderCheck']);

                Route::get('/consultations', [ConsultationController::class, 'consultations']);
                Route::get('/consultation', [ConsultationController::class, 'consultation']);
                Route::get('/consultation/cancell', [ConsultationController::class, 'cancellation']);
                Route::post('/upload/file', [ConsultationController::class, 'uploadFile']);

                Route::get('/insurances', [InsurancesController::class, 'insurances']);
                Route::post('/insurance', [InsurancesController::class, 'add']);
                Route::get('/insurance/status', [InsurancesController::class, 'status']);
                Route::delete('/insurance', [InsurancesController::class, 'delete']);

                Route::get('/banks', [BankController::class, 'banks']);
                Route::post('/bank', [BankController::class, 'add']);
                Route::get('/bank/status', [BankController::class, 'status']);
                Route::delete('/bank', [BankController::class, 'delete']);

                Route::get('/messages/list', [SupportController::class, 'tickets']);
                Route::get('/messages', [SupportController::class, 'ticket']);
                Route::post('/message/new', [SupportController::class, 'save']);
                Route::post('/message/comment/save', [SupportController::class, 'saveComment']);

                Route::get('/chat/text', [ChatController::class, 'text']);
                Route::post('/chat/text/message', [ChatController::class, 'sendMessage']);
                Route::get('/live/room', [ChatController::class, 'live']);

                Route::get('/notifications', [NotificationsController::class, 'notifications']);
            });

        });
    });

    Route::namespace('doctor')->prefix('doctor')->group(function () {
        Route::namespace('v1')->prefix('v1')->group(function () {
            Route::post('/login', [V1LoginController::class, 'login']);
            Route::middleware('auth:api')->group(function () {
                Route::get('/profile', [ProfileController::class, 'profile']);
                Route::post('/profile', [ProfileController::class, 'updateUser']);
                Route::post('/password', [LoginController::class, 'password']);

                Route::get('/consultations', [V1ConsultationController::class, 'consultations']);
                Route::get('/consultation', [V1ConsultationController::class, 'consultation']);
                Route::post('/upload/file', [V1ConsultationController::class, 'uploadFile']);

                Route::post('/prescription', [PrescriptionController::class, 'add']);
                Route::delete('/prescription', [PrescriptionController::class, 'delete']);
                Route::delete('/prescription/item', [PrescriptionController::class, 'deleteItem']);

                Route::get('/banks', [V1BankController::class, 'banks']);
                Route::post('/bank', [V1BankController::class, 'add']);
                Route::get('/bank/status', [V1BankController::class, 'status']);
                Route::delete('/bank', [V1BankController::class, 'delete']);

                Route::get('/messages/list', [V1SupportController::class, 'tickets']);
                Route::get('/messages', [V1SupportController::class, 'ticket']);
                Route::post('/message/new', [V1SupportController::class, 'save']);
                Route::post('/message/comment/save', [V1SupportController::class, 'saveComment']);

                Route::get('/notifications', [V1NotificationsController::class, 'notifications']);

                Route::get('/days', [ScheduleController::class, 'days']);
                Route::get('/hours', [ScheduleController::class, 'hours']);
                Route::get('/schedule', [ScheduleController::class, 'schedule']);
                Route::post('/schedule/add', [ScheduleController::class, 'add']);
                Route::post('/schedule/add/allTime', [ScheduleController::class, 'addAllTime']);
                Route::post('/schedule/add/allDay', [ScheduleController::class, 'addAllDay']);
                Route::delete('/schedule/delete', [ScheduleController::class, 'delete']);
                Route::delete('/schedule/delete/allDay', [ScheduleController::class, 'deleteAllDay']);
                Route::delete('/schedule/delete/allTime', [ScheduleController::class, 'deleteAllTime']);

            });
        });
    });
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
