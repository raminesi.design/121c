<?php

use App\Http\Controllers\Dashboard\CheckoutController;
use App\Http\Controllers\Dashboard\ConsultationController;
use App\Http\Controllers\Dashboard\ContactController;
use App\Http\Controllers\Dashboard\CooperationController as DashboardCooperationController;
use App\Http\Controllers\Dashboard\DoctorController;
use App\Http\Controllers\Dashboard\ExpertiseController;
use App\Http\Controllers\Dashboard\HomeController;
use App\Http\Controllers\Dashboard\PackagesController;
use App\Http\Controllers\Dashboard\PermissionController;
use App\Http\Controllers\Dashboard\SettingController;
use App\Http\Controllers\Dashboard\TicketController as DashboardTicketController;
use App\Http\Controllers\Dashboard\UsersController;
use App\Http\Controllers\Front\BankController;
use App\Http\Controllers\Front\ConsultationController as FrontConsultationController;
use App\Http\Controllers\Front\CooperationController;
use App\Http\Controllers\Front\DoctorController as FrontDoctorController;
use App\Http\Controllers\Front\IndexController;
use App\Http\Controllers\Front\InsuranceController;
use App\Http\Controllers\Front\PrescriptionController;
use App\Http\Controllers\Front\TicketController;
use App\Http\Controllers\Front\UserController;
use App\Http\Controllers\Front\WalletController;
use App\Http\Controllers\GatewayController;
use App\Http\Controllers\LiveController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [IndexController::class, 'index'])->name('index');

Route::get('/login', [UserController::class, 'login'])->name('login_form');
Route::get('/logOut', [UserController::class, 'logOut'])->name('logOut');
Route::get('/register', [UserController::class, 'register'])->name('register_form');
Route::post('/login', [UserController::class, 'loginUser'])->name('login');
Route::post('/register', [UserController::class, 'registerUser'])->name('register');
Route::get('/forget/password', [UserController::class, 'forgetPassword'])->name('forget');
Route::post('/forget/password', [UserController::class, 'sendRecoveryLink'])->name('sendRecoveryLink');
Route::get('/login/token/{token}', [UserController::class, 'loginWithToken'])->name('loginWithToken');
Route::get('/cooperation', [CooperationController::class, 'index'])->name('cooperation');
Route::post('/cooperation', [CooperationController::class, 'save'])->name('cooperation_save');
Route::post('/contact', [IndexController::class, 'contact'])->name('contact');

Route::get('/transaction', [GatewayController::class, 'transaction'])->name('transaction');
Route::get('/transaction/cancel', [GatewayController::class, 'cancel'])->name('transaction_cancel');
Route::post('/transaction', [GatewayController::class, 'stripePost'])->name('transaction_post');
Route::get('/transaction/successful', [GatewayController::class, 'successful'])->name('transaction_successful');

Route::get('/live/call', [LiveController::class, 'call'])->name('live_call');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::middleware(['checkRoleUser'])->group(function () {
        Route::get('/doctor/profile', [FrontDoctorController::class, 'profile'])->name('doctorProfile');
        Route::get('/doctor/settings', [FrontDoctorController::class, 'settings'])->name('doctorProfile_settings');
        Route::get('/doctor/schedule', [FrontDoctorController::class, 'schedule'])->name('doctorProfile_schedule');
        Route::get('/doctor/expertise', [FrontDoctorController::class, 'expertise'])->name('doctorProfile_expertise');
        Route::post('/doctor/expertise/save', [FrontDoctorController::class, 'expertiseSave'])->name('doctorProfile_expertise_save');
        Route::post('/doctor/time', [FrontDoctorController::class, 'doctorTime'])->name('doctorProfile_time');
        Route::post('/doctor/allTime', [FrontDoctorController::class, 'doctorAllTime'])->name('doctorProfile_all_time');
        Route::post('/doctor/allDay', [FrontDoctorController::class, 'doctorAllDay'])->name('doctorProfile_all_day');
        Route::get('/doctor/password', [FrontDoctorController::class, 'password'])->name('doctorProfile_password');
        Route::get('/doctor/checkout', [FrontDoctorController::class, 'checkout'])->name('doctorProfile_checkout');
        Route::get('/doctor/financial/report', [WalletController::class, 'report'])->name('doctorProfile_financial_report');
        Route::get('/doctor/financial/report/checkout', [WalletController::class, 'reportCheckouts'])->name('doctorProfile_financial_report_checkouts');
        Route::get('/doctor/financial/report/orders', [WalletController::class, 'reportOrders'])->name('doctorProfile_financial_report_orders');
        Route::post('/doctor/password/update', [FrontDoctorController::class, 'changePassword'])->name('doctorProfile_password_update');
        Route::get('/doctor/profile/notifications', [FrontDoctorController::class, 'notifications'])->name('doctorProfile_notifications');
        Route::post('/doctor/settings/update', [FrontDoctorController::class, 'updateUser'])->name('doctorProfile_settings_update');
        Route::get('/doctor/profile/consultation', [FrontDoctorController::class, 'consultation'])->name('doctorProfile_consultation_info');
        Route::get('/doctor/profile/consultation/accept', [FrontDoctorController::class, 'consultationAccept'])->name('doctorProfile_consultation_accept');
        Route::post('/doctor/profile/consultation/reject', [FrontDoctorController::class, 'consultationReject'])->name('doctorProfile_consultation_reject');
        Route::get('/doctor/profile/consultation/text', [FrontDoctorController::class, 'consultationText'])->name('doctorProfile_consultationText');
        Route::get('/doctor/profile/startCall', [FrontDoctorController::class, 'startCall'])->name('doctorProfile_startCall');
        Route::get('/doctor/profile/call', [FrontDoctorController::class, 'call'])->name('doctorProfile_call');
        Route::post('/doctor/profile/consultation/upload', [FrontDoctorController::class, 'uploadFile'])->name('doctorProfile_consultation_upload');
        Route::get('/doctor/consultation/nextSession', [FrontDoctorController::class, 'consultationNextSession'])->name('doctorProfile_consultation_nextSession');
        Route::post('/doctor/consultation/nextSession', [FrontDoctorController::class, 'consultationNextSessionSave'])->name('doctorProfile_consultation_nextSession_save');
        Route::get('/doctor/prescription', [PrescriptionController::class, 'prescriptionDoctor'])->name('doctorProfile_prescription');
        Route::get('/doctor/prescription/add', [PrescriptionController::class, 'add'])->name('doctorProfile_prescription_add');
        Route::post('/doctor/prescription/save', [PrescriptionController::class, 'save'])->name('doctorProfile_prescription_save');
        Route::post('/doctor/prescription/searchAjax', [PrescriptionController::class, 'searchAjax'])->name('doctorProfile_prescription_searchAjax');
        Route::get('/doctor/prescription/delete', [PrescriptionController::class, 'delete'])->name('doctorProfile_prescription_delete');
        Route::get('/doctor/prescription/item/delete', [PrescriptionController::class, 'deleteItem'])->name('doctorProfile_prescription_deleteItem');
        Route::post('/doctor/activity/status', [FrontDoctorController::class, 'activityStatus'])->name('doctorProfile_activity_status');


        Route::get('/profile', [UserController::class, 'profile'])->name('profile');
        Route::get('/profile/family', [UserController::class, 'children'])->name('profile_children');
        Route::get('/profile/family/add', [UserController::class, 'childrenAdd'])->name('profile_children_add');
        Route::get('/profile/family/edit', [UserController::class, 'childrenEdit'])->name('profile_children_edit');
        Route::get('/profile/family/status', [UserController::class, 'childrenStatus'])->name('profile_children_status');
        Route::get('/profile/family/delete', [UserController::class, 'childrenDelete'])->name('profile_children_delete');
        Route::post('/profile/family/save', [UserController::class, 'childrenSave'])->name('profile_children_save');
        Route::post('/profile/family/update/{id}', [UserController::class, 'updateChildren'])->name('profile_children_update');
        Route::get('/profile/notifications', [UserController::class, 'notifications'])->name('profile_notifications');
        Route::get('/profile/settings', [UserController::class, 'settings'])->name('profile_settings');
        Route::get('/profile/password', [UserController::class, 'password'])->name('profile_password');
        Route::post('/profile/password/save', [UserController::class, 'changePassword'])->name('profile_password_save');
        Route::post('/profile/settings/save', [UserController::class, 'updateUser'])->name('profile_settings_save');
        Route::get('/profile/consultation', [UserController::class, 'consultation'])->name('profile_consultation_info');
        Route::get('/profile/consultation/text', [UserController::class, 'consultationText'])->name('profile_consultationText');
        Route::post('/profile/consultation/upload', [UserController::class, 'uploadFile'])->name('profile_consultation_upload');
        Route::get('/profile/startCall', [UserController::class, 'startCall'])->name('profile_startCall');
        Route::get('/profile/call', [UserController::class, 'call'])->name('profile_call');
        Route::post('/doctor/profile/consultation/cancellation', [UserController::class, 'consultationCancellation'])->name('profile_consultation_cancellation');

        Route::get('/profile/tickets', [TicketController::class, 'tickets'])->name('profile_tickets');
        Route::get('/profile/ticket', [TicketController::class, 'ticket'])->name('profile_ticket');
        Route::get('/profile/ticket/add', [TicketController::class, 'add'])->name('profile_ticket_add');
        Route::post('/profile/ticket/save', [TicketController::class, 'save'])->name('profile_ticket_save');
        Route::post('/profile/ticket/comment/save', [TicketController::class, 'saveComment'])->name('profile_ticket_comment_save');


        Route::get('/profile/wallet', [WalletController::class, 'index'])->name('profile_wallet');
        Route::post('/profile/wallet', [WalletController::class, 'charge'])->name('profile_wallet_charge');
        Route::post('/profile/checkout/new', [WalletController::class, 'checkoutNew'])->name('profile_checkout_new');

        Route::get('/consultation/expertise', [FrontConsultationController::class, 'expertise'])->name('consultation_expertise');
        Route::get('/consultation/doctors', [FrontConsultationController::class, 'doctors'])->name('consultation_doctors');
        Route::get('/consultation/schedule', [FrontConsultationController::class, 'schedule'])->name('consultation_schedule');
        Route::post('/consultation/schedule', [FrontConsultationController::class, 'saveSchedule'])->name('consultation_schedule_save');
        Route::get('/consultation/order', [FrontConsultationController::class, 'order'])->name('consultation_order');
        Route::post('/consultation/order/wallet', [FrontConsultationController::class, 'walletUsed'])->name('consultation_order_wallet_used');
        Route::get('/consultation/endCall', [FrontConsultationController::class, 'endCall'])->name('consultation_endCall');
        Route::get('/consultation/report', [FrontConsultationController::class, 'report'])->name('consultation_report');
        Route::post('/consultation/report/save', [FrontConsultationController::class, 'reportSave'])->name('consultation_report_save');

        Route::get('/profile/insurances', [InsuranceController::class, 'Insurances'])->name('profile_insurances');
        Route::get('/profile/insurance/add', [InsuranceController::class, 'add'])->name('profile_insurance_add');
        Route::post('/profile/insurance/save', [InsuranceController::class, 'save'])->name('profile_insurance_save');
        Route::get('/profile/insurance/status', [InsuranceController::class, 'status'])->name('profile_insurance_status');
        Route::get('/profile/insurance/delete', [InsuranceController::class, 'delete'])->name('profile_insurance_delete');

        Route::post('/profile/consultation/text/message', [FrontConsultationController::class, 'sendMessage'])->name('consultation_send_message');

        Route::get('/profile/prescription', [PrescriptionController::class, 'prescription'])->name('profile_prescription');

        Route::get('/profile/banks', [BankController::class, 'Banks'])->name('profile_banks');
        Route::get('/profile/bank/add', [BankController::class, 'add'])->name('profile_bank_add');
        Route::post('/profile/bank/save', [BankController::class, 'save'])->name('profile_bank_save');
        Route::get('/profile/bank/status', [BankController::class, 'status'])->name('profile_bank_status');
        Route::get('/profile/bank/delete', [BankController::class, 'delete'])->name('profile_bank_delete');

    });
});

Route::middleware(['auth:sanctum', 'verified'])->prefix('dashboard')->group(function () {
    Route::group(['middleware' => ['permission:admin']], function() {
        Route::get('/', [HomeController::class, 'index'])->name('dashboard');

        Route::get('/doctors', [DoctorController::class, 'doctors'])->name('dashboard_doctors');
        Route::get('/doctor/add', [DoctorController::class, 'doctorAdd'])->name('dashboard_doctor_add');
        Route::post('/doctor/save', [DoctorController::class, 'doctorSave'])->name('dashboard_doctor_save');
        Route::get('/doctor', [DoctorController::class, 'doctor'])->name('dashboard_doctor');
        Route::get('/doctor/status', [DoctorController::class, 'status'])->name('dashboard_doctor_status');
        Route::post('/doctor/update/{id}', [DoctorController::class, 'editInfo'])->name('dashboard_doctor_update');
        Route::post('/doctor/expertise', [DoctorController::class, 'doctorExpertise'])->name('dashboard_doctor_expertise');
        Route::post('/doctor/time', [DoctorController::class, 'doctorTime'])->name('dashboard_doctor_time');
        Route::post('/doctor/allTime', [DoctorController::class, 'doctorAllTime'])->name('dashboard_doctor_all_time');
        Route::post('/doctor/allDay', [DoctorController::class, 'doctorAllDay'])->name('dashboard_doctor_all_day');
        Route::post('/doctor/password/{id}', [DoctorController::class, 'changePassword'])->name('dashboard_doctor_password');
        Route::post('/doctor/account_number/{id}', [DoctorController::class, 'accountNumber'])->name('dashboard_doctor_account_number');
        Route::get('/doctor/export', [DoctorController::class, 'export'])->name('dashboard_doctor_export');

        Route::get('/expertise', [ExpertiseController::class, 'expertise'])->name('dashboard_expertise');
        Route::get('/expertise/add', [ExpertiseController::class, 'add'])->name('dashboard_expertise_add');
        Route::get('/expertise/edit', [ExpertiseController::class, 'edit'])->name('dashboard_expertise_edit');
        Route::get('/expertise/status', [ExpertiseController::class, 'status'])->name('dashboard_expertise_status');
        Route::post('/expertise/sort', [ExpertiseController::class, 'sort'])->name('dashboard_expertise_sort');
        Route::post('/expertise/save', [ExpertiseController::class, 'save'])->name('dashboard_expertise_save');
        Route::post('/expertise/update/{id}', [ExpertiseController::class, 'update'])->name('dashboard_expertise_update');

        Route::get('/consultation', [ConsultationController::class, 'index'])->name('dashboard_consultation');
        Route::get('/consultation/information', [ConsultationController::class, 'consultation'])->name('dashboard_consultation_info');
        Route::get('/consultation/export', [ConsultationController::class, 'export'])->name('dashboard_consultation_export');

        Route::get('/packages', [PackagesController::class, 'packages'])->name('dashboard_packages');


        Route::get('/users', [UsersController::class, 'index'])->name('dashboard_users');
        Route::get('/user', [UsersController::class, 'user'])->name('dashboard_user');
        Route::get('/user/add', [UsersController::class, 'userAdd'])->name('dashboard_user_add');
        Route::post('/user/save', [UsersController::class, 'saveUser'])->name('dashboard_user_save');
        Route::post('/user/update/{id}', [UsersController::class, 'updateUser'])->name('dashboard_user_update');
        Route::get('/user/status', [UsersController::class, 'status'])->name('dashboard_user_status');
        Route::get('/user/delete', [UsersController::class, 'deleteUser'])->name('dashboard_user_delete');
        Route::get('/children/delete', [UsersController::class, 'delete'])->name('dashboard_children_delete');
        Route::post('/user/password/{id}', [UsersController::class, 'changePassword'])->name('dashboard_user_password');
        Route::post('/user/searchAjax', [UsersController::class, 'searchAjax'])->name('dashboard_searchAjax');
        Route::get('/user/export', [UsersController::class, 'export'])->name('dashboard_users_export');

        Route::get('/cooperation', [DashboardCooperationController::class, 'index'])->name('dashboard_cooperation');
        Route::get('/cooperation/info', [DashboardCooperationController::class, 'cooperation'])->name('dashboard_cooperation_info');
        Route::post('/cooperation/info', [DashboardCooperationController::class, 'cooperation_update'])->name('dashboard_cooperation_update');

        Route::get('/setting/information', [SettingController::class, 'information'])->name('dashboard_settings_information');
        Route::post('/setting/information', [SettingController::class, 'infoSave'])->name('dashboard_settings_information_save');

        Route::get('/setting/password', [SettingController::class, 'password'])->name('dashboard_settings_password');
        Route::post('/setting/password', [SettingController::class, 'changePassword'])->name('dashboard_settings_password_update');

        Route::get('/contact/messages', [ContactController::class, 'messages'])->name('dashboard_contact_messages');
        Route::get('/contact/notifications', [ContactController::class, 'notifications'])->name('dashboard_contact_notifications');
        Route::post('/contact/notification', [ContactController::class, 'sesndNotif'])->name('dashboard_contact_notification_send');

        Route::get('/tickets', [DashboardTicketController::class, 'tickets'])->name('dashboard_tickets');
        Route::get('/ticket', [DashboardTicketController::class, 'ticket'])->name('dashboard_ticket');
        Route::get('/ticket/close', [DashboardTicketController::class, 'close'])->name('dashboard_ticket_close');
        Route::post('/ticket', [DashboardTicketController::class, 'saveComment'])->name('dashboard_ticket_comment_save');

        Route::get('/checkout/list', [CheckoutController::class, 'index'])->name('dashboard_checkout_list');
        Route::get('/checkout', [CheckoutController::class, 'checkout'])->name('dashboard_checkout');
        Route::post('/checkout/status', [CheckoutController::class, 'status'])->name('dashboard_checkout_status');

        Route::group(['middleware' => ['permission:Permission']], function() {
            Route::get('/Permission/createRole', [PermissionController::class, 'createRole'])->name('dashboard_Permission_createRole');
            Route::get('/Permission/createPermission', [PermissionController::class, 'createPermission'])->name('dashboard_Permission_createPermission');
            Route::get('/Permission/permissionToRole', [PermissionController::class, 'permissionToRole'])->name('dashboard_Permission_permissionToRole');
            Route::get('/Permission/RoleToUser', [PermissionController::class, 'RoleToUser'])->name('dashboard_Permission_RoleToUser');
        });
    });
});

