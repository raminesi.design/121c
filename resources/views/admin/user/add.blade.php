@extends('layouts.admin')

@section('title')
    Add user
@endsection

@section('header')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <style>
        .span-error{
            color: #F00;
            font-size: 10px;
        }
    </style>
@endsection

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form method="post" action="{{ route('dashboard_user_save') }}" class="form-horizontal">
            @csrf
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title float-right">Add user</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>First name*</label>
                        <input type="text" name="first_name" class="form-control @if ($errors->has('first_name')) is-invalid @endif" id="first_name" placeholder="First name" value="{{ old('first_name') }}">
                        @if ($errors->has('first_name'))
                            <span class="span-error">{{ $errors->first('first_name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Last name*</label>
                        <input type="text" name="last_name" class="form-control @if ($errors->has('last_name')) is-invalid @endif" id="last_name" placeholder="Last name" value="{{ old('last_name') }}">
                        @if ($errors->has('last_name'))
                            <span class="span-error">{{ $errors->first('last_name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="icheck-primary d-inline">
                            <input type="radio" id="radioPrimary2" name="gender" value="male" checked>
                            <label for="radioPrimary2"> Male
                            </label>
                          </div>
                        <div class="icheck-primary d-inline">
                            <input type="radio" id="radioPrimary1" name="gender" value="female">
                            <label for="radioPrimary1"> Female
                            </label>
                          </div>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Birthdate</label>
                        <input type="date" name="birthdate" autocomplete="off" class="form-control @if ($errors->has('birthdate')) is-invalid @endif">
                        @if ($errors->has('birthdate'))
                            <span class="span-error">{{ $errors->first('birthdate') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Email*</label>
                        <input type="text" name="email" class="form-control @if ($errors->has('email')) is-invalid @endif" id="email" placeholder="Email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                                <span class="span-error">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Country</label>
                        <select name="country" class="form-control">
                            @foreach($country as $key => $value)
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-info float-right">Submit</button>
                    </div>
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </form>
    </div>
</section>
<!-- /.content -->

@endsection


@section('footer')


@endsection

@section('pageScript')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
@endsection
