@extends('layouts.admin')

@section('title')
    {{ $user->name }}
@endsection

@section('header')
    <link rel="stylesheet" href="{{ asset('css/croppie.css') }}" media="all">
    <style>
        #pac-input:focus {
            border: 1px solid #193e72;
        }
        .croppie-container {
            height: auto;
            margin: 10px auto;
            border: 1px solid #fff;
        }
        .croppie-container .cr-boundary {
            padding: 10px;
        }
        .cr-image {
            font-size: 0;
        }
        .cr-slider-wrap {
            display: none;
        }
        #upload-image {
            display: none;
        }
        #Div-image {
            text-align: center;
            overflow: hidden;
        }
        #style_switcher a {
            color: #193e72;
        }

        .uk-file-upload {
            padding: 0;
        }
        .box-max-200 {
            max-width: 120px;
        }
        .slider-image {
            max-width: 235px;
            margin: 0 auto;
            border: 1px solid #000000;
        }
        .slider-image img {
            max-width: 100%;
            height: auto;
            display: block;
            margin: 0 auto;
            object-fit: cover;
        }
        .cr-boundary{
            border-radius: 50%;
        }
        .card{
            direction: rtl;
            text-align: right;
        }
        #Select_photo_croper{
            color: #193e72;
            font-size: 40px;
            cursor: pointer;
        }
        .span-error{
            color: #F00;
            font-size: 10px;
        }
        .pointer {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
              <div class="col-md-3">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                  <div class="card-body box-profile">
                    <div class="text-center">
                        <img src="{{ url('media/user/'.$user->avatar) }}" class="img-circle elevation-2 avatar">
                    </div>

                    <h3 class="profile-username text-center">{{  $user->name  }}</h3>

                    <p class="text-muted text-center">{{  $user->code  }}</p>
                    <p class="text-muted text-center">{{  $user->email  }}</p>

                    <p class="text-muted text-center">{{  number_format($wallet->credit)  }} $</p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                          <b> All consultation </b> <a class="float-right"> {{ $allRequest }} </a>
                        </li>
                        <li class="list-group-item">
                          <b> done </b> <a class="float-right">{{ $doneRequest }}</a>
                        </li>
                        <li class="list-group-item">
                          <b> rejected </b> <a class="float-right">{{ $rejectRequest }}</a>
                        </li>
                        <li class="list-group-item">
                          <b> canceled </b> <a class="float-right">{{ $cancelRequest }}</a>
                        </li>
                        <li class="list-group-item">
                          <b> pending </b> <a class="float-right">{{ $pendingRequest }}</a>
                        </li>
                      </ul>

                    <a href="{{ route('dashboard_consultation') }}?user={{ $user->id }}" class="btn btn-primary btn-block"><b>Consultation</b></a>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
              <div class="col-md-9">
                <div class="card">

                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Information</a></li>
                        <li class="nav-item"><a class="nav-link" href="#Children" data-toggle="tab">Children</a></li>
                        <li class="nav-item"><a class="nav-link" href="#changePassword" data-toggle="tab">Change password</a></li>
                    </ul>
                </div><!-- /.card-header -->

                  <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="settings">
                            <form method="post" action="{{route('dashboard_user_update' , [$user->id])}}" class="form-horizontal">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div id="Div-image">
                                            <i class="nav-icon fas fa-plus" id="Select_photo_croper"></i>
                                            <div id="upload-demo" class="max-width-300"></div>
                                            <input type="hidden" id="image-code" name="image" value="">
                                            <input class="Upload_Croper" type="file" id="upload-image">
                                            <div class="clearfix"><br></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <label for="inputName" class="col-sm-2 col-form-label">First name*</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="first_name" class="form-control" id="first_name" value="@if(old('first_name')){{ old('first_name') }}@else{{ $user->first_name }}@endif" placeholder="First name">
                                        @if ($errors->has('first_name'))
                                            <span class="span-error">{{ $errors->first('first_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">Last name*</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="last_name" class="form-control" id="last_name" value="@if(old('last_name')){{ old('last_name') }}@else{{ $user->last_name }}@endif" placeholder="Last name">
                                        @if ($errors->has('last_name'))
                                            <span class="span-error">{{ $errors->first('birthdate') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Birthdate</label>
                                    <div class="col-sm-10">
                                        <input type="date" name="birthdate" autocomplete="off" value="{{ $user->birthdate }}" class="form-control">
                                        @if ($errors->has('birthdate'))
                                            <span class="span-error">{{ $errors->first('birthdate') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">Gender</label>
                                    <div class="col-sm-10">
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="radioPrimary2" name="gender" value="male" checked>
                                            <label for="radioPrimary2"> Male
                                            </label>
                                        </div>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="radioPrimary1" @if($user->gender == 'female') checked @endif name="gender" value="female">
                                            <label for="radioPrimary1"> Female
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Email*</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="email" class="form-control @if ($errors->has('email')) is-invalid @endif" id="email" placeholder="Email" value="@if(old('email')){{ old('email') }}@else{{ $user->email }}@endif">
                                        @if ($errors->has('email'))
                                                <span class="span-error">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2 col-form-labe"></div>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-success">submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="Children">
                            <table id="DataTable" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Name</th>
                                  <th>Avatar</th>
                                  <th>Gender</th>
                                  <th>Mobile</th>
                                  <th>Wallet</th>
                                  <th>Tools</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ( $children as $childe)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{  $childe->name  }}</td>
                                            <td>
                                                @if(is_null($childe->avatar))
                                                    <img src="{{ url('media/user/avatar.png') }}" class="img-circle elevation-2 avatar">
                                                @else
                                                    <img src="{{ url('media/user/'.$childe->avatar) }}" class="img-circle elevation-2 avatar">
                                                @endif
                                            </td>
                                            <td>@if($childe->gender == 'male') {{ Lang::get('messages.male') }} @else {{ Lang::get('messages.female') }} @endif</td>
                                            <td>{{ $childe->mobile }}</td>
                                            <td>{{ number_format($childe->credit) }}</td>
                                            <td>
                                                <a href="{{ route('dashboard_user') }}?id={{ $childe->id }}">
                                                    <i class="fas fa-info-circle tools-btn edit-btn" title="Information"></i>
                                                </a>
                                                <a href="{{ route('dashboard_user_status').'?id='.$childe->id }}">
                                                    @if($childe->status == 'enable')
                                                        <i class="fas fa-check tools-btn status-enable" title="Enable"></i>
                                                    @else
                                                        <i class="fas fa-times tools-btn status-disable" title="Disable"></i>
                                                    @endif
                                                </a>
                                                <a href="{{ route('dashboard_children_delete').'?id='.$childe->parent }}">
                                                    <i class="far fa-trash-alt tools-btn delete-btn" title="Delete"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="changePassword">
                            <form method="post" action="{{ route('dashboard_user_password' , [$user->id]) }}" class="form-horizontal">
                                @csrf
                                <div class="form-group row">
                                    <label>Password* <span id="randomPassword">Random <i class="fas fa-key"></i></span></label>
                                    <input type="text" name="password" class="form-control" id="password" placeholder="Password">
                                    @if ($errors->has('password'))
                                        <span class="span-error">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </form>
                          </div>
                    </div>
                    <!-- /.tab-content -->
                  </div><!-- /.card-body -->
                </div>
                <!-- /.nav-tabs-custom -->
              </div>
            </div>
            <!-- /.row -->
          </div><!-- /.container-fluid -->
    </section>
@endsection


@section('footer')

@endsection

@section('pageScript')
<script src="{{ asset('js/croppie.js') }}"></script>
<script type="text/javascript">
    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 200,
            height: 200,
            type: 'circle'
        },
        boundary: {
            width: 200,
            height: 200
        }
    });

    $('#upload-image').on('change', function () {
        var Id = $(this).parent().attr('id');
        $('#'+Id+' .cr-viewport').css('background-image' , 'none');
        var reader = new FileReader();
        reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function(){
                GetCode();
            });
        }
        reader.readAsDataURL(this.files[0]);
    });

    function GetCode() {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: {
                width: 500,
                height: 500
            },
            format: "jpeg",
            quality: '0.7'
        }).then(function (resp) {
            $('#image-code').val(resp);
        });
    }

    $('#Select_photo_croper').click(function () {
        $('#upload-image').trigger('click');
    });

    $('.remove_photo_croper').on('click', function() {
        $('.cr-viewport').css('background-image' , 'none');
        $('.cr-image').attr('src', '');
        $('#upload-image').val('');
        $('#image-code').val('delete');
    });

    $('.cr-overlay').mouseup(function () {
        GetCode();
    });

    $('.cr-overlay').bind('mousewheel', function(e){
        GetCode();
    });

    setTimeout(function () {
        $('.cr-image').attr('src', '{{ url('media/user/'.$user->avatar) }}').css({'width': '200' , 'height': '200'});
    } , 300);

    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@$';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
     }

     $('#randomPassword').click(function(){
         var password = makeid(8);
         $('#password').val(password);
     });

    @if ($errors->has('password'))
        activaTab('changePassword');
     @endif

     function activaTab(tab){
        $('.nav-item a[href="#' + tab + '"]').tab('show');
    };
</script>
@endsection
