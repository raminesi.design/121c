@extends('layouts.admin')

@section('title')
    {{ $user->name }}
@endsection

@section('header')

@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{ url('media/user/'.$user->avatar) }}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{  $user->name  }}</h3>
                @if(!is_null($user->password))
                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Email: </b><a class="float-right">{{  $user->email  }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Password: </b><a class="float-right">{{ $user->password }}</a>
                        </li>
                    </ul>
                @endif
                <a href="{{ route('dashboard_user') }}?id={{ $user->id }}" class="btn btn-primary btn-block"><b>Information</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
    </div>
</section>
@endsection


@section('footer')


@endsection

@section('pageScript')

@endsection
