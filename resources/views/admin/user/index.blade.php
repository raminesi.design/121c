@extends('layouts.admin')

@section('title')
 Users
@endsection

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Users</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <form method="get">
                            <div class="form-group">
                            <label>@lang('page.Searching')</label>
                            <div class="row">
                                <input name="search" class="form-control col-8" value="{{ $request->search }}">
                                <button type="submit" class="btn btn-info col-3"><i class="nav-icon fas fa-search"></i></button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
              <table id="DataTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Avatar</th>
                  <th>Gender</th>
                  <th>Mobile</th>
                  <th>Wallet</th>
                  <th>Tools</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ( $users as $user)
                        <tr>
                            <td>{{$loop->iteration + $users->firstItem() - 1}}</td>
                            <td>{{  $user->name  }}</td>
                            <td>
                                <img src="{{ url('media/user/'.$user->avatar) }}" class="img-circle elevation-2 avatar">
                            </td>
                            <td>@if($user->gender == 'male') {{ Lang::get('messages.male') }} @else {{ Lang::get('messages.female') }} @endif</td>
                            <td>{{ $user->mobile }}</td>
                            <td>{{ number_format($user->credit) }}</td>
                            <td>
                                <a href="{{ route('dashboard_user') }}?id={{ $user->id }}">
                                    <i class="fas fa-info-circle tools-btn edit-btn" title="Information"></i>
                                </a>
                                <a href="{{ route('dashboard_user_status').'?id='.$user->id }}">
                                    @if($user->status == 'enable')
                                        <i class="fas fa-check tools-btn status-enable" title="Enable"></i>
                                    @else
                                        <i class="fas fa-times tools-btn status-disable" title="Disable"></i>
                                    @endif
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <div class="table-buttons row col-12">
                <div class="col-12">
                    {{ $users->appends(request()->except('page'))->links() }}
                </div>
                <div class="col-12">
                    <a href="{{ route('dashboard_users_export') }}"><button class="btn btn-success float-right">Export <i class="fas fa-download"></i></button></a>
                </div>
                <div class="col-12"><br/></div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
@endsection


@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<!-- Bootstrap Switch -->
<script src="{{ asset('admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
@endsection

@section('pageScript')
<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });

    });
  </script>
@endsection
