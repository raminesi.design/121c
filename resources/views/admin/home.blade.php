@extends('layouts.admin')

@section('title')
    Home page
@endsection

@section('header')
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/jqvmap/jqvmap.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
  <!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <style>
      .list-view{
          font-size: 25px;
          color: #28a745;
      }
      .info-box{
          color: #1f1f1f !important;
      }
  </style>
@endsection

@section('content')
<!-- Main content -->
  <section class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="{{ route('dashboard_users') }}">
                    <div class="info-box">
                        <span class="info-box-icon bg-blue"><i class="fas fa-users"></i></span>
                        <div class="info-box-content">
                        <span class="info-box-text">Users</span>
                        <span class="info-box-number">{{ number_format($data['allUsers']) }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a>
              <!-- /.info-box -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="{{ route('dashboard_users') }}">
                    <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fas fa-user-plus"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Registered today</span>
                        <span class="info-box-number">{{ number_format($data['todayUsers']) }}</span>
                    </div>
                    <!-- /.info-box-content -->
                    </div>
                </a>
                <!-- /.info-box -->
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="{{ route('dashboard_doctors') }}">
                    <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fas fa-user-md"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Doctors</span>
                        <span class="info-box-number">{{ number_format($data['allDoctors']) }}</span>
                    </div>
                    <!-- /.info-box-content -->
                    </div>
                </a>
                <!-- /.info-box -->
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="{{ route('dashboard_consultation') }}">
                    <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fas fa-headset"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Consultations</span>
                        <span class="info-box-number">{{ number_format($data['allConsultation']) }}</span>
                    </div>
                    <!-- /.info-box-content -->
                    </div>
                </a>
                <!-- /.info-box -->
              </div>
        </div>
        <label>Upcoming requests</label>
        <table id="DataTable" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>Doctor</th>
              <th>User</th>
              <th>Date & Time</th>
              <th>Status</th>
              <th>View</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($data['consultation'] as $consultation)
                    <tr>
                        <td>
                            <a href="{{ route('dashboard_doctor').'?id='.$consultation->doctor->id }}">
                                <img src="{{ url('media/doctor/'.$consultation->doctor->avatar) }}" class="img-circle elevation-2 avatar"><br/>
                            </a>
                            {{ $consultation->doctor->user->name }}<br/>
                            {{ $consultation->doctor->code }}<br/>
                        </td>
                        <td>
                            <a href="{{ route('dashboard_user').'?id='.$consultation->user->id }}">
                                <img src="{{ url('media/user/'.$consultation->user->avatar) }}" class="img-circle elevation-2 avatar"><br/>
                            </a>
                            {{ $consultation->user->name }}<br/>
                            {{ $consultation->user->code }}<br/>
                        </td>
                        <td>{{ $consultation->date }}<br/>{{ $consultation->day->title }}<br/>{{ $consultation->visitStart }}</td>
                        <td>{{ $consultation->status }}</td>
                        <td>
                            <a href="{{ route('dashboard_consultation_info').'?id='.$consultation->id }}">
                                <span class="list-view"><i class="fas fa-clipboard-list"></i></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
  </section>
@endsection

@section('footer')
<!-- ChartJS -->
<script src="{{ asset('admin/plugins/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('admin/plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('admin/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('admin/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('admin/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });

    });
</script>
@endsection
