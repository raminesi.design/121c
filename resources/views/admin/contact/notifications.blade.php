@extends('layouts.admin')

@section('title')
Notifications
@endsection

@section('header')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Notifications</h3>
            </div>


            <div class="container-fluid">
                <form method="post" action="{{ route('dashboard_contact_notification_send') }}" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <!-- SELECT2 EXAMPLE -->
                    <div class="card card-default">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>User group</label>
                                <select name="group" id="group" class="form-control">
                                    <option value="user">All users</option>
                                    <option value="doctor">All doctors</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Search</label>
                                <input class="form-control @if ($errors->has('uaserId')) is-invalid @endif" id="user" type="text" name="user" value="{{ old('user') }}">
                                <input id="hiddenUser" type="hidden" name="uaserId" value="{{ old('uaserId') }}">
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Title*</label>
                                <input type="text" name="title" class="form-control @if ($errors->has('title')) is-invalid @endif" id="title" placeholder="Title" value="{{ old('title') }}">
                                @if ($errors->has('title'))
                                    <span class="span-error">Please enter a title</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Message*</label>
                                <textarea name="message" class="form-control @if ($errors->has('message')) is-invalid @endif" placeholder="Message">{{ old('message') }}</textarea>
                                @if ($errors->has('message'))
                                    <span class="span-error">Please enter a message</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info float-left">Send</button>
                            </div>
                        </div>
                        <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </form>
            </div>



            <!-- /.card-header -->
            <div class="card-body">
              <table id="DataTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Title</th>
                  <th>Message</th>
                  <th>Date</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ( $notifications as $notification)
                        <tr>
                            <td>{{  $loop->iteration + $notifications->firstItem() - 1}}</td>
                            <td>
                            @if ($notification->user)
                                {{  $notification->user->name  }}
                            @elseif($notification->group == 'user')
                                All users
                            @else
                                All doctors
                            @endif
                            </td>
                            <td>{{  $notification->title  }}</td>
                            <td>{{  $notification->description  }}</td>
                            <td>{{  $notification->created_at  }}</td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <div class="table-buttons">
                <div>
                    {{ $notifications->appends(request()->except('page'))->links() }}
                </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
@endsection


@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('pageScript')
    <script type="text/javascript">

        $(function () {

            $('#DataTable').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": false,
                "responsive": true,
            });

            // Single Select
            $("#user").autocomplete({
                source: function (request, response) {
                    console.log(request);
                    var url     = "{{route('dashboard_searchAjax')}}";
                    // Fetch data
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: "json",
                        headers: {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')},
                        data: {
                            _token: "{{ csrf_token()}}",
                            search: request.term
                        },
                        success: function (data) {
                            response(data);
                            console.log(data);
                            if(data.length == 0){
                                $('#error-empty').show();
                            }else{
                                $('#error-empty').hide();
                            }
                        }
                    });
                },
                select: function (event, ui) {
                    // Set selection
                    $('#user').val(ui.item.label); // display the selected text
                    $('#hiddenUser').val(ui.item.value); // display the selected text
                    return false;
                }
            });

        });

    </script>
@endsection
