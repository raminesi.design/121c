@extends('layouts.admin')

@section('title')
consultation
@endsection

@section('header')
<link rel="stylesheet" href="{{ asset('admin/plugins/ekko-lightbox/ekko-lightbox.css') }}">
<style>
    .avatar{
        width: 60px;
    }
    .file-attachment{
        width: 55px;
    }
</style>
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
            @if($consultation->description)
                <div class="callout callout-info">
                    <h5>User Description:</h5>
                    {{ $consultation->description }}
                </div>
            @endif

          <!-- Main content -->
          <div class="invoice p-3 mb-3">
            <!-- title row -->
            <div class="row">
              <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                <b>User</b>
                <address>
                    <br>
                    <a href="#">
                        @if(is_null($consultation->user->avatar))
                            <img src="{{ url('media/user/avatar.png') }}" class="img-circle elevation-2 avatar">
                        @else
                            <img src="{{ url('media/user/'.$consultation->user->avatar) }}" class="img-circle elevation-2 avatar">
                        @endif
                        <br>
                        <strong>{{ $consultation->user->name }}</strong>
                    </a>
                    <br>
                    <b>Genger :</b> @lang('messages.'.$consultation->user->gender)<br>
                    <b>Birthdate :</b> {{ $consultation->user->birthdate }}
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <b>Doctor</b>
                <address>
                    @if($consultation->doctor)
                    <br>
                    <a href="{{ route('dashboard_doctor') }}?id={{ $consultation->doctor->id }}">
                        <img src="{{ url('media/doctor/'.$consultation->doctor->avatar) }}" class="img-circle elevation-2 avatar">
                        <br>
                        <strong>{{ $consultation->doctor->user->name }}</strong>
                    </a>
                    <br>
                    <b>Mobile :</b> {{ $consultation->doctor->user->mobile }}<br>
                    <b>Email :</b> {{ $consultation->doctor->user->email }}
                  @endif
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <b>Num #{{ $consultation->order->orderNumber }}</b><br>
                <br>
                <b>Created date:</b> {{ $consultation->created_at }}<br>
                <b>Date:</b> {{ $consultation->date }}<br>
                <b>Day:</b> {{ $consultation->schedule->day->title }}<br>
                <b>Time:</b> {{ $consultation->schedule->time->start.' - '.$consultation->schedule->time->end }}<br>
                <b>Status:</b> <span class="status-{{ $consultation->status }}">
                    {{ $consultation->status }}
                </span>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- Table row -->
            <div class="row">
              <div class="col-12 table-responsive">
                <table class="table table-striped">
                  <thead>
                  <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Time</th>
                  </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td>{{ $consultation->expertise->title }}</td>
                        <td>{{ $consultation->expertise->description }}</td>
                        <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <!-- accepted payments column -->
              <div class="col-6">

              </div>
              <!-- /.col -->
              <div class="col-6">

                <div class="table-responsive">
                  <table class="table">
                    <tr>
                      <th style="width:50%">Total:</th>
                      <td>{{ number_format($consultation->order->total_amount) }}</td>
                    </tr>
                    <tr>
                      <th>Discount:</th>
                      <td>{{ number_format($consultation->order->discount_amount) }}</td>
                    </tr>
                    <tr>
                      <th>Payable:</th>
                      <td>{{ number_format($consultation->order->payable_amount) }}</td>
                    </tr>
                  </table>
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.invoice -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
@endsection


@section('footer')
<script src="{{ asset('admin/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
@endsection

@section('pageScript')
<script>
    $(function () {
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
          event.preventDefault();
          $(this).ekkoLightbox({
            alwaysShowClose: true
          });
        });

        $('.filter-container').filterizr({gutterPixels: 3});
        $('.btn[data-filter]').on('click', function() {
          $('.btn[data-filter]').removeClass('active');
          $(this).addClass('active');
        });
      })

</script>
@endsection
