@extends('layouts.admin')

@section('title')
    Consultation
@endsection

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<style>
    .card{
        direction: rtl;
        text-align: right;
    }
    .card-title{
        float: right !important;
        text-align: right;
        direction: rtl;
    }
    .avatar{
        width: 50px;
    }
    .alertError{
        background: #dba35e;
    }
</style>
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Consultation</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                          <label>Status</label>
                          <select class="custom-select filter" id="status">
                            <option value="">All</option>
                            <option @if($request->status == 'pending') selected @endif value="pending">Pending</option>
                            <option @if($request->status == 'accepted') selected @endif value="accepted">Accepted</option>
                            <option @if($request->status == 'doing') selected @endif value="doing">Doing</option>
                            <option @if($request->status == 'done') selected @endif value="done">Done</option>
                            <option @if($request->status == 'canceled') selected @endif value="canceled">Canceled</option>
                            <option @if($request->status == 'rejected') selected @endif value="rejected">Rejected</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Start</label>
                            <input class="form-control filter" id="start" name="start" type="date" value="{{ $request->start }}" />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>End</label>
                            <input class="form-control filter" id="end" name="end" type="date" value="{{ $request->end }}" />
                        </div>
                    </div>
                </div>
              <table id="DataTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>User</th>
                  <th>Date & Time</th>
                  <th>Create date</th>
                  <th>Status</th>
                  <th>Tools</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ( $consultation as $order)
                        <tr>
                            <td>{{ $order->user->name }}</td>
                            <td>
                                {{ $order->date }} </br>
                                {{ $order->day->title }}</br>
                                {{ $order->visitStart.' ~ '.$order->visitEnd }}
                            </td>
                            <td>{{ $order->created_at }}</td>
                            <td class="status-{{ $order->status }}">
                                {{ $order->status }}
                            </td>
                            <td>
                                <a href="{{ route('dashboard_consultation_info') }}?id={{ $order->id }}">
                                    <i class="fas fa-info-circle tools-btn edit-btn" title="Information"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <div class="table-buttons row col-12">
                <div class="col-12">
                    {{ $consultation->appends(request()->except('page'))->links() }}
                </div>
                <div class="col-12">
                    <a href="{{ route('dashboard_consultation_export').'?status='.$request->status.'&start='.$request->start.'&end='.$request->end.'&user='.$request->user.'&doctor='.$request->doctor }}"><button class="btn btn-success float-right">Export <i class="fas fa-download"></i></button></a>
                </div>
                <div class="col-12"><br/></div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
@endsection


@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('pageScript')
<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });
    });

    $('.filter').change(function(){
        var status = $('#status').val();
        var start = $('#start').val();
        var end = $('#end').val();
        window.location.replace("/dashboard/consultation?status="+status+"&start="+start+"&end="+end+"&user={{ $request->user }}&doctor={{ $request->doctor }}");
    });

  </script>
@endsection
