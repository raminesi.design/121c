@extends('layouts.admin')

@section('title')
    Checkout list
@endsection

@section('header')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <style>
        .pending{
            color: rgb(230, 121, 32);
        }
        .processing{
            color: rgb(32, 95, 230);
        }
        .paid{
            color: rgb(88, 230, 32);
        }
        .reject{
            color: rgb(230, 32, 32);
        }
    </style>

@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Checkout list</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="DataTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Amount</th>
                  <th>Date of request</th>
                  <th>Date of review</th>
                  <th>Status</th>
                  <th>Tools</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ( $checkouts as $checkout)
                        <tr>
                            <td>{{$loop->iteration + $checkouts->firstItem() - 1}}</td>
                            <td>{{ $checkout->name  }}</td>
                            <td>{{ $checkout->amount }}</td>
                            <td>{{ $checkout->created_at }}</td>
                            <td>{{ $checkout->date }}</td>
                            <td class="{{ $checkout->status }}">{{ $checkout->status }}</td>
                            <td>
                                <a href="{{ route('dashboard_checkout') }}?id={{ $checkout->id }}">
                                    <i class="fas fa-info-circle tools-btn edit-btn" title="Information"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <div class="table-buttons">
                <div>
                    {{$checkouts->links()}}
                </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
@endsection


@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('pageScript')
<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });

    });
  </script>
@endsection
