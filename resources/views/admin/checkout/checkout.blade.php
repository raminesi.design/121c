@extends('layouts.admin')

@section('title')
    Checkout
@endsection

@section('header')
    <link rel="stylesheet" href="{{ asset('admin/plugins/ekko-lightbox/ekko-lightbox.css') }}">
    <style>
        .avatar{
            width: 60px;
        }
        .pending{
            color: rgb(230, 121, 32);
        }
        .processing{
            color: rgb(32, 95, 230);
        }
        .paid{
            color: rgb(88, 230, 32);
        }
        .reject{
            color: rgb(230, 32, 32);
        }
    </style>

@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Checkout</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                      <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                      <div class="col-sm-4 invoice-col">
                        <address>
                            @if(!is_null($checkout->user->doctor))
                            <img src="{{ url('media/doctor/'.$checkout->user->doctor->avatar) }}" class="img-circle elevation-2 avatar"><br/>
                            <strong><a href="{{ route('dashboard_doctor') }}?id={{ $checkout->user->doctor->id }}">{{ $checkout->user->name }}</a></strong><br/>
                            <strong>{{ $checkout->user->doctor->code }}</strong>
                            @else
                            <img src="{{ url('media/user/'.$checkout->user->avatar) }}" class="img-circle elevation-2 avatar"><br/>
                            <strong><a href="{{ route('dashboard_user') }}?id={{ $checkout->user->id }}">{{ $checkout->user->name }}</a></strong><br/>
                            <strong>{{ $checkout->user->code }}</strong>
                            @endif
                            <br/><strong>Type: {{ $checkout->userType }}</strong>
                        </address>
                      </div>
                      <div class="col-sm-4 invoice-col">
                        <address>
                            <strong>Amount: {{ number_format($checkout->amount) }} $</strong><br/>
                            <strong>Date: {{ $checkout->created_at }} </strong><br/>
                            <strong>Status: <span class="{{ $checkout->status }}">{{ $checkout->status }}</span> </strong><br/>
                            <strong>Bank name: {{ $checkout->bank->name }}</strong><br/>
                            <strong>Number: {{ $checkout->bank->code }}</strong><br/>
                        </address>
                      </div>
                      <div class="col-sm-4 invoice-col">
                        @if($checkout->status == 'processing')
                        <form method="post" action="{{ route('dashboard_checkout_status') }}" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <input name="id" type="hidden" value="{{ $checkout->id }}">
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <option value="paid">Paid</option>
                                    <option value="reject">Reject</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>File</label>
                                <div class="input-group" dir="ltr" style="text-align: left;">
                                    <div class="custom-file">
                                    <input type="file" name="file" class="custom-file-input @if ($errors->has('image')) is-invalid @endif" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">File selection</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info float-left">Submit</button>
                            </div>
                        </form>
                        @else
                        <address>
                            <strong>Date of review: {{ $checkout->date }} </strong><br/>
                            <strong>File: @if(!is_null($checkout->file))
                                <a target="_blank" href="{{ url('media/checkout/'.$checkout->file) }}">download</a>
                                @else - @endif </strong><br/>
                            <strong>Description: {{ $checkout->description }} </strong><br/>
                        </address>
                        @endif
                      </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
@endsection


@section('footer')

@endsection

@section('pageScript')
    <script>

  </script>
@endsection
