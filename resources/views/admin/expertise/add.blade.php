@extends('layouts.admin')

@section('title')
    Add
@endsection

@section('header')
    <style>
        .span-error{
            color: #F00;
            font-size: 10px;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js" integrity="sha512-f0VlzJbcEB6KiW8ZVtL+5HWPDyW1+nJEjguZ5IVnSQkvZbwBt2RfCBY0CBO1PsMAqxxrG4Di6TfsCPP3ZRwKpA==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.js" integrity="sha512-tCkLWlSXiiMsUaDl5+8bqwpGXXh0zZsgzX6pB9IQCZH+8iwXRYfcCpdxl/owoM6U4ap7QZDW4kw7djQUiQ4G2A==" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.css" integrity="sha512-SZgE3m1he0aEF3tIxxnz/3mXu/u/wlMNxQSnE0Cni9j/O8Gs+TjM9tm1NX34nRQ7GiLwUEzwuE3Wv2FLz2667w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css" integrity="sha512-3q8fi8M0VS+X/3n64Ndpp6Bit7oXSiyCnzmlx6IDBLGlY5euFySyJ46RUlqIVs0DPCGOypqP8IRk/EyPvU28mQ==" crossorigin="anonymous" />
@endsection

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form method="post" action="{{ route('dashboard_expertise_save') }}" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title float-right">Add</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Title*</label>
                        <input type="text" name="title" class="form-control @if ($errors->has('title')) is-invalid @endif" id="title" placeholder="Title" value="{{ old('title') }}">
                        @if ($errors->has('title'))
                            <span class="span-error">Please enter a title</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Video call price*</label>
                        <div>
                            <b>5 $</b> <input name="price_video" id="price_video" type="text" class="span2 slider-price" value="" data-slider-min="5" data-slider-max="500" data-slider-step="1" data-slider-value="@if(old('price_video')) [{{ old('price_video') }}] @else [100,200] @endif"/> <b>500 $</b>
                        </div>
                        <hr/>
                    </div>

                    <div class="form-group">
                        <label>Voice call price*</label>
                        <div>
                            <b>5 $</b> <input name="price_voice" id="price_voice" type="text" class="span2 slider-price" value="" data-slider-min="5" data-slider-max="500" data-slider-step="1" data-slider-value="@if(old('price_voice')) [{{ old('price_voice') }}] @else [80,180] @endif"/> <b>500 $</b>
                        </div>
                        <hr/>
                    </div>

                    <div class="form-group">
                        <label>Text chat price*</label>
                        <div>
                            <b>5 $</b> <input name="price_text" id="price_text" type="text" class="span2 slider-price" value="" data-slider-min="5" data-slider-max="500" data-slider-step="1" data-slider-value="@if(old('price_text')) [{{ old('price_text') }}] @else [60,160] @endif"/> <b>500 $</b>
                        </div>
                        <hr/>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-6">

                    <div class="form-group">
                        <label>Percent*</label>
                        <input type="number" name="percent" class="form-control @if ($errors->has('percent')) is-invalid @endif" id="percent" placeholder="Percent" value="{{ old('percent') }}">
                        @if ($errors->has('percent'))
                            <span class="span-error">Please enter percent</span>
                        @endif
                    </div>

                    <div class="form-group">
                    <label for="exampleInputFile">Icon*</label>
                    <div class="input-group" dir="ltr" style="text-align: left;">
                        <div class="custom-file">
                        <input type="file" name="image" class="custom-file-input @if ($errors->has('image')) is-invalid @endif" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">File selection</label>
                        </div>
                    </div>
                    @if ($errors->has('image'))
                        <span class="span-error">Please enter the icon</span>
                    @endif
                    </div>

                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" class="form-control @if ($errors->has('description')) is-invalid @endif" id="description" placeholder="Description">{{ old('description') }}</textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-info float-left">Submit</button>
                    </div>
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </form>
    </div>
</section>
<!-- /.content -->

@endsection


@section('footer')


@endsection

@section('pageScript')

    <script>
        // With JQuery
        //$("#ex2").slider({});

        // Without JQuery
        //var slider = new Slider('#price_video', {});
        new Slider('#price_video', {});
        new Slider('#price_voice', {});
        new Slider('#price_text', {});
    </script>
@endsection
