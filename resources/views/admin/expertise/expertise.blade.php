@extends('layouts.admin')

@section('title')
    Specialties
@endsection

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Specialties</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <form method="get">
                            <div class="form-group">
                            <label>@lang('page.Searching')</label>
                            <div class="row">
                                <input name="search" class="form-control col-8" value="{{ $request->search }}">
                                <button type="submit" class="btn btn-info col-3"><i class="nav-icon fas fa-search"></i></button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
              <table id="DataTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Image</th>
                  <th>Sort</th>
                  <th>Tools</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ( $expertise as $exp)
                        <tr>
                            <td>{{$loop->iteration + $expertise->firstItem() - 1}}</td>
                            <td>
                                {{  $exp->title  }}
                            </td>
                            <td><img src="{{ url('media/expertise/'.$exp->image) }}" class="img-circle elevation-2 avatar"></td>
                            <td><input class="changeSort" type="number" data-value="{{ $exp->id }}" min="0" style="width: 50px; text-align: center;" value="{{ $exp->sort }}"></td>
                            <td>
                                <a href="{{ route('dashboard_expertise_edit') }}?id={{ $exp->id }}">
                                    <i class="fas fa-info-circle tools-btn edit-btn" title="اطلاعات"></i>
                                </a>
                                <a href="{{ route('dashboard_expertise_status').'?id='.$exp->id }}">
                                    @if($exp->status == 'enable')
                                        <i class="fas fa-check tools-btn status-enable" title="enable"></i>
                                    @else
                                        <i class="fas fa-times tools-btn status-disable" title="disable"></i>
                                    @endif
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <div class="table-buttons">
                <div>
                    {{ $expertise->appends(request()->except('page'))->links() }}
                </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
@endsection


@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<!-- Bootstrap Switch -->
<script src="{{ asset('admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
@endsection

@section('pageScript')
<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });

        $(".switch_checkbox").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
            $('.bootstrap-switch-success').html('');
            $('.bootstrap-switch-danger').html('');
            $(this).parent().addClass('btnSwitch');
            var id = $(this).attr('data-value');
            $(this).parent().attr('id' , 'ch-'+id);
            $('#ch-'+id+' span').attr('data-value' , id);
        });


        $('.changeSort').change(function(){

            var id = $(this).attr('data-value');
            var value = $(this).val();
            $.ajax({
                type:'POST',
                url:'{{route('dashboard_expertise_sort')}}',
                data:{_token: "{{ csrf_token()}}" , id: id , sort: value},
                success: function( msg ) {
                    console.log(msg);
                }
            });

        });

    });
  </script>
@endsection
