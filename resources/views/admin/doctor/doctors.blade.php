@extends('layouts.admin')

@section('title')
Doctors
@endsection

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<style>
    .card{
        direction: rtl;
        text-align: right;
    }
    .card-title{
        float: right !important;
        text-align: right;
        direction: rtl;
    }
    .avatar{
        width: 50px;
    }
</style>
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Doctors</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <form method="get">
                            <div class="form-group">
                            <label>@lang('page.Searching')</label>
                            <div class="row">
                                <input name="search" class="form-control col-8" value="{{ $request->search }}">
                                <button type="submit" class="btn btn-info col-3"><i class="nav-icon fas fa-search"></i></button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
              <table id="DataTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Avatar</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Rate</th>
                  <th>Active</th>
                  <th>Tools</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ( $doctors as $doctor)
                        <tr>
                            <td>{{$loop->iteration + $doctors->firstItem() - 1}}</td>
                            <td>
                                {{  $doctor->name  }}
                            </td>
                            <td><img src="{{ url('media/doctor/'.$doctor->avatar) }}" class="img-circle elevation-2 avatar"></td>
                             <td>{{ $doctor->email }}</td>
                            <td>{{ $doctor->mobile }}</td>
                            <td>{{ $doctor->rate }}</td>
                            <td>@if($doctor->active == 1) Yes @else No @endif</td>
                            <td>
                                <a href="{{ route('dashboard_doctor') }}?id={{ $doctor->id }}">
                                    <i class="fas fa-info-circle tools-btn edit-btn" title="Information"></i>
                                </a>
                                <a href="{{ route('dashboard_doctor_status').'?id='.$doctor->id }}">
                                    @if($doctor->status == 'enable')
                                        <i class="fas fa-check tools-btn status-enable" title="Enable"></i>
                                    @else
                                        <i class="fas fa-times tools-btn status-disable" title="Disable"></i>
                                    @endif
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <div class="table-buttons row col-12">
                <div class="col-12">
                    {{ $doctors->appends(request()->except('page'))->links() }}
                </div>
                <div class="col-12">
                    <a href="{{ route('dashboard_doctor_export') }}"><button class="btn btn-success float-right">Export <i class="fas fa-download"></i></button></a>
                </div>
                <div class="col-12"><br/></div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
@endsection


@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<!-- Bootstrap Switch -->
<script src="{{ asset('admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
@endsection

@section('pageScript')
<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });
    });
  </script>
@endsection
