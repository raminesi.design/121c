@extends('layouts.admin')

@section('title')
    Add doctor
@endsection

@section('header')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/croppie.css') }}" media="all">
    <style>
        #pac-input:focus {
            border: 1px solid #193e72;
        }
        .croppie-container {
            height: auto;
            margin: 10px auto;
            border: 1px solid #fff;
        }
        .croppie-container .cr-boundary {
            padding: 10px;
        }
        .cr-image {
            font-size: 0;
        }
        .cr-slider-wrap {
            display: none;
        }
        #upload-image {
            display: none;
        }
        #Div-image {
            text-align: center;
            overflow: hidden;
        }
        #style_switcher a {
            color: #193e72;
        }

        .uk-file-upload {
            padding: 0;
        }
        .box-max-200 {
            max-width: 120px;
        }
        .slider-image {
            max-width: 235px;
            margin: 0 auto;
            border: 1px solid #000000;
        }
        .slider-image img {
            max-width: 100%;
            height: auto;
            display: block;
            margin: 0 auto;
            object-fit: cover;
        }
        .cr-boundary{
            border-radius: 50%;
        }
        .card{
            direction: rtl;
            text-align: right;
        }
        #Select_photo_croper{
            color: #193e72;
            font-size: 40px;
            cursor: pointer;
        }
    </style>

@endsection

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form method="post" action="{{ route('dashboard_doctor_save') }}" class="form-horizontal">
            @csrf
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title float-right">Add doctor</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>First name*</label>
                        <input type="text" name="firstname" class="form-control @if ($errors->has('firstname')) is-invalid @endif" id="firstname" placeholder="First name" value="{{ old('firstname') }}">
                        @if ($errors->has('firstname'))
                            <span class="span-error">Please enter a first name</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Last name*</label>
                        <input type="text" name="lastname" class="form-control @if ($errors->has('lastname')) is-invalid @endif" id="lastname" placeholder="Last name" value="{{ old('lastname') }}">
                        @if ($errors->has('lastname'))
                            <span class="span-error">Please enter a last name</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Birthdate</label>
                        <input type="date" name="birthdate" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group">
                        <div class="icheck-primary d-inline">
                            <input type="radio" id="radioPrimary2" name="gender" value="male" checked>
                            <label for="radioPrimary2"> Male
                            </label>
                          </div>
                        <div class="icheck-primary d-inline">
                            <input type="radio" id="radioPrimary1" name="gender" value="female">
                            <label for="radioPrimary1"> Female
                            </label>
                          </div>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" class="form-control @if ($errors->has('description')) is-invalid @endif" id="description" placeholder="Description">{{ old('description') }}</textarea>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-6">

                    <div class="form-group">
                        <div id="Div-image">
                            <i class="nav-icon fas fa-plus" id="Select_photo_croper"></i>
                            <div id="upload-demo" class="max-width-300"></div>
                            <input type="hidden" id="image-code" name="image" value="">
                            <input class="Upload_Croper" type="file" id="upload-image">
                            <div class="clearfix"><br></div>
                            @if ($errors->has('image'))
                                <span class="span-error">
                                    Please select an avatar
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Mobile*</label>
                        <input type="text" name="mobile" class="form-control @if ($errors->has('mobile')) is-invalid @endif" id="mobile" placeholder="Mobile" value="{{ old('mobile') }}">
                        @if ($errors->has('mobile'))
                            <span class="span-error">{{ $errors->first('mobile') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Email*</label>
                        <input type="text" name="email" class="form-control @if ($errors->has('email')) is-invalid @endif" id="email" placeholder="Email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                                <span class="span-error">{{ $errors->first('email') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-info float-right">Submit</button>
                    </div>
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </form>
    </div>
</section>
<!-- /.content -->

@endsection


@section('footer')


@endsection

@section('pageScript')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script src="{{ asset('js/croppie.js') }}"></script>

    <script type="text/javascript">

        $uploadCrop = $('#upload-demo').croppie({
            enableExif: true,
            viewport: {
                width: 200,
                height: 200,
                type: 'circle'
            },
            boundary: {
                width: 200,
                height: 200
            }
        });

        $('#upload-image').on('change', function () {
            var Id = $(this).parent().attr('id');
            $('#'+Id+' .cr-viewport').css('background-image' , 'none');
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    GetCode();
                });
            }
            reader.readAsDataURL(this.files[0]);
        });

        function GetCode() {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: {
                    width: 500,
                    height: 500
                },
                format: "jpeg",
                quality: '0.7'
            }).then(function (resp) {
                $('#image-code').val(resp);
            });
        }

        $('#Select_photo_croper').click(function () {
            $('#upload-image').trigger('click');
        });

        $('.remove_photo_croper').on('click', function() {
            $('.cr-viewport').css('background-image' , 'none');
            $('.cr-image').attr('src', '');
            $('#upload-image').val('');
            $('#image-code').val('delete');
        });

        $('.cr-overlay').mouseup(function () {
            GetCode();
        });

        $('.cr-overlay').bind('mousewheel', function(e){
            GetCode();
        });
    </script>
@endsection
