@extends('layouts.admin')

@section('title')
    {{ $user->name }}
@endsection

@section('header')

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('css/croppie.css') }}" media="all">
    <style>
        #pac-input:focus {
            border: 1px solid #193e72;
        }
        .croppie-container {
            height: auto;
            margin: 10px auto;
            border: 1px solid #fff;
        }
        .croppie-container .cr-boundary {
            padding: 10px;
        }
        .cr-image {
            font-size: 0;
        }
        .cr-slider-wrap {
            display: none;
        }
        #upload-image {
            display: none;
        }
        #Div-image {
            text-align: center;
            overflow: hidden;
        }
        #style_switcher a {
            color: #193e72;
        }

        .uk-file-upload {
            padding: 0;
        }
        .box-max-200 {
            max-width: 120px;
        }
        .slider-image {
            max-width: 235px;
            margin: 0 auto;
            border: 1px solid #000000;
        }
        .slider-image img {
            max-width: 100%;
            height: auto;
            display: block;
            margin: 0 auto;
            object-fit: cover;
        }
        .cr-boundary{
            border-radius: 50%;
        }
        .card{
            direction: rtl;
            text-align: right;
        }
        #Select_photo_croper{
            color: #193e72;
            font-size: 40px;
            cursor: pointer;
        }
        .span-error{
            color: #F00;
            font-size: 10px;
        }
        .pointer {
            cursor: pointer;
        }
</style>
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
              <div class="col-md-3">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                  <div class="card-body box-profile">
                    <div class="text-center">
                      <img class="profile-user-img img-fluid img-circle" src="{{ url('media/doctor/'.$doctor->avatar) }}" alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center">{{  $user->name  }}</h3>

                    <p class="text-muted text-center">{{  $doctor->code  }}</p>
                    <p class="text-muted text-center">{{  $user->mobile  }}</p>
                    <p class="text-muted text-center">{{  $user->email  }}</p>

                    <ul class="list-group list-group-unbordered mb-3">
                      <li class="list-group-item">
                        <b> All consultation </b> <a class="float-right"> {{ $allRequest }} </a>
                      </li>
                      <li class="list-group-item">
                        <b> done </b> <a class="float-right">{{ $doneRequest }}</a>
                      </li>
                      <li class="list-group-item">
                        <b> rejected </b> <a class="float-right">{{ $rejectRequest }}</a>
                      </li>
                      <li class="list-group-item">
                        <b> canceled </b> <a class="float-right">{{ $cancelRequest }}</a>
                      </li>
                      <li class="list-group-item">
                        <b> pending </b> <a class="float-right">{{ $pendingRequest }}</a>
                      </li>
                    </ul>

                    <a href="{{ route('dashboard_consultation').'?doctor='.$doctor->id }}" class="btn btn-primary btn-block"><b>Consultation</b></a>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->

              </div>
              <!-- /.col -->
              <div class="col-md-9">
                <div class="card">
                  <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Information</a></li>
                        <li class="nav-item"><a class="nav-link" href="#expertise" data-toggle="tab">Specialties</a></li>
                        <li class="nav-item"><a class="nav-link" href="#activity" data-toggle="tab">Schedule</a></li>
                        <li class="nav-item"><a class="nav-link" href="#bankInformation" data-toggle="tab">Bank</a></li>
                        <li class="nav-item"><a class="nav-link" href="#changePassword" data-toggle="tab">Change password</a></li>
                    </ul>
                  </div><!-- /.card-header -->
                  <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="settings">
                            <form method="post" action="{{route('dashboard_doctor_update' , [$doctor->id])}}" class="form-horizontal">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div id="Div-image">
                                            <i class="nav-icon fas fa-plus" id="Select_photo_croper"></i>
                                            <div id="upload-demo" class="max-width-300"></div>
                                            <input type="hidden" id="image-code" name="image" value="">
                                            <input class="Upload_Croper" type="file" id="upload-image">
                                            <div class="clearfix"><br></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                <label for="inputName" class="col-sm-2 col-form-label">First name*</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="firstname" class="form-control" id="firstname" value="@if(old('firstname')){{ old('firstname') }}@else{{ $user->first_name }}@endif" placeholder="First name">
                                        @if ($errors->has('firstname'))
                                            <span class="span-error">Please enter a first name</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">Last name*</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="lastname" class="form-control" id="lastname" value="@if(old('lastname')){{ old('lastname') }}@else{{ $user->last_name }}@endif" placeholder="Last name">
                                        @if ($errors->has('lastname'))
                                            <span class="span-error">Please enter a last name</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Birthdate</label>
                                    <div class="col-sm-10">
                                        <input type="date" name="birthdate" autocomplete="off" value="{{ $user->birthdate }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">Gender</label>
                                    <div class="col-sm-10">
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="radioPrimary2" name="gender" value="male" checked>
                                            <label for="radioPrimary2"> Male
                                            </label>
                                        </div>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="radioPrimary1" @if($user->gender == 'female') checked @endif name="gender" value="female">
                                            <label for="radioPrimary1"> Female
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Mobile*</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="mobile" class="form-control @if ($errors->has('mobile')) is-invalid @endif" id="mobile" placeholder="Mobile" value="@if(old('mobile')){{ old('mobile') }}@else{{ $user->mobile }}@endif">
                                        @if ($errors->has('mobile'))
                                            <span class="span-error">{{ $errors->first('mobile') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Email*</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="email" class="form-control @if ($errors->has('email')) is-invalid @endif" id="email" placeholder="Email" value="@if(old('email')){{ old('email') }}@else{{ $user->email }}@endif">
                                        @if ($errors->has('email'))
                                                <span class="span-error">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                <label for="inputExperience" class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="description" id="description" placeholder="Description">{{ $doctor->description }}</textarea>
                                </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-2 col-form-labe"></div>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="activity">
                            <table class="table-bordered col-12 table-hover" style="text-align: center;">
                                <head>
                                    <tr>
                                        <th>#</th>
                                        <th class="day pointer" data-status="0" data-id="1">Monday</th>
                                        <th class="day pointer" data-status="0" data-id="2">Tuesday</th>
                                        <th class="day pointer" data-status="0" data-id="3">Wednesday</th>
                                        <th class="day pointer" data-status="0" data-id="4">Thursday</th>
                                        <th class="day pointer" data-status="0" data-id="5">Friday</th>
                                        <th class="day pointer" data-status="0" data-id="6">Saturday</th>
                                        <th class="day pointer" data-status="0" data-id="7">Sunday</th>
                                    </tr>
                                </head>
                                <body>
                                    @foreach ($times as $time)
                                    <tr>
                                        <td class="time pointer" data-status="0" data-id="{{ $time->id }}">{{ $time->title }}</td>
                                        @for($i = 1 ; $i < 8 ; $i++)
                                            <td class="custom-checkbox"><input class="custom-control-input checkbox-times day-{{ $i }} time-{{ $time->id }}" id="ch-{{ $i }}-{{ $time->id }}" value="{{$i}}-{{$time->id}}" type="checkbox" @if($time->status == 'disable') disabled @endif @if(array_search( $i."-".$time->id , $arraySchedule) !== false) checked data-status="1" @else data-status="0" @endif><label for="ch-{{ $i }}-{{ $time->id }}" class="custom-control-label" ></label></td>
                                        @endfor
                                    </tr>
                                @endforeach
                                </body>
                            </table>
                        </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="expertise">

                        <div class="form-group">
                            <table class="table table-bordered table-hover">
                                @foreach ($allExpertise as $expertise)
                                    <tr>
                                        <td><img src="{{ url('media/expertise/'.$expertise->image) }}" class="img-circle elevation-2 avatar"></td>
                                        <td>{{ $expertise->title }}</td>
                                        <td>
                                            <input class="form-control checkbox-expertise" @if(array_search($expertise->id , $arrayExpertise) !== false) checked @endif name="expertise" type="radio" value="{{ $expertise->id }}" />
                                        </td>
                                    </tr>
                                @endforeach
                                </table>
                        </div>

                    </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="bankInformation">
                        <form method="post" action="{{ route('dashboard_doctor_account_number' , [$doctor->id]) }}" class="form-horizontal">
                            @csrf
                            <div class="form-group row">
                                <label>Bank account number*</label>
                                <input type="text" name="account_number" class="form-control" id="account_number" placeholder="Account number" value="{{ $doctor->account_number }}">
                                @if ($errors->has('account_number'))
                                    <span class="span-error">{{ $errors->first('account_number') }}</span>
                                @endif
                            </div>
                            <div class="form-group row">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="changePassword">
                        <form method="post" action="{{ route('dashboard_doctor_password' , [$doctor->id]) }}" class="form-horizontal">
                            @csrf
                            <div class="form-group row">
                                <label>Password* <span id="randomPassword">Random <i class="fas fa-key"></i></span></label>
                                <input type="text" name="password" class="form-control" id="password" placeholder="Password">
                                @if ($errors->has('password'))
                                    <span class="span-error">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <div class="form-group row">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                      </div>
                    </div>
                    <!-- /.tab-content -->
                  </div><!-- /.card-body -->
                </div>
                <!-- /.nav-tabs-custom -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div><!-- /.container-fluid -->
    </section>
@endsection


@section('footer')

@endsection

@section('pageScript')
<script src="{{ asset('js/croppie.js') }}"></script>
<script type="text/javascript">

    $('.day').click(function(){
        var id = $(this).attr('data-id');
        var status = $(this).attr('data-status');
        if(status == 0){
            $('.day-'+id).prop('checked', true);
            $(this).attr('data-status' , 1);
        }else{
            $('.day-'+id).prop('checked', false);
            $(this).attr('data-status' , 0);
        }
        var doctor_id = {{ $doctor->id }};
        $.ajax({
            type:'POST',
            url:'{{route('dashboard_doctor_all_time')}}',
            data:{_token: "{{ csrf_token()}}" , doctor_id: doctor_id , day_id: id , status: status},
            success: function( msg ) {
                console.log(msg);
            }
        });
    });

    $('.time').click(function(){
        var id = $(this).attr('data-id');
        var status = $(this).attr('data-status');
        if(status == 0){
            $('.time-'+id).prop('checked', true);
            $(this).attr('data-status' , 1);
        }else{
            $('.time-'+id).prop('checked', false);
            $(this).attr('data-status' , 0);
        }
        var doctor_id = {{ $doctor->id }};
        $.ajax({
            type:'POST',
            url:'{{route('dashboard_doctor_all_day')}}',
            data:{_token: "{{ csrf_token()}}" , doctor_id: doctor_id , time_id: id , status: status},
            success: function( msg ) {
                console.log(msg);
            }
        });
    });

    $('.checkbox-times').change(function(){
        var doctor_id = {{ $doctor->id }};
        var val = $(this).val().split('-');
        console.log(val);
        $.ajax({
            type:'POST',
            url:'{{route('dashboard_doctor_time')}}',
            data:{_token: "{{ csrf_token()}}" , doctor_id: doctor_id , day_id: val[0] , time_id: val[1]},
            success: function( msg ) {
                console.log(msg);
            }
        });
    });

    //start image 1
    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 200,
            height: 200,
            type: 'circle'
        },
        boundary: {
            width: 200,
            height: 200
        }
    });

    $('#upload-image').on('change', function () {
        var Id = $(this).parent().attr('id');
        $('#'+Id+' .cr-viewport').css('background-image' , 'none');
        var reader = new FileReader();
        reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function(){
                GetCode();
            });
        }
        reader.readAsDataURL(this.files[0]);
    });

    function GetCode() {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: {
                width: 500,
                height: 500
            },
            format: "jpeg",
            quality: '0.7'
        }).then(function (resp) {
            $('#image-code').val(resp);
        });
    }

    $('#Select_photo_croper').click(function () {
        $('#upload-image').trigger('click');
    });

    $('.remove_photo_croper').on('click', function() {
        $('.cr-viewport').css('background-image' , 'none');
        $('.cr-image').attr('src', '');
        $('#upload-image').val('');
        $('#image-code').val('delete');
    });

    $('.cr-overlay').mouseup(function () {
        GetCode();
    });

    $('.cr-overlay').bind('mousewheel', function(e){
        GetCode();
    });

    setTimeout(function () {
        $('.cr-image').attr('src', '{{ url('media/doctor/'.$doctor->avatar) }}').css({'width': '200' , 'height': '200'});
    } , 300);

    $('.checkbox-expertise').change(function(){
        var doctor_id = {{ $doctor->id }};
        var expertise_id = $(this).val();
        $.ajax({
            type:'POST',
            url:'{{route('dashboard_doctor_expertise')}}',
            data:{_token: "{{ csrf_token()}}" , doctor_id: doctor_id , expertise_id: expertise_id},
            success: function( msg ) {
                console.log(msg);
            }
        });
    });

    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@$';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
     }

     $('#randomPassword').click(function(){
         var password = makeid(8);
         $('#password').val(password);
     });

     @if ($errors->has('account_number'))
        activaTab('bankInformation');
     @endif

     @if ($errors->has('password'))
        activaTab('changePassword');
     @endif

     function activaTab(tab){
        $('.nav-item a[href="#' + tab + '"]').tab('show');
    };

</script>
@endsection
