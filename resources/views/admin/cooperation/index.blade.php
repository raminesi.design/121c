@extends('layouts.admin')

@section('title')
    Request list
@endsection

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<style>
    .pending{
        color: rgb(230, 121, 32);
    }
    .accepted{
        color: rgb(88, 230, 32);
    }
    .failed{
        color: rgb(230, 32, 32);
    }
</style>

@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Request list</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="DataTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Avatar</th>
                  <th>Gender</th>
                  <th>Mobile</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Tools</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ( $cooperation as $user)
                        <tr>
                            <td>{{$loop->iteration + $cooperation->firstItem() - 1}}</td>
                            <td>{{  $user->name  }}</td>
                            <td>
                                <img src="{{ url('media/doctor/'.$user->avatar) }}" class="img-circle elevation-2 avatar">
                            </td>
                            <td>@if($user->gender == 'male') {{ Lang::get('messages.male') }} @else {{ Lang::get('messages.female') }} @endif</td>
                            <td>{{ $user->mobile }}</td>
                            <td>{{ $user->email }}</td>
                            <td class="{{ $user->status }}">{{ $user->status }}</td>
                            <td>
                                <a href="{{ route('dashboard_cooperation_info') }}?id={{ $user->cId }}">
                                    <i class="fas fa-info-circle tools-btn edit-btn" title="Information"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <div class="table-buttons">
                <div>
                    {{$cooperation->links()}}
                </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
@endsection


@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('pageScript')
<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });

    });
  </script>
@endsection
