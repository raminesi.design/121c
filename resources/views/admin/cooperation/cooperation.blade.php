@extends('layouts.admin')

@section('title')
{{ $cooperation->name }}
@endsection

@section('header')
<style>
    .pending{
        color: rgb(230, 121, 32);
    }
    .accepted{
        color: rgb(88, 230, 32);
    }
    .failed{
        color: rgb(230, 32, 32);
    }
</style>
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Information</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <img src="{{ url('media/doctor/'.$cooperation->avatar) }}" class="img-circle elevation-2 avatar">
                    </div>
                    <div class="form-group">
                        <b>Name:</b>
                        <p>{{ $cooperation->name }}</p>
                    </div>
                    <div class="form-group">
                        <b>Gender:</b>
                        <p>{{ $cooperation->gender }}</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <b>Age:</b>
                        <p>{{ $cooperation->age }}</p>
                    </div>
                    <div class="form-group">
                        <b>Email:</b>
                        <p>{{ $cooperation->email }}</p>
                    </div>
                    <div class="form-group">
                        <b>Mobile:</b>
                        <p>{{ $cooperation->mobile }}</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <b>Expertise:</b>
                        <p>{{ $cooperation->expertise }}</p>
                    </div>
                    <div class="form-group">
                        <b>Description:</b>
                        <p>{{ $cooperation->description }}</p>
                    </div>
                    <div class="form-group">
                        <b>Status:</b>
                        <p class="{{ $cooperation->status }}">{{ $cooperation->status }}</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    @if($cooperation->status == 'pending')
                    <form method="post" action="{{ route('dashboard_cooperation_update') }}" class="form-horizontal">
                        @csrf
                        <input name="id" type="hidden" value="{{ $cooperation->cId }}">
                        <div class="form-group">
                            <label>Comment*</label>
                            <input type="text" name="comment" class="form-control @if ($errors->has('comment')) is-invalid @endif" id="comment" placeholder="Comment" value="{{ old('comment') }}">
                            @if ($errors->has('comment'))
                                <span class="span-error">Please enter a comment</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select name="status" class="form-control">
                                <option value="accepted">Accepted</option>
                                <option value="failed">Failed</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-info float-right">Submit</button>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
@endsection


@section('footer')

@endsection

@section('pageScript')

@endsection
