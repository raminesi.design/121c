@extends('layouts.admin')

@section('title')
 Tickets
@endsection

@section('header')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Tickets</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="DataTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>user</th>
                  <th>Subject</th>
                  <th>Status</th>
                  <th>Creation date</th>
                  <th>Update date</th>
                  <th>Tools</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ( $tickets as $ticket)
                        <tr>
                            <td>{{$loop->iteration + $tickets->firstItem() - 1}}</td>
                            <td>{{  $ticket->user->name  }}</td>
                            <td>{{  $ticket->subject  }}</td>
                            <td>{{  $ticket->status  }}</td>
                            <td>{{  $ticket->created_at  }}</td>
                            <td>{{  $ticket->updated_at  }}</td>
                            <td>
                                <a href="{{ route('dashboard_ticket') }}?id={{ $ticket->id }}">
                                    <i class="fas fa-info-circle tools-btn edit-btn" title="information"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <div class="table-buttons">
                <div>
                    {{ $tickets->appends(request()->except('page'))->links() }}
                </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
@endsection


@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('pageScript')
<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });
    });
  </script>
@endsection
