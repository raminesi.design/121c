@extends('layouts.admin')

@section('title')
 Ticket
@endsection

@section('header')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=twww1hnqtso2i9wm65hz6r8lkzj1i7ihdo31cbvlh4qtqflj"></script>
    <style>
        hr.dashed {
            border-top: 1px dashed rgba(180, 180, 180, 0.37);
        }
    </style>
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Ticket</h3>
              <span class="float-right"> Status: {{ $ticket->status }}</span>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if ($ticket->status == 'open')
                    <p>Click to close the ticket: <a href="{{ route('dashboard_ticket_close').'?id='.$ticket->id }}"><span>Close</span></a></p>
                @endif
                <p>Subject: <br/>{{ $ticket->subject }} </p>
                <hr/>
                {!! $ticket->text !!}
                <hr/>
                @if(!is_null($ticket->file))
                <span><a target="_blank" href="{{ url('media/ticket/'.$ticket->file) }}">Download file <i class="fas fa-download"></i></a></span>
                <hr/>
            @endif
                @if ($ticket->status == 'open')
                <form method="post" action="{{route('dashboard_ticket_comment_save')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>File</label>
                        <div class="custom-file col-12">
                            <input type="file" name="file" class="custom-file-input @if($errors->has('file')) is-invalid @endif" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">File selection</label>
                        </div>
                        @if ($errors->has('file'))
                            <span class="span-error">{{ $errors->first('file') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input name="id" value="{{ $ticket->id }}" type="hidden">
                        <label>Text*</label>
                        <textarea id="textTicket" name="text" class="form-control" id="text">{{ old('text') }}</textarea>
                        @if ($errors->has('text'))
                            <span class="span-error">{{ $errors->first('text') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">submit</button>
                    </div>
                </form>
                <hr/>
                @endif
                @if(!is_null($ticket->tickets))
                 <table id="DataTable" class="table table-bordered table-hover">
                        @foreach ( $ticket->tickets as $details)
                        <tr>
                            <td class="text-left">
                                [@if($details->userId == $ticket->userId) User @else Admin @endif] <hr class="dashed"/> {!! $details->text !!}
                                @if(!is_null($details->file))
                                    <hr class="dashed"/>
                                    <span><a target="_blank" href="{{ url('media/ticket/'.$details->file) }}">Download file <i class="fas fa-download"></i></a></span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                  </table>
                @endif
            </div>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
@endsection


@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('pageScript')
<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });
    });
    tinymce.init({
        selector:'#textTicket' ,
        menubar: 'insert',
        default_link_target: '_blank',
        directionality :"ltr"
    });
  </script>
@endsection
