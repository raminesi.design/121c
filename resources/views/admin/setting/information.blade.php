@extends('layouts.admin')

@section('title')
    Information
@endsection

@section('header')

@endsection

@section('content')

    <section class="content">
        <div class="container-fluid">
            <form method="post" action="{{ route('dashboard_settings_information_save') }}" class="form-horizontal">
                @csrf
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title float-right">Information</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Website name</label>
                            <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" id="name" placeholder="Website name" value="@if(old('name')){{ old('name') }}@else{{@$setting->name}}@endif">
                            @if ($errors->has('name'))
                                <span class="span-error">Please enter a website name</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Keywords</label>
                            <input type="text" name="keywords" class="form-control @if ($errors->has('keywords')) is-invalid @endif" id="keywords" placeholder="Keywords" value="@if(old('keywords')){{ old('keywords') }}@else{{@$setting->keywords}}@endif">
                            @if ($errors->has('keywords'))
                                <span class="span-error">Please enter keywords</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text" name="description" class="form-control @if ($errors->has('description')) is-invalid @endif" id="description" placeholder="Description" value="@if(old('description')){{ old('description') }}@else{{@$setting->description}}@endif">
                            @if ($errors->has('description'))
                                <span class="span-error">Please enter description</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Twitter</label>
                            <input type="text" name="twitter" class="form-control @if ($errors->has('twitter')) is-invalid @endif" id="twitter" placeholder="Twitter" value="@if(old('twitter')){{ old('twitter') }}@else{{@$setting->twitter}}@endif">
                            @if ($errors->has('twitter'))
                                <span class="span-error">Please enter twitter</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Facebook</label>
                            <input type="text" name="facebook" class="form-control @if ($errors->has('facebook')) is-invalid @endif" id="facebook" placeholder="Facebook" value="@if(old('facebook')){{ old('facebook') }}@else{{@$setting->facebook}}@endif">
                            @if ($errors->has('facebook'))
                                <span class="span-error">Please enter facebook</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Linkedin</label>
                            <input type="text" name="linkedin" class="form-control @if ($errors->has('linkedin')) is-invalid @endif" id="linkedin" placeholder="linkedin" value="@if(old('linkedin')){{ old('linkedin') }}@else{{@$setting->linkedin}}@endif">
                            @if ($errors->has('linkedin'))
                                <span class="span-error">Please enter linkedin</span>
                            @endif
                        </div>
                    </div>
                    <!-- /.col -->

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" name="phone" class="form-control @if ($errors->has('phone')) is-invalid @endif" id="phone" placeholder="Phone" value="@if(old('phone')){{ old('phone') }}@else{{@$setting->phone}}@endif">
                            @if ($errors->has('phone'))
                                <span class="span-error">Please enter a phone</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" class="form-control @if ($errors->has('email')) is-invalid @endif" id="email" placeholder="Email" value="@if(old('email')){{ old('email') }}@else{{@$setting->email}}@endif">
                            @if ($errors->has('email'))
                                <span class="span-error">Please enter a email</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" name="address" class="form-control @if ($errors->has('address')) is-invalid @endif" id="address" placeholder="Address" value="@if(old('address')){{ old('address') }}@else{{@$setting->address}}@endif">
                            @if ($errors->has('address'))
                                <span class="span-error">Please enter a address</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Skype</label>
                            <input type="text" name="skype" class="form-control @if ($errors->has('skype')) is-invalid @endif" id="skype" placeholder="Skype" value="@if(old('skype')){{ old('skype') }}@else{{@$setting->skype}}@endif">
                            @if ($errors->has('skype'))
                                <span class="span-error">Please enter skype</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Instagram</label>
                            <input type="text" name="instagram" class="form-control @if ($errors->has('instagram')) is-invalid @endif" id="instagram" placeholder="Instagram" value="@if(old('instagram')){{ old('instagram') }}@else{{@$setting->instagram}}@endif">
                            @if ($errors->has('instagram'))
                                <span class="span-error">Please enter instagram</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>About</label>
                            <textarea name="about" class="form-control">@if(old('about')){{ old('about') }}@else{{@$setting->about}}@endif</textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-info float-left">Submit</button>
                        </div>
                    </div>
                    <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </form>
        </div>
    </section>

@endsection


@section('footer')

@endsection

@section('pageScript')
  <script>

  </script>
@endsection
