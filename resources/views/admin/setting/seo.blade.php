@extends('layouts.admin')

@section('title')
سئو
@endsection

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<style>
    .fa-star{
        color: rgb(255, 102, 0);

    }
</style>
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">لیست سئو</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="DataTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>توضیحات</th>
                  <th>زبان</th>
                  <th>پیشفرض</th>
                  <th>ابزارها</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ( $settings as $setting)
                        <tr>
                            <td>{{  $setting->description  }}</td>
                            <td>{{  $setting->lang  }}</td>
                            <td>
                                    @if($setting->default)
                                        <i class="fas fa-star"></i>
                                    @else
                                        <a href="{{ route('dashboard_settings_default').'?id='.$setting->id }}">
                                            <i class="far fa-star"></i>
                                        </a>
                                    @endif
                            </td>
                            <td>
                                <a href="{{ route('dashboard_settings_seo_edit') }}?id={{ $setting->id }}">
                                    <i class="fas fa-pencil-alt tools-btn edit-btn" title="ویرایش"></i>
                                </a>
                                <a href="{{ route('dashboard_settings_status').'?id='.$setting->id }}">
                                    @if($setting->status == 'enable')
                                        <i class="fas fa-check tools-btn status-enable" title="فعال"></i>
                                    @else
                                        <i class="fas fa-times tools-btn status-disable" title="غیر فعال"></i>
                                    @endif
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>

    <!-- Modal -->
    <div id="imageViewModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <img src="" id="imageView">
    </div>
    </div>

@endsection


@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('pageScript')
<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });
    });
  </script>
@endsection
