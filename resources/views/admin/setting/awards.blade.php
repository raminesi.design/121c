@extends('layouts.admin')

@section('title')
افتخارات
@endsection

@section('header')
    <style>
        .card{
            direction: rtl;
            text-align: right;
        }
        .form-control.is-invalid{
            background-position: left calc(.375em + .1875rem) center !important;
        }
        .span-error{
            color: #F00;
            font-size: 10px;
        }
        .span-success{
            font-size: 12px;
            color: rgb(46, 163, 0);
        }
    </style>
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">افتخارات</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form method="post" action="{{route('dashboard_settings_awards_update')}}">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>@lang('front.Happy Clients')*</label>
                            <input type="number" name="HappyClients" class="form-control" id="HappyClients" value="{{ $awards->HappyClients }}">
                            @if ($errors->has('HappyClients'))
                                <span class="span-error">{{ $errors->first('HappyClients') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label>@lang('front.Projects')*</label>
                            <input type="number" name="Projects" class="form-control" id="Projects" value="{{ $awards->Projects }}">
                            @if ($errors->has('Projects'))
                                <span class="span-error">{{ $errors->first('Projects') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label>@lang('front.Years of experience')*</label>
                            <input type="number" name="Experience" class="form-control" id="Experience" value="{{ $awards->Experience }}">
                            @if ($errors->has('Experience'))
                                <span class="span-error">{{ $errors->first('Experience') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label>@lang('front.Awards')*</label>
                            <input type="number" name="Awards" class="form-control" id="Awards" value="{{ $awards->Awards }}">
                            @if ($errors->has('Awards'))
                                <span class="span-error">{{ $errors->first('Awards') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-12">
                            <button type="submit" class="btn btn-success">@lang('front.Submit')</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>

@endsection


@section('footer')

@endsection

@section('pageScript')
  <script>

  </script>
@endsection
