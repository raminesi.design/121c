@extends('layouts.admin')

@section('title')
Password
@endsection

@section('header')
    <style>
        .card{
            direction: rtl;
            text-align: right;
        }
        .form-control.is-invalid{
            background-position: left calc(.375em + .1875rem) center !important;
        }
        .span-error{
            color: #F00;
            font-size: 10px;
        }
        .span-success{
            font-size: 12px;
            color: rgb(46, 163, 0);
        }
        #randomPassword{
            cursor: pointer;
            color: rgb(228, 129, 0);
        }
    </style>
@endsection

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Change the password</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form method="post" action="{{route('dashboard_settings_password_update')}}">
                    @csrf
                    <div class="row">
                        <div class="form-group col-12">
                            <label>Password* <span id="randomPassword">Random <i class="fas fa-key"></i></span></label>
                            <input type="text" name="password" class="form-control" id="password" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="span-error">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                            @if ($errors->has('msg'))
                                <div class="form-group col-12">
                                    <span class="span-success">{{ $errors->first('msg') }}</span>
                                </div>
                            @endif
                        <div class="form-group col-12">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>

@endsection


@section('footer')

@endsection

@section('pageScript')
  <script>
    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@$';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
     }

     $('#randomPassword').click(function(){
         var password = makeid(10);
         $('#password').val(password);
     });
  </script>
@endsection
