@extends('layouts.admin')

@section('title')
    ویرایش سئو
@endsection

@section('header')
    <style>
        .span-error{
            color: #F00;
            font-size: 10px;
        }
        .card{
            direction: rtl;
            text-align: right;
        }
        .form-control.is-invalid{
            background-position: left calc(.375em + .1875rem) center !important;
        }
        .span-error{
            color: #F00;
            font-size: 10px;
        }
    </style>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=twww1hnqtso2i9wm65hz6r8lkzj1i7ihdo31cbvlh4qtqflj"></script>
@endsection

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form method="post" action="{{ route('dashboard_settings_update_seo' , [$setting->id]) }}" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title float-right">ویرایش سئو</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>زبان</label>
                        <input type="text" disabled class="form-control" value="{{$language->title}}">
                    </div>
                    <div class="form-group">
                        <label>توضیحات</label>
                        <textarea rows="4" class="form-control @if ($errors->has('description')) is-invalid @endif" name="description" >@if(old('description')){{ old('description') }}@else{{$setting->description}}@endif</textarea>
                        @if ($errors->has('description'))
                            <span class="span-error">لطفا توضیحات را وارد کنید</span>
                        @endif
                    </div>
                </div>
                <!-- /.col -->

                <div class="col-md-6">
                    <div class="form-group">
                        <label>کلید واژه ها</label>
                        <textarea rows="8" class="form-control @if ($errors->has('keywords')) is-invalid @endif" name="keywords" >@if(old('keywords')){{ old('keywords') }}@else{{$setting->keywords}}@endif</textarea>
                        @if ($errors->has('keywords'))
                            <span class="span-error">لطفا کلید واژه ها را وارد کنید</span>
                        @endif
                    </div>
                </div>

                <div class="col-md-12">

                    <div class="form-group">
                        <button type="submit" class="btn btn-info float-left">ثبت</button>
                    </div>
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </form>
    </div>
</section>
<!-- /.content -->

@endsection


@section('footer')


@endsection

@section('pageScript')
        <script type="text/javascript">
            var lat     = $('#lat').val();
            var lng     = $('#lng').val();
            var mymap = L.map('map', {
                center: [lat, lng],
                zoom: 13
            });
            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoicmFtaW5lc2kiLCJhIjoiY2tpOGhxaGptMDV4ODJ0bGN4ZDNoM3l3cyJ9.N48L9J8T_29jSoOg7vNzwg', {
                maxZoom: 18,
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1,
                accessToken: 'pk.eyJ1IjoicmFtaW5lc2kiLCJhIjoiY2tpOGhxaGptMDV4ODJ0bGN4ZDNoM3l3cyJ9.N48L9J8T_29jSoOg7vNzwg'
            }).addTo(mymap);
            var marker = L.marker([lat , lng] , {draggable:'true'});
            var newLatLng;
            function changeLocation(lat , lng){
                mymap.panTo(new L.LatLng(lat, lng));
                newLatLng = new L.LatLng(lat, lng);
                marker.setLatLng(newLatLng);
            }
            marker.on('dragend', function (e) {
                document.getElementById('lat').value = marker.getLatLng().lat;
                document.getElementById('lng').value = marker.getLatLng().lng;
            });
            marker.addTo(mymap);

            tinymce.init({
                selector:'#about' ,
                plugins: 'link image',
                menubar: 'insert',
                default_link_target: '_blank',
                directionality :"rtl",
                image_list: [
                    //{title: 'My image 1', value: 'https://www.example.com/my1.gif'},
                    //{title: 'My image 2', value: 'http://www.moxiecode.com/my2.gif'}
                ]
            });
        </script>
@endsection
