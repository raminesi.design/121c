<html itemscope itemtype="http://schema.org/Product" prefix="og: http://ogp.me/ns#" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="content-type" content="text/html;charset=utf-8">
    </head>
    <body>
        <div id="meet"></div>
        <script src="https://oxintv.com/external_api.js"></script>
        <script>
            var domain = "oxintv.com";
            @if($request->type == 'voice')
                var options = {
                    roomName: "{{ $request->channel }}",
                    parentNode: document.querySelector('#meet'),
                    configOverwrite: {},
                    interfaceConfigOverwrite: {
                        TOOLBAR_BUTTONS: [
                                    'microphone' , 'hangup'
                                ]
                    }
                }
            @else
                var options = {
                    roomName: "{{ $request->channel }}",
                    parentNode: document.querySelector('#meet'),
                    configOverwrite: {},
                    interfaceConfigOverwrite: {}
                }
            @endif
            var api = new JitsiMeetExternalAPI(domain, options);
            @if($request->type == 'voice')
                api.executeCommand('toggleVideo');
            @endif
            api.addEventListeners({videoConferenceLeft: endCall});
            function endCall(object){
                window.location.replace("{{ route('consultation_endCall').'?channel='.$request->channel }}");
            }
        </script>
    </body>
</html>
