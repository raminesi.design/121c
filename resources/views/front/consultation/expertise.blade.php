@extends('layouts.page')

@section('header')

@endsection

@section('content')
<main id="main">
<!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs doctors">
      <div class="container">
            <div class="row">
                @foreach($expertise as $key => $value)
                    <div class="col-lg-3 col-sm-6 col-md-4 mt-4 mt-lg-0">
                        <div class="member align-items-start">
                            <div class="pic"><img src="{{ url('media/expertise/'.$value->image) }}" class="img-fluid" alt="{{ $value->title }}"></div>
                            <div class="member-info">
                                <p><strong>{{ $value->title }}</strong></p>
                                <p><a href="{{ route('consultation_doctors').'?id='.$value->id }}"><i class="icofont-rounded-double-right"></i> Select</a></p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
      </div>
    </section>
</main>
@endsection

@section('footer')

@endsection

@section('script')

@endsection
