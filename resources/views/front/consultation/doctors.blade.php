@extends('layouts.page')

@section('header')

@endsection

@section('content')
<main id="main">
<!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs doctors">
      <div class="container">
            <div class="row">
                @foreach($doctors as $key => $value)
                    <div class="col-lg-3 col-sm-6 col-md-4 mt-4 mt-lg-0">
                        <div class="member align-items-start">
                            <div class="pic"><img src="{{ url('media/doctor/'.$value->avatar) }}" class="img-fluid" alt="{{ $value->name }}"></div>
                            <div class="member-info">
                                <p><strong>{{ $value->user->name }}</strong></p>
                                <p>
                                    <i class="icofont-star @if($value->rate >= 1) checked-star @endif"></i>
                                    <i class="icofont-star @if($value->rate >= 2) checked-star @endif"></i>
                                    <i class="icofont-star @if($value->rate >= 3) checked-star @endif"></i>
                                    <i class="icofont-star @if($value->rate >= 4) checked-star @endif"></i>
                                    <i class="icofont-star @if($value->rate >= 5) checked-star @endif"></i>
                                </p>
                                <p>Video: @if($value->expertises[0]->price_video >= 0){{ number_format($value->expertises[0]->price_video + ($value->expertises[0]->price_video * $value->expertises[0]->expertise->percent / 100 )) }}$ <a href="{{ route('consultation_schedule').'?id='.$value->id.'&type=video' }}"><i class="icofont-rounded-double-right"></i> Select</a> @else - @endif</p>
                                <p>Voice: @if($value->expertises[0]->price_voice >= 0){{ number_format($value->expertises[0]->price_voice + ($value->expertises[0]->price_voice * $value->expertises[0]->expertise->percent / 100 )) }}$ <a href="{{ route('consultation_schedule').'?id='.$value->id.'&type=voice' }}"><i class="icofont-rounded-double-right"></i> Select</a> @else - @endif</p>
                                <p>Text: @if($value->expertises[0]->price_text >= 0){{ number_format($value->expertises[0]->price_text + ($value->expertises[0]->price_text * $value->expertises[0]->expertise->percent / 100 )) }}$ <a href="{{ route('consultation_schedule').'?id='.$value->id.'&type=text' }}"><i class="icofont-rounded-double-right"></i> Select</a> @else - @endif</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
      </div>
    </section>
</main>
@endsection

@section('footer')

@endsection

@section('script')

@endsection
