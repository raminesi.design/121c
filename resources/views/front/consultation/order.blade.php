@extends('layouts.page')

@section('header')
    <style>
        .line-text {
            text-decoration-line:line-through;
          }
          .switch {
            position: relative;
            display: inline-block;
            width: 50px;
            height: 23px;
          }

          .switch input {
            opacity: 0;
            width: 0;
            height: 0;
          }

          .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
          }

          .slider:before {
            position: absolute;
            content: "";
            height: 16px;
            width: 16px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
          }

          input:checked + .slider {
            background-color: #2196F3;
          }

          input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
          }

          input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
          }

          /* Rounded sliders */
          .slider.round {
            border-radius: 34px;
          }

          .slider.round:before {
            border-radius: 50%;
          }
    </style>

@endsection

@section('content')
<main id="main">
<!-- ======= Breadcrumbs Section ======= -->
<section class="breadcrumbs doctors">
    <div class="container">
          <div class="row">
              <div class="col-md-3 mt-4 mt-lg-0"></div>
              <div class="col-md-6 mt-4 mt-lg-0 text-center">
                <table width="100%">
                    <tr>
                        <th>
                            <img src="{{ url('media/doctor/'.$order->consultation->doctor->avatar) }}"  class="img-circle elevation-2 avatar" alt="{{ $order->consultation->doctor->user->name }}"><br/>
                            {{ $order->consultation->doctor->user->name }}<br/>
                            ({{ $order->consultation->expertise->title }})<br/>
                        </th>
                        <th class="text-left">
                            Date: {{ $order->consultation->date }}<br/>
                            Time: {{ $order->consultation->visitStart.' ~ '.$order->consultation->visitEnd }}<br/>
                            Number of sessions: {{ $order->consultation->sessions }}<br/>
                        </th>
                    </tr>
                    <tr>
                        <td></td>
                        <td>

                            <table width="100%">
                                <tr>
                                    <td class="text-left" width="30%">Total: </td>
                                    <td class="text-left">{{ number_format($order->total_amount) }}$</td>
                                </tr>
                                <tr>
                                    <td class="text-left">Discount: </td>
                                    <td class="text-left">{{ (is_null($order->discount_amount) ? '0$' : number_format($order->discount_amount).'$') }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left">Wallet: </td>
                                    @if($user->wallet->credit  > 0)
                                    <td id="wallet" class="text-left @if($order->wallet_amount == 0) line-text @endif">{{ number_format($user->wallet->credit) }}$ &nbsp;
                                        <label class="switch">
                                            <input id="walletStatus" type="checkbox" @if($order->wallet_amount > 0) checked @endif>
                                            <span class="slider round"></span>
                                          </label>
                                    </td>
                                    @else
                                        <td class="text-left">0$</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="text-left">Payable: </td>
                                    <td class="text-left"><span id="payable_amount">{{ number_format($order->payable_amount) }}</span>$</td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>

                <hr/>
                <a href="{{ route('transaction').'?num='.$order->orderNumber }}"><button class="btn btn-success">Pay</button></a>
                <a href="{{ route('transaction_cancel').'?num='.$order->orderNumber }}"><button class="btn btn-danger">Cancel</button></a>
              </div>
              <div class="col-md-3 mt-4 mt-lg-0"></div>
          </div>
    </section>
</main>
@endsection

@section('footer')

@endsection

@section('script')
    <script>
        $('#walletStatus').change(function(){
            if($(this).is(':checked')){
                $('#wallet').removeClass('line-text');
            }else{
                $('#wallet').addClass('line-text');
            }
            $.ajax({
                type:'POST',
                url:'{{route('consultation_order_wallet_used')}}',
                data:{_token: "{{ csrf_token()}}" , num: {{ $order->orderNumber }} },
                success: function( amount ) {
                    $('#payable_amount').html(amount);
                }
            });
        });
    </script>
@endsection
