@extends('layouts.page')

@section('header')

@endsection

@section('content')
<main id="main">
<!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs doctors">
      <div class="container">
            <div class="row">
                <div class="col-md-4 mt-4 mt-lg-0"></div>
                <div class="col-md-4 mt-4 mt-lg-0 text-center">
                    <div class="form-group">
                        <strong>Schedule</strong><br/>
                        <div class="pic"><img src="{{ url('media/doctor/'.$doctor->avatar) }}" class="img-fluid avatar" alt="{{ $doctor->user->name }}"></div>
                        <strong>{{ $doctor->user->name }}</strong>
                        <p>Type: {{ $Consultation['type'] }}</p>
                        <p>Price: <span id="price" data-price="{{ $price[$Consultation['type']] }}">{{ number_format($price[$Consultation['type']]) }}</span> $</p>
                        <input id="newPrice" value="{{ $price[$Consultation['type']] }}" type="hidden" />
                        <input id="extraSessionPrice" value="0" type="hidden" />
                        <input id="extraTimePrice" value="0" type="hidden" />
                        <hr/>
                        @if ($errors->has('schStatus'))
                            <div class="validate text-center">This time is not in the doctors schedule</div>
                            <hr/>
                        @endif
                        @if ($errors->has('consultationStatus'))
                            <div class="validate text-center">This time has already been booked by someone else</div>
                            <hr/>
                        @endif
                    </div>
                    <form action="{{ route('consultation_schedule_save') }}" method="post" role="form">
                        @csrf
                        <div class="form-group">
                            <label>Number of sessions</label>
                            <select name="sessions" id="sessions" class="form-control">
                                <option @if(old('sessions') == 1) selected @endif value="1">1</option>
                                <option @if(old('sessions') == 2) selected @endif value="2">2</option>
                                <option @if(old('sessions') == 3) selected @endif value="3">3</option>
                                <option @if(old('sessions') == 4) selected @endif value="4">4</option>
                                <option @if(old('sessions') == 5) selected @endif value="5">5</option>
                                <option @if(old('sessions') == 6) selected @endif value="6">6</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Day</label>
                            <select name="date" id="date-dropdown" class="form-control">
                                <option value="">--select day--</option>
                                @foreach($Schedule as $value)
                                    <option @if(old('date') == $value['date']) selected @endif value="{{ $value['date'] }}">{{ $value['name'] }} ({{ $value['date'] }})</option>
                                @endforeach
                            </select>
                            @if ($errors->has('date'))
                                <div class="validate">Please select a day</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Start time</label>
                            <input class="form-control" type="time" id="visitStart" name="visitStart" value="{{ old('visitStart') }}">
                            @if ($errors->has('visitStart'))
                                <div class="validate">Please select a start time</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Duration</label>
                            <select name="visitTime" id="visitTime" class="form-control">
                                @foreach($packages as $value)
                                    <option @if(old('visitTime') == $value->package->id) selected @endif data-percent="{{ $value->package->percent }}" id="t-{{ $value->package->id }}" value="{{ $value->package->id }}">{{ $value->package->title }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('visitTime'))
                                <div class="validate">Please select a time</div>
                            @endif
                        </div>
                        @if(count($children) > 0)
                            <div class="form-group">
                                <label>Sick</label>
                                <select name="sick" class="form-control">
                                    <option value="">myself</option>
                                    @foreach($children as $key => $value)
                                        <option @if(old('sick') == $value->id) selected @endif value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="text-center"><button class="btn btn-info" type="submit">Submit</button></div>
                    </form>
                </div>
                <div class="col-md-4 mt-4 mt-lg-0"></div>
            </div>
      </div>
    </section>
</main>
@endsection

@section('footer')

@endsection

@section('script')
    <script>
        $('#sessions').change(function(){
            var price = parseInt($('#newPrice').val()) + parseInt($('#extraTimePrice').val());
            var sessions = parseInt($(this).val());
            var newPrice = price;
            if(sessions > 2){
                newPrice = ( price * sessions ) - (sessions * 1.5 * 100 / price);
            }else{
                newPrice = price * sessions;
            }
            $('#extraSessionPrice').val(parseInt(newPrice - price));
            $('#price').html(addCommas(Math.round(newPrice)));
        });
        $('#visitTime').change(function(){
            var val = $(this).val();
            var percent = $('#t-'+val).attr('data-percent') * 1;
            var price = parseInt($('#newPrice').val()) + parseInt($('#extraSessionPrice').val());
            var newPrice = price + (percent * 100 / price);
            $('#extraTimePrice').val(parseInt(newPrice - price));
            $('#price').html(addCommas(Math.round(newPrice)));
        });
        $('#date-dropdown').change(function(){
            var id = $(this).val();
            $('.option-time').hide();
            $('.option-time-'+id).show();
            $('#time-dropdown').val('');
        });

        function addCommas(nStr)
        {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }
    </script>
@endsection
