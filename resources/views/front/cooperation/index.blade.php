@extends('layouts.page')

@section('header')
    <link rel="stylesheet" href="{{ asset('css/croppie.css') }}" media="all">
    <style>
        #pac-input:focus {
            border: 1px solid #193e72;
        }
        .croppie-container {
            height: auto;
            margin: 10px auto;
        }
        .croppie-container .cr-boundary {
            padding: 10px;
        }
        .cr-image {
            font-size: 0;
        }
        .cr-slider-wrap {
            display: none;
        }
        #upload-image {
            display: none;
        }
        #Div-image {
            text-align: center;
            overflow: hidden;
        }
        #style_switcher a {
            color: #193e72;
        }

        .uk-file-upload {
            padding: 0;
        }
        .box-max-200 {
            max-width: 120px;
        }
        .slider-image {
            max-width: 235px;
            margin: 0 auto;
            border: 1px solid #000000;
        }
        .slider-image img {
            max-width: 100%;
            height: auto;
            display: block;
            margin: 0 auto;
            object-fit: cover;
        }
        .card{
            direction: rtl;
            text-align: right;
        }
        #Select_photo_croper{
            color: #193e72;
            font-size: 15px;
            cursor: pointer;
        }
        .span-error{
            color: #F00;
            font-size: 10px;
        }
        .card{
            direction: rtl;
            text-align: right;
        }
        .cr-boundary{
            border-radius: 50%;
        }
    </style>
    <script type="text/javascript">
        var onloadCallback = function() {
          grecaptcha.render('recaptcha', {
            'sitekey' : '6LfeqVsaAAAAAFeU0gbEARPyaxFGgcCjFK2Jjeq8'
          });
        };
    </script>
@endsection

@section('content')
<main id="main">
<!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs">
      <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    @if ($errors->has('msg'))
                        <span class="span-success">{{ $errors->first('msg') }}</span>
                        <br/>
                    @endif
                    @if ($errors->has('request_max'))
                        <span class="span-error">You can not request more than 3 times</span>
                        <br/>
                    @endif
                    <form id="cooperation" method="post" action="{{route('cooperation_save')}}">
                        @csrf
                            <div class="form-group">
                                <div id="Div-image">
                                    <div id="upload-demo" class="max-width-300"></div>
                                    <input type="hidden" id="image-code" name="image" value="">
                                    <input class="Upload_Croper" type="file" id="upload-image">
                                    <span id="Select_photo_croper">
                                        Select the avatar
                                        <i class="nav-icon fas fa-plus"></i>
                                    </span>
                                    @if ($errors->has('image'))
                                        <br/>
                                        <span class="span-error">{{ $errors->first('image') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="first_name" class="form-control @if ($errors->has('first_name')) is-invalid @endif" id="first_name" value="{{ old('first_name') }}" placeholder="First name*">
                                @if ($errors->has('first_name'))
                                    <span class="span-error">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" name="last_name" class="form-control @if ($errors->has('last_name')) is-invalid @endif" id="last_name" value="{{ old('last_name') }}" placeholder="Last name*">
                                @if ($errors->has('last_name'))
                                    <span class="span-error">{{ $errors->first('birthdate') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="date" name="birthdate" autocomplete="off" value="{{ old('birthdate') }}" class="form-control @if ($errors->has('birthdate')) is-invalid @endif">
                                @if ($errors->has('birthdate'))
                                    <span class="span-error">{{ $errors->first('birthdate') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioPrimary2" name="gender" value="male" checked>
                                    <label for="radioPrimary2"> Male
                                    </label>
                                </div>
                                <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioPrimary1" name="gender" value="female">
                                    <label for="radioPrimary1"> Female
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="mobile" class="form-control @if ($errors->has('mobile')) is-invalid @endif" id="mobile" placeholder="Mobile*" value="{{ old('mobile') }}">
                                @if ($errors->has('mobile'))
                                    <span class="span-error">{{ $errors->first('mobile') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control @if ($errors->has('email')) is-invalid @endif" id="email" placeholder="Email*" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                        <span class="span-error">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" name="expertise" class="form-control @if ($errors->has('expertise')) is-invalid @endif" id="expertise" placeholder="Expertise*" value="{{ old('expertise') }}">
                                @if ($errors->has('expertise'))
                                    <span class="span-error">{{ $errors->first('expertise') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <textarea name="description" class="form-control @if ($errors->has('description')) is-invalid @endif" id="description" placeholder="Description*">{{ old('description') }}</textarea>
                                @if ($errors->has('description'))
                                    <span class="span-error">{{ $errors->first('description') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <div id="recaptcha"></div>
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="span-error">{{ $errors->first('g-recaptcha-response') }}</span>
                                @endif
                                @if ($errors->has('g-recaptcha-response-1'))
                                    <span class="span-error">The captcha is invalid</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">submit</button>
                            </div>
                    </form>
                </div>
                <div class="col-md-4"></div>
            </div>
      </div>
    </section>
</main>
@endsection

@section('footer')

@endsection

@section('script')
        <script src="{{ asset('js/croppie.js') }}"></script>
        <script type="text/javascript">
            //start image 1
            $uploadCrop = $('#upload-demo').croppie({
                enableExif: true,
                viewport: {
                    width: 200,
                    height: 200,
                    type: 'circle'
                },
                boundary: {
                    width: 200,
                    height: 200
                }
            });

            $('#upload-image').on('change', function () {
                var Id = $(this).parent().attr('id');
                $('#'+Id+' .cr-viewport').css('background-image' , 'none');
                var reader = new FileReader();
                reader.onload = function (e) {
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function(){
                        GetCode();
                    });
                }
                reader.readAsDataURL(this.files[0]);
            });

            function GetCode() {
                $uploadCrop.croppie('result', {
                    type: 'canvas',
                    size: {
                        width: 200,
                        height: 200
                    },
                    format: "jpeg",
                    quality: '0.7'
                }).then(function (resp) {
                    $('#image-code').val(resp);
                });
            }

            $('#Select_photo_croper').click(function () {
                $('#upload-image').trigger('click');
            });

            $('.remove_photo_croper').on('click', function() {
                $('.cr-viewport').css('background-image' , 'none');
                $('.cr-image').attr('src', '');
                $('#upload-image').val('');
                $('#image-code').val('delete');
            });

            $('.cr-overlay').mouseup(function () {
                GetCode();
            });

            $('.cr-overlay').bind('mousewheel', function(e){
                GetCode();
            });
            setTimeout(function () {
                $('.cr-image').attr('src', '/media/user/avatar.png').css({'width': '200' , 'height': '200'});
            } , 300);

            $('#cooperation').submit(function(){
                $('#preloader').show();
            });
        </script>

        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit&hl=en"
            async defer>
        </script>
@endsection
