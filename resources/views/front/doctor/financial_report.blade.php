@extends('layouts.panel')

@section('header')

@endsection

@section('content')
        <div class="header-panel">
            <label>Financial report</label>
        </div>
        <div class="row col-12">
            <div class="col-md-6"><a href="{{ route('doctorProfile_financial_report_checkouts') }}"><button class="btn btn-info">Report checkouts</button></a></div>
            <div class="col-md-6"><a href="{{ route('doctorProfile_financial_report_orders') }}"><button class="btn btn-info">Report orders</button></a></div>
        </div>
@endsection

@section('footer')

@endsection

@section('script')
    <script>

    </script>
@endsection
