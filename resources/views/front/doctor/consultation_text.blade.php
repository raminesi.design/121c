@extends('layouts.panel')

@section('header')
    <link rel="stylesheet" href="{{ asset('css/chat.css') }}">
    <style>
        .hidden{
            display: none;
        }
    </style>
@endsection

@section('content')
        <div class="row col-12">
            <div class="col-12">
                <div id="Remaining">
                    <p>Remaining time : <span id="start_date"></span></p>
                </div>
                <div id="endConsultation" class="hidden col-12">
                    <a href="{{ route('consultation_endCall').'?channel='.$consultation->channel }}">
                        <button class="btn btn-danger">End</button>
                    </a>
                    <hr/>
                </div>
            </div>
            <div class="card card-bordered col-md-8">
                <div class="header-panel">
                    <label>Text</label>
                </div>
                <div class="ps-container ps-theme-default ps-active-y" id="chat-content" style="overflow-y: scroll !important; height:350px !important;">

                    @foreach($messageList as $key => $value)
                        @if($value['userId'] == $user->id)
                            <div class="media media-chat media-chat-reverse">
                                <div class="media-body">
                                    <p>{{ $value['message'] }}</p>
                                    <p class="meta"><time>{{ $value['date'] }}</time></p>
                                </div>
                            </div>
                        @else
                            <div class="media media-chat"> <img class="avatar" src="{{ url('media/user/'.$consultation->user->avatar) }}" alt="{{ $consultation->user->name }}" title="{{ $consultation->user->name }}">
                                <div class="media-body">
                                    <p>{{ $value['message'] }}</p>
                                    <p class="meta"><time>{{ $value['date'] }}</time></p>
                                </div>
                            </div>
                        @endif
                    @endforeach

                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                        <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; height: 0px; right: 2px;">
                        <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 2px;"></div>
                    </div>
                </div>
                <div class="publisher bt-1 border-light"> <img class="avatar avatar-xs" src="{{ url('media/doctor/' . $doctor->avatar) }}" alt="..."> <input class="publisher-input" id="message-box" type="text" placeholder="Write something"><i class="icofont-paper-plane send-btn" id="send-btn"></i><div class="spinner-border hide" id="loading-spinner"></div> </div>
            </div>
            <div class="card card-bordered col-md-4">
                <div class="header-panel">
                    <label>Files</label>
                    <a href="#"><span class="float-right" data-toggle="modal" data-target="#AddFile">Add file <i class="icofont-ui-add"></i></span></a>
                </div>
                <div class="ps-container ps-theme-default ps-active-y" id="files-content" style="overflow-y: scroll !important; height:350px !important;">
                    @foreach ($files as $file)
                    <div class="col-12 files">
                        <a target="_blank" href="{{ url('media/consultation/'.$file->file) }}"><strong>{{ $file->title }}</strong></a></br>
                        <p>{{ $file->description }}</p>
                        <p>{{ $file->created_at }}</p>
                        <hr/>
                    </div>
                   @endforeach
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="AddFile" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <form method="post" action="{{ route('doctorProfile_consultation_upload') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <input name="id" type="hidden" value="{{ $consultation->id }}">
                            <p>Add new file</p>
                            <div class="form-group">
                                <input name="title" class="form-control @if($errors->has('title')) is-invalid @endif" placeholder="title" value="{{ old('title') }}">
                                @if($errors->has('title'))
                                    <span class="span-error">{{ $errors->first('title') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <textarea name="description" class="form-control" placeholder="Description">{{ old('description') }}</textarea>
                            </div>
                            <div class="input-group">
                                <div class="custom-file col-12">
                                    <input type="file" name="file" class="custom-file-input @if($errors->has('file')) is-invalid @endif" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">File selection</label>
                                </div>
                            </div>
                            <div>
                                @if($errors->has('file'))
                                    <span class="span-error">{{ $errors->first('file') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
@endsection

@section('footer')

@endsection

@section('script')
    <script>

        @if($errors->has([]))
            $('#AddFile').modal('toggle');
        @endif

        $("#chat-content").animate({ scrollTop: $('#chat-content').prop("scrollHeight")}, 1000);
        $('#message-box').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                $("#send-btn").trigger("click");
            }
        });
        $('#send-btn').click(function(){
            var message = $('#message-box').val();
            if(message != ''){
                $('#message-box').val('');
                $('#loading-spinner').show();
                $('#send-btn').hide();
                $.ajax({
                    type:'POST',
                    url:'{{route('consultation_send_message')}}',
                    data:{_token: "{{ csrf_token()}}" , id: {{ $consultation->id }} , userId: {{ $user->id }} , receiver: {{ $consultation->user->id }} , message: message},
                    success: function( time ) {
                        $('#chat-content').append('<div class="media media-chat media-chat-reverse"><div class="media-body"><p>'+message+'</p><p class="meta"><time>'+time+'</time></p></div></div>');
                        $("#chat-content").animate({ scrollTop: $('#chat-content').prop("scrollHeight")}, 1000);
                        $('#loading-spinner').hide();
                        $('#send-btn').show();
                    }
                });
            }
        });

        // Set the date we're counting down to
        var countDownDate = new Date("{{ $consultation->date.' '.$consultation->visitStart }}").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("start_date").innerHTML = days + "d " + hours + "h "
        + minutes + "m " + seconds + "s ";

        // If the count down is finished, write some text
        if (distance < 300000) {
            $('#endConsultation').removeClass('hidden');
        }
        if (distance < 0) {
            clearInterval(x);
            $('#Remaining').addClass('hidden');
            window.location.replace("{{ route('consultation_endCall').'?channel='.$consultation->channel }}");
            //document.getElementById("start_date").innerHTML = "EXPIRED";
        }
        }, 1000);

    </script>
@endsection
