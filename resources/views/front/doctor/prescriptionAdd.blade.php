@extends('layouts.panel')

@section('header')
        <style>
            #addItem{
                color: rgb(65, 207, 8);
                cursor: pointer;
            }
        </style>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
        <div class="header-panel">
            <label>Add prescription</label>
        </div>

        <form method="post" action="{{route('doctorProfile_prescription_save')}}">
            @csrf
            <div class="row">
                <input name="id" type="hidden" value="{{ $consultation->id }}">
                <div class="form-group col-12">
                    <label>Description*</label>
                    <textarea required name="description" class="form-control"></textarea>
                </div>
                <div class="form-group col-12">Items<hr/></div>

                <div class="row col-12" id="itemList" data-item="0">
                    <div class="form-group col-md-4">
                        <label>Name*</label>
                        <input required name="name[0]" class="form-control nameItem">
                    </div>
                    <div class="form-group col-md-8">
                        <label>Consumption*</label>
                        <input required name="consumption[0]" class="form-control">
                    </div>
                </div>
                <div class="form-group col-12"><span id="addItem"> Add item +</span></div>
                <div class="form-group col-12">
                    <button type="submit" class="btn btn-success">submit</button>
                </div>
            </div>
        </form>
@endsection

@section('footer')
    <script src="{{ asset('admin/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection

@section('script')
    <script type="text/javascript">
        $(function () {
            $('#addItem').click(function(){
                var item = ($('#itemList').attr('data-item') * 1) + 1;
                $('#itemList').append('<div class="form-group col-md-4"><label>Name*</label><input required name="name['+item+']" class="form-control nameItem"></div><div class="form-group col-md-8"><label>Consumption*</label><input required name="consumption['+item+']" class="form-control"></div>');
                $('#itemList').attr('data-item' , item);
                $(".nameItem").autocomplete({
                    source: function (request, response) {
                        console.log(request);
                        var url     = "{{route('doctorProfile_prescription_searchAjax')}}";
                        // Fetch data
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: "json",
                            headers: {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')},
                            data: {
                                _token: "{{ csrf_token()}}",
                                search: request.term
                            },
                            success: function (data) {
                                response(data);
                                console.log(data);
                                if(data.length == 0){
                                    $('#error-empty').show();
                                }else{
                                    $('#error-empty').hide();
                                }
                            }
                        });
                    },
                    select: function (event, ui) {
                        // Set selection
                        //console.log(ui.item.value);
                        $(this).val(ui.item.value); // display the selected text
                        //$('#hiddenUser').val(ui.item.value); // display the selected text
                        return false;
                    }
                });
            });

            $(".nameItem").autocomplete({
                source: function (request, response) {
                    console.log(request);
                    var url     = "{{route('doctorProfile_prescription_searchAjax')}}";
                    // Fetch data
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: "json",
                        headers: {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')},
                        data: {
                            _token: "{{ csrf_token()}}",
                            search: request.term
                        },
                        success: function (data) {
                            response(data);
                            console.log(data);
                            if(data.length == 0){
                                $('#error-empty').show();
                            }else{
                                $('#error-empty').hide();
                            }
                        }
                    });
                },
                select: function (event, ui) {
                    // Set selection
                    //console.log(ui.item.value);
                    $(this).val(ui.item.value); // display the selected text
                    //$('#hiddenUser').val(ui.item.value); // display the selected text
                    return false;
                }
            });
        });
    </script>
@endsection
