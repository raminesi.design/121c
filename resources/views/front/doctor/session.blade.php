@extends('layouts.panel')

@section('header')

@endsection

@section('content')
        <div class="header-panel">
            <label>Next session</label>
        </div>
        <form action="{{ route('doctorProfile_consultation_nextSession_save') }}" method="post" role="form">
            @csrf
            <input name="id" value="{{ $request->id }}" type="hidden">
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Day</label>
                    <select name="date" id="date-dropdown" class="form-control">
                        <option value="">--select day--</option>
                        @foreach($Schedule as $value)
                            <option value="{{ $value['date'] }}">{{ $value['name'] }} ({{ $value['date'] }})</option>
                        @endforeach
                    </select>
                    @if ($errors->has('date'))
                        <div class="validate">Please select a day</div>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label>Time</label>
                    <select name="time" id="time-dropdown" class="form-control">
                        <option value="">--select time--</option>
                        @foreach($Schedule as $key => $value)
                            @foreach($value['times'] as $val)
                                <option class="option-time option-time-{{ $value['date'] }}" value="{{ $val['id'] }}">{{ $val['title'] }}</option>
                            @endforeach
                        @endforeach
                    </select>
                    @if ($errors->has('time'))
                        <div class="validate">Please select a time</div>
                    @endif
                </div>

            </div>
            <div><button class="btn btn-success" type="submit">Submit</button></div>
        </form>
@endsection

@section('footer')

@endsection

@section('script')
  <script>
    $('#date-dropdown').change(function(){
        var id = $(this).val();
        $('.option-time').hide();
        $('.option-time-'+id).show();
        $('#time-dropdown').val('');
    });
  </script>
@endsection
