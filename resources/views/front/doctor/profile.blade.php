@extends('layouts.panel')

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
        <div class="header-panel">
            <label>Latest consultation</label>
        </div>
        <table id="DataTable" class="table table-bordered table-hover">
            <thead>
                <tr>
                <th>#</th>
                <th>Num</th>
                <th>Date & Time</th>
                <th>Ill</th>
                <th>Subject</th>
                <th>Status</th>
                <th>Tools</th>
                </tr>
            </thead>
            <tbody>
                @foreach ( $consultation as $order)
                    <tr>
                        <td>{{  $loop->iteration + $consultation->firstItem() - 1}}</td>
                        <td>{{  (is_null($order->order) ? $order->parent->order->orderNumber : $order->order->orderNumber)  }}</td>
                        <td>{{  $order->date }}<br/>{{ $order->day->title }}<br/>{{ $order->visitStart  }}</td>
                        <td><img src="{{ url('media/user/'.$order->user->avatar) }}" class="img-circle elevation-2 avatar"><br/>{{  $order->user->name  }}</td>
                        <td>{{  $order->expertise->title  }}</td>
                        <td class="status-{{ $order->status }}">{{ $order->status }}</td>
                        <td>
                            <a href="{{ route('doctorProfile_consultation_info') }}?id={{ $order->id }}">
                                <i class="icofont-info-circle tools-btn edit-btn" title="Information"></i>
                            </a>
                            @if($order->status == 'done')
                            <a href="{{ route('consultation_report') }}?id={{ $order->id }}">
                                <i class="icofont-list tools-btn @if(is_null($order->report)) edit-btn @else status-accepted @endif" title="Report"></i>
                            </a>
                            @endif
                            @if($order->sessions > 1 && $order->status == 'done' && is_null($order->childeId))
                            <a href="{{ route('doctorProfile_consultation_nextSession') }}?id={{ $order->id }}">
                                <i class="icofont-plus tools-btn edit-btn" title="Next session"></i>
                            </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="table-buttons">
            <div>
                {{ $consultation->appends(request()->except('page'))->links() }}
            </div>
        </div>
@endsection

@section('footer')
    <!-- DataTables -->
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('script')
<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });

    });
  </script>
@endsection
