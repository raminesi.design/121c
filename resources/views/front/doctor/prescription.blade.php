@extends('layouts.panel')

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
        <div class="header-panel">
            <label>Prescription</label>
        </div>
        <table id="DataTable" class="table table-bordered table-hover">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Consumption</th>
                <th>Delete</th>
            </tr>
        @foreach ($prescription->prescription->details as $details)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{ $details->name }}</td>
            <td>{{ $details->description }}</td>
            <td><a class="status-rejected" href="{{ route('doctorProfile_prescription_deleteItem').'?id='.$details->id }}"><i class="icofont-trash"></i></a></td>
        </tr>
        @endforeach
        </table>
        <p>Description:  {{ $prescription->prescription->description }}</p>
        <hr/>
        <p> <a class="status-rejected" href="{{ route('doctorProfile_prescription_delete').'?id='.$prescription->prescription->id }}">Delete the prescription</a> </p>
@endsection

@section('footer')
<!-- DataTables -->
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('script')
    <script>
        $(function () {
            $('#DataTable').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endsection
