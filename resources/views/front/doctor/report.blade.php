@extends('layouts.panel')

@section('header')
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=twww1hnqtso2i9wm65hz6r8lkzj1i7ihdo31cbvlh4qtqflj"></script>
@endsection

@section('content')
        <div class="header-panel">
            <label>Report</label>
        </div>
        <form method="post" action="{{ route('consultation_report_save') }}">
            @csrf
            <div class="row">
                <input name="id" type="hidden" value="{{ $consultation->id }}">
                <div class="form-group col-12">
                    <label>Report text*</label>
                    <textarea id="report" name="report">@if(old('report')){{ old('report') }}@else{{ $consultation->report }}@endif</textarea>
                    @if ($errors->has('report'))
                            <span class="span-error">{{ $errors->first('report') }}</span>
                    @endif
                </div>
                <div class="form-group col-12">
                    <button type="submit" class="btn btn-success">submit</button>
                </div>
            </div>
        </form>
@endsection

@section('footer')

@endsection

@section('script')
    <script>
        tinymce.init({
            selector:'#report' ,
            plugins: 'link image',
            menubar: '',
            default_link_target: '_blank',
            directionality :"ltr"
        });
    </script>
@endsection
