@extends('layouts.panel')

@section('header')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js" integrity="sha512-f0VlzJbcEB6KiW8ZVtL+5HWPDyW1+nJEjguZ5IVnSQkvZbwBt2RfCBY0CBO1PsMAqxxrG4Di6TfsCPP3ZRwKpA==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.js" integrity="sha512-tCkLWlSXiiMsUaDl5+8bqwpGXXh0zZsgzX6pB9IQCZH+8iwXRYfcCpdxl/owoM6U4ap7QZDW4kw7djQUiQ4G2A==" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.css" integrity="sha512-SZgE3m1he0aEF3tIxxnz/3mXu/u/wlMNxQSnE0Cni9j/O8Gs+TjM9tm1NX34nRQ7GiLwUEzwuE3Wv2FLz2667w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css" integrity="sha512-3q8fi8M0VS+X/3n64Ndpp6Bit7oXSiyCnzmlx6IDBLGlY5euFySyJ46RUlqIVs0DPCGOypqP8IRk/EyPvU28mQ==" crossorigin="anonymous" />
    <style>
        #ex1Slider .slider-selection {
            background: #BABABA;
        }
        .price_slider{
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="header-panel">
        <label>Selection and pricing</label>
    </div>

    <form method="post" action="{{ route('doctorProfile_expertise_save') }}">
        @csrf
        <div class="row">
            <div class="form-group col-12">
                <label>Expertise</label>
                <select class="form-control" id="expertise" name="expertise">
                    @foreach($allExpertise as $key => $value)
                        <option @if($exp['id'] == $value->id) selected @endif value="{{ $value->id }}" id="op-{{ $value->id }}">{{ $value->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-12">
                <hr/>
                <label><input value="1" type="checkbox" @if($exp['price_video_status']) checked @endif id="price_video" name="price_video_status" class="status_price" /> Video call price</label>
                <div class="price_slider" id="price_video_slider_box" @if($exp['price_video_status']) style="display: block" @endif>
                    <span id="pvimi">{{ $priceRange->price_video_min }}</span> $ <input name="price_video" id="price_video_slider" data-slider-id='ex1Slider' type="text" data-slider-min="{{ $priceRange->price_video_min }}" data-slider-max="{{ $priceRange->price_video_max }}" data-slider-step="1" data-slider-value="{{ $exp['price_video'] }}"/><span id="pvima">{{ $priceRange->price_video_max }}</span> $
                </div>
                <hr/>
            </div>

            <div class="form-group col-12">
                <label><input value="1" type="checkbox" @if($exp['price_voice_status']) checked @endif id="price_voice" name="price_voice_status" class="status_price" /> Voice call price</label>
                <div class="price_slider" id="price_voice_slider_box" @if($exp['price_voice_status']) style="display: block" @endif>
                    <span id="pvomi">{{ $priceRange->price_voice_min }}</span> $ <input name="price_voice" id="price_voice_slider" data-slider-id='ex1Slider' type="text" data-slider-min="{{ $priceRange->price_voice_min }}" data-slider-max="{{ $priceRange->price_voice_max }}" data-slider-step="1" data-slider-value="{{ $exp['price_voice'] }}"/><span id="pvoma">{{ $priceRange->price_voice_max }}</span> $
                </div>
                <hr/>
            </div>

            <div class="form-group col-12">
                <label><input value="1" type="checkbox" @if($exp['price_text_status']) checked @endif id="price_text" name="price_text_status" class="status_price" /> Text chat price</label>
                <div class="price_slider" id="price_text_slider_box" @if($exp['price_text_status']) style="display: block" @endif>
                    <span id="ptemi">{{ $priceRange->price_text_min }}</span> $ <input name="price_text" id="price_text_slider" data-slider-id='ex1Slider' type="text" data-slider-min="{{ $priceRange->price_text_min }}" data-slider-max="{{ $priceRange->price_text_max }}" data-slider-step="1" data-slider-value="{{ $exp['price_text'] }}"/><span id="ptema">{{ $priceRange->price_text_max }}</span> $
                </div>
                <hr/>
            </div>

            <div class="form-group col-12">
                <button type="submit" class="btn btn-success">submit</button>
            </div>
        </div>
    </form>

@endsection

@section('footer')

@endsection

@section('script')
    <script>
        new Slider('#price_video_slider', {});
        new Slider('#price_voice_slider', {});
        new Slider('#price_text_slider', {});

        $('.status_price').change(function(){
            var id = $(this).attr('id');
            var val = this.checked;
            if(val){
                $('#'+id+'_slider_box').show();
            }else{
                $('#'+id+'_slider_box').hide();
            }
        });

        $('#expertise').change(function(){
            $('.status_price').prop('checked', false);
            $('.price_slider').hide();
            var id = $(this).val();
            window.location.replace("{{ route('doctorProfile_expertise').'?id=' }}"+id);
        });

    </script>
@endsection
