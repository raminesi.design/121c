@extends('layouts.panel')

@section('header')

@endsection

@section('content')
    <div class="header-panel">
        <label>Schedule</label>
    </div>

    <table class="table-bordered col-12 table-hover" style="text-align: center;">
        <head>
            <tr>
                <th>#</th>
                <th class="day pointer" data-status="0" data-id="1">Monday</th>
                <th class="day pointer" data-status="0" data-id="2">Tuesday</th>
                <th class="day pointer" data-status="0" data-id="3">Wednesday</th>
                <th class="day pointer" data-status="0" data-id="4">Thursday</th>
                <th class="day pointer" data-status="0" data-id="5">Friday</th>
                <th class="day pointer" data-status="0" data-id="6">Saturday</th>
                <th class="day pointer" data-status="0" data-id="7">Sunday</th>
            </tr>
        </head>
        <body>
            @foreach ($times as $time)
            <tr>
                <td class="time pointer" data-status="0" data-id="{{ $time->id }}">{{ $time->title }}</td>
                @for($i = 1 ; $i < 8 ; $i++)
                    <td class="custom-checkbox"><input class="custom-control-input checkbox-times day-{{ $i }} time-{{ $time->id }}" id="ch-{{ $i }}-{{ $time->id }}" value="{{$i}}-{{$time->id}}" type="checkbox" @if($time->status == 'disable') disabled @endif @if(array_search( $i."-".$time->id , $arraySchedule) !== false) checked data-status="1" @else data-status="0" @endif><label for="ch-{{ $i }}-{{ $time->id }}" class="custom-control-label" ></label></td>
                @endfor
            </tr>
        @endforeach
        </body>
    </table>
@endsection

@section('footer')

@endsection

@section('script')
    <script>
        $('.day').click(function(){
            var id = $(this).attr('data-id');
            var status = $(this).attr('data-status');
            if(status == 0){
                $('.day-'+id).prop('checked', true);
                $(this).attr('data-status' , 1);
            }else{
                $('.day-'+id).prop('checked', false);
                $(this).attr('data-status' , 0);
            }
            var doctor_id = {{ $doctor->id }};
            $.ajax({
                type:'POST',
                url:'{{route('doctorProfile_all_time')}}',
                data:{_token: "{{ csrf_token()}}" , doctor_id: doctor_id , day_id: id , status: status},
                success: function( msg ) {
                    console.log(msg);
                }
            });
        });

        $('.time').click(function(){
            var id = $(this).attr('data-id');
            var status = $(this).attr('data-status');
            if(status == 0){
                $('.time-'+id).prop('checked', true);
                $(this).attr('data-status' , 1);
            }else{
                $('.time-'+id).prop('checked', false);
                $(this).attr('data-status' , 0);
            }
            var doctor_id = {{ $doctor->id }};
            $.ajax({
                type:'POST',
                url:'{{route('doctorProfile_all_day')}}',
                data:{_token: "{{ csrf_token()}}" , doctor_id: doctor_id , time_id: id , status: status},
                success: function( msg ) {
                    console.log(msg);
                }
            });
        });

        $('.checkbox-times').change(function(){
            var doctor_id = {{ $doctor->id }};
            var val = $(this).val().split('-');
            console.log(val);
            $.ajax({
                type:'POST',
                url:'{{route('doctorProfile_time')}}',
                data:{_token: "{{ csrf_token()}}" , doctor_id: doctor_id , day_id: val[0] , time_id: val[1]},
                success: function( msg ) {
                    console.log(msg);
                }
            });
        });
    </script>
@endsection
