@extends('layouts.panel')

@section('header')
    <style>
        .form-control.is-invalid{
            background-position: left calc(.375em + .1875rem) center !important;
        }
        .span-error{
            color: #F00;
            font-size: 10px;
        }
        .card{
            direction: rtl;
            text-align: right;
        }
        .cr-boundary{
            border-radius: 50%;
        }
    </style>
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=twww1hnqtso2i9wm65hz6r8lkzj1i7ihdo31cbvlh4qtqflj"></script>

@endsection

@section('content')
    <div class="header-panel">
        <label>New message</label>
    </div>
    <form method="post" action="{{route('profile_ticket_save')}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Subject*</label>
                    <input type="text" name="subject" class="form-control" id="subject" value="{{ old('subject') }}" placeholder="Subject">
                    @if ($errors->has('subject'))
                        <span class="span-error">{{ $errors->first('subject') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>File</label>
                    <div class="custom-file col-12">
                        <input type="file" name="file" class="custom-file-input @if($errors->has('file')) is-invalid @endif" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">File selection</label>
                    </div>
                    @if ($errors->has('file'))
                        <span class="span-error">{{ $errors->first('file') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Text*</label>
                    <textarea id="textTicket" name="text" class="form-control" id="text">{{ old('text') }}</textarea>
                    @if ($errors->has('text'))
                        <span class="span-error">{{ $errors->first('text') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group col-12">
                <button type="submit" class="btn btn-success">submit</button>
            </div>
        </div>
    </form>
@endsection

@section('footer')

@endsection

@section('script')
        <script>
            tinymce.init({
                selector:'#textTicket' ,
                menubar: 'insert',
                default_link_target: '_blank',
                directionality :"ltr"
            });
        </script>
@endsection
