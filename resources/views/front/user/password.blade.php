@extends('layouts.panel')

@section('header')

@endsection

@section('content')
    <div class="header-panel">
        <label>Change Password</label>
    </div>
    <form method="post" action="{{route('profile_password_save')}}">
        @csrf
        <div class="row">
            <div class="form-group col-12">
                <label>Password* <span id="randomPassword">Random <i class="fas fa-key"></i></span></label>
                <input type="text" name="password" class="form-control" id="password" placeholder="Password">
                @if ($errors->has('password'))
                    <span class="span-error">{{ $errors->first('password') }}</span>
                @endif
            </div>
                @if ($errors->has('msg'))
                    <div class="form-group col-12">
                        <span class="span-success">{{ $errors->first('msg') }}</span>
                    </div>
                @endif
            <div class="form-group col-12">
                <button type="submit" class="btn btn-success">submit</button>
            </div>
        </div>
    </form>
@endsection

@section('footer')

@endsection

@section('script')
    <script>
        function makeid(length) {
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@$';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
               result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
         }

         $('#randomPassword').click(function(){
             var password = makeid(8);
             $('#password').val(password);
         });
    </script>
@endsection
