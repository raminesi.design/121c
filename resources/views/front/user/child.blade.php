@extends('layouts.panel')

@section('header')
    <link rel="stylesheet" href="{{ asset('css/croppie.css') }}" media="all">
    <style>
        #pac-input:focus {
            border: 1px solid #193e72;
        }
        .croppie-container {
            height: auto;
            margin: 10px auto;
        }
        .croppie-container .cr-boundary {
            padding: 10px;
        }
        .cr-image {
            font-size: 0;
        }
        .cr-slider-wrap {
            display: none;
        }
        #upload-image {
            display: none;
        }
        #Div-image {
            text-align: center;
            overflow: hidden;
        }
        #style_switcher a {
            color: #193e72;
        }

        .uk-file-upload {
            padding: 0;
        }
        .box-max-200 {
            max-width: 120px;
        }
        .slider-image {
            max-width: 235px;
            margin: 0 auto;
            border: 1px solid #000000;
        }
        .slider-image img {
            max-width: 100%;
            height: auto;
            display: block;
            margin: 0 auto;
            object-fit: cover;
        }
        .card{
            direction: rtl;
            text-align: right;
        }
        #Select_photo_croper{
            color: #193e72;
            font-size: 15px;
            cursor: pointer;
        }
        .form-control.is-invalid{
            background-position: left calc(.375em + .1875rem) center !important;
        }
        .span-error{
            color: #F00;
            font-size: 10px;
        }
        .card{
            direction: rtl;
            text-align: right;
        }
        .cr-boundary{
            border-radius: 50%;
        }
    </style>
@endsection

@section('content')
    <div class="header-panel">
        <label>Settings</label>
    </div>
    <form method="post" action="{{route('profile_children_update' , [$child->user->id])}}">
        @csrf
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>First name*</label>
                    <input type="text" name="first_name" class="form-control" id="first_name" value="@if(old('first_name')){{ old('first_name') }}@else{{ $child->user->first_name }}@endif" placeholder="First name">
                    @if ($errors->has('first_name'))
                        <span class="span-error">{{ $errors->first('first_name') }}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label>Last name*</label>
                    <input type="text" name="last_name" class="form-control" id="last_name" value="@if(old('last_name')){{ old('last_name') }}@else{{ $child->user->last_name }}@endif" placeholder="Last name">
                    @if ($errors->has('last_name'))
                        <span class="span-error">{{ $errors->first('birthdate') }}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label>Birthdate*</label>
                    <input type="date" name="birthdate" autocomplete="off" value="{{ $child->user->birthdate }}" class="form-control">
                    @if ($errors->has('birthdate'))
                        <span class="span-error">{{ $errors->first('birthdate') }}</span>
                    @endif
                </div>
                <div class="form-group">
                    <div class="icheck-primary d-inline">
                        <input type="radio" id="radioPrimary2" name="gender" value="male" checked>
                        <label for="radioPrimary2"> Male
                        </label>
                    </div>
                    <div class="icheck-primary d-inline">
                        <input type="radio" id="radioPrimary1" @if($child->user->gender == 'female') checked @endif name="gender" value="female">
                        <label for="radioPrimary1"> Female
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <div id="Div-image">
                        <div id="upload-demo" class="max-width-300"></div>
                        <input type="hidden" id="image-code" name="image" value="">
                        <input class="Upload_Croper" type="file" id="upload-image">
                        <span id="Select_photo_croper">
                            Select the avatar
                            <i class="nav-icon fas fa-plus"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group col-12">
                <button type="submit" class="btn btn-success">submit</button>
            </div>
        </div>
    </form>
@endsection

@section('footer')

@endsection

@section('script')
        <script src="{{ asset('js/croppie.js') }}"></script>
        <script type="text/javascript">
            //start image 1
            $uploadCrop = $('#upload-demo').croppie({
                enableExif: true,
                viewport: {
                    width: 200,
                    height: 200,
                    type: 'circle'
                },
                boundary: {
                    width: 200,
                    height: 200
                }
            });

            $('#upload-image').on('change', function () {
                var Id = $(this).parent().attr('id');
                $('#'+Id+' .cr-viewport').css('background-image' , 'none');
                var reader = new FileReader();
                reader.onload = function (e) {
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function(){
                        GetCode();
                    });
                }
                reader.readAsDataURL(this.files[0]);
            });

            function GetCode() {
                $uploadCrop.croppie('result', {
                    type: 'canvas',
                    size: {
                        width: 200,
                        height: 200
                    },
                    format: "jpeg",
                    quality: '0.7'
                }).then(function (resp) {
                    $('#image-code').val(resp);
                });
            }

            $('#Select_photo_croper').click(function () {
                $('#upload-image').trigger('click');
            });

            $('.remove_photo_croper').on('click', function() {
                $('.cr-viewport').css('background-image' , 'none');
                $('.cr-image').attr('src', '');
                $('#upload-image').val('');
                $('#image-code').val('delete');
            });

            $('.cr-overlay').mouseup(function () {
                GetCode();
            });

            $('.cr-overlay').bind('mousewheel', function(e){
                GetCode();
            });
            setTimeout(function () {
                $('.cr-image').attr('src', '/media/user/{{ $child->user->avatar }}').css({'width': '200' , 'height': '200'});
            } , 300);
        </script>
@endsection
