@extends('layouts.panel')

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
        <div class="header-panel">
            <label>Support</label>
            <span class="float-right"><a href="{{ route('profile_ticket_add') }}">New <i class="icofont-ui-add"></i></a></span>
        </div>
        <table id="DataTable" class="table table-bordered table-hover">
            <thead>
                <tr>
                <th>#</th>
                <th>Subject</th>
                <th>Status</th>
                <th>Creation date</th>
                  <th>Update date</th>
                <th>Tools</th>
                </tr>
            </thead>
            <tbody>
                @foreach ( $tickets as $ticket)
                    <tr>
                        <td>{{  $loop->iteration + $tickets->firstItem() - 1}}</td>
                        <td>{{  $ticket->subject  }}</td>
                        <td>{{  $ticket->status  }}</td>
                        <td>{{  $ticket->created_at  }}</td>
                        <td>{{  $ticket->updated_at  }}</td>
                        <td>
                            <a href="{{ route('profile_ticket') }}?id={{ $ticket->id }}">
                                <i class="icofont-info-circle tools-btn edit-btn" title="Information"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="table-buttons">
            <div>
                {{ $tickets->appends(request()->except('page'))->links() }}
            </div>
        </div>
@endsection

@section('footer')
    <!-- DataTables -->
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('script')
    <script>
        $(function () {
            $('#DataTable').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": false,
                "responsive": true,
            });

        });
    </script>
@endsection
