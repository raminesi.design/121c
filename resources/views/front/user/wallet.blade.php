@extends('layouts.panel')

@section('header')

@endsection

@section('content')
        <div class="header-panel">
            <label>Financial settlement</label>
            <span class="float-right"><a href="">Charge +</a></span>
        </div>

        @if($user->wallet->credit > 0)
        <hr/>
        @if(count($banks) > 0)
        
        <form method="post" action="{{ route('profile_checkout_new') }}">
            @csrf
            <input name="userType" value="user" type="hidden">
            <div class="row col-12">
                <div class="form-group col-sm-6">
                    <label>Bank</label>
                    <select name="bank" class="form-control">
                        @foreach ($banks as $bank)
                            <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-12">
                    <button type="submit" class="btn btn-info">Checkout</button>
                </div>
            </div>
        </form>

        @else
        <label>Please add a bank account first: <a href="{{ route('profile_bank_add') }}">Add bank</a></label>
        @endif
        <hr/>
        @endif

        <table id="DataTable" class="table table-bordered table-hover">
            <thead>
                <tr>
                <th>#</th>
                <th>Amount ($)</th>
                <th>Date of request</th>
                <th>Date of review</th>
                <th>Status</th>
                <th>Description</th>
                <th>file</th>
                </tr>
            </thead>
            <tbody>
                @foreach($checkouts as $key => $value)
                <tr>
                    <td>{{$loop->iteration + $checkouts->firstItem() - 1}}</td>
                    <td>{{ number_format($value->amount)  }}</td>
                    <td>{{ $value->created_at  }}</td>
                    <td>{{ $value->date  }}</td>
                    <td class="{{ $value->status  }}">{{ $value->status  }}</td>
                    <td>{{ $value->description  }}</td>
                    <td>
                        @if(!is_null($value->file))
                            <a target="_blank" href="{{ url('media/checkout/'.$value->file) }}"><i class="icofont-download"></i></a>
                        @else - @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="table-buttons">
            <div>
                {{ $checkouts->appends(request()->except('page'))->links() }}
            </div>
        </div>
@endsection

@section('footer')

@endsection

@section('script')

@endsection
