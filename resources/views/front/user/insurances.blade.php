@extends('layouts.panel')

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
        <div class="header-panel">
            <label>Insurances</label>
            <span class="float-right"><a href="{{ route('profile_insurance_add') }}">Add <i class="icofont-ui-add"></i></a></span>
        </div>

        <table id="DataTable" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>User</th>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Tools</th>
                </tr>
            </thead>
            <tbody>
                @foreach($insurances as $key => $value)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>
                            <img src="{{ url('media/user/'.$value->user->avatar) }}" class="img-circle elevation-2 avatar"><br/>
                            {{ $value->user->name }}
                        </td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->code }}</td>
                        <td>
                            <a href="{{ route('profile_insurance_status').'?id='.$value->id }}">
                                @if($value->status == 'enable')
                                    <i class="icofont-check tools-btn status-enable" title="Enable"></i>
                                @else
                                    <i class="icofont-close tools-btn status-disable" title="Disable"></i>
                                @endif
                            </a>
                            <a href="{{ route('profile_insurance_delete').'?id='.$value->id }}">
                                <i class="icofont-trash tools-btn delete-btn" title="Delete"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

@endsection

@section('footer')
    <!-- DataTables -->
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('script')
<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });

    });
  </script>
@endsection
