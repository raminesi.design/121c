@extends('layouts.panel')

@section('header')
    <style>
        .hidden{
            display: none;
        }
    </style>
@endsection

@section('content')
        <div class="header-panel">
            <label>Consultation</label>
        </div>
        <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <!-- /.col -->
                <!-- /.col -->
                <div class="col-sm-6 invoice-col">
                    <address>
                        <img class="avatar" src="{{ url('media/doctor/'.$consultation->doctor->avatar) }}" /><br>
                        <b>Name:</b> {{ $consultation->doctor->user->name }}<br>
                        <b>Code:</b> {{ $consultation->doctor->code }}<br>
                    </address>
                  </div>
                <div class="col-sm-6 invoice-col">
                  <address>
                      <b>Expertise:</b> {{ $consultation->expertise->title }}<br>
                      <b>Type:</b> {{ $consultation->type }}<br>
                      <b>Date:</b> {{ $consultation->date }}<br>
                      <b>Day:</b> {{ $consultation->day->title }}<br>
                      <b>Start:</b> {{ $consultation->visitStart }}<br>
                      <b>Duration:</b> {{ $consultation->visitTime }} minutes<br>
                      <b>Number of sessions:</b> {{ $consultation->sessions }}<br>
                      {{-- @if($consultation->sessions > 1 && $consultation->status == 'done' && is_null($consultation->childeId))
                        <a href="{{ route('doctorProfile_consultation_nextSession').'?id='.$consultation->id }}"><button class="btn btn-success">Next session</button></a><br/>
                      @endif --}}
                      <b>Status:</b> <span class="status-{{ $consultation->status }}">{{ $consultation->status }}</span><br>
                      @if(!is_null($consultation->prescription))
                        <a href="{{ route('profile_prescription').'?id='.$consultation->id }}">Prescription <i class="icofont-link"></i></a>
                      @endif
                  </address>
                </div>
                <div class="col-12 invoice-col">
                    @if($consultation->status == 'pending')
                        <hr/>
                            <button class="btn btn-danger" data-toggle="modal" data-target="#Reject">Cancellation</button></p>
                        <hr/>
                    @endif
                    @if($consultation->status == 'accepted')
                        <hr/>
                            <div id="Remaining">
                                <p>Remaining time : <span id="start_date"></span></p>
                            </div>
                        <hr/>
                    @endif
                    @if($consultation->status == 'doing')
                        <hr/>
                        <div id="Remaining">
                            <p>Remaining time : <span id="start_date"></span></p>
                        </div>
                        <div>
                            <a href="{{ route('profile_startCall').'?id='.$consultation->id }}">
                                <button class="btn btn-success">Join</button>
                            </a>
                        </div>
                        <hr/>
                    @endif
                    @if($consultation->status == 'rejected')
                        <hr/>
                            <p class="status-{{ $consultation->status }}">Description: {{ $consultation->description }}</p>
                        <hr/>
                    @endif
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <strong>File list</strong> <a href="#"><span class="float-right" data-toggle="modal" data-target="#AddFile">Add file <i class="icofont-ui-add"></i></span></a>
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>Title</th>
                      <th>Description</th>
                      <th>Sender</th>
                      <th>File</th>
                    </tr>
                    </thead>
                    <tbody>
                   @foreach ($files as $file)
                      <tr>
                          <td>{{$loop->iteration}}</td>
                          <td>{{ $file->title }}</td>
                          <td>{{ $file->description }}</td>
                          <td>{{ $file->sender }}</td>
                          <td><a target="_blank" href="{{ url('media/consultation/'.$file->file) }}"><i class="icofont-download"></i></a></td>
                      </tr>
                   @endforeach
                    </tbody>
                  </table>
                  <div class="table-buttons">
                    <div>
                        {{ $files->appends(request()->except('page'))->links() }}
                    </div>
                </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->

          <!-- Modal -->
        <div id="AddFile" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <form method="post" action="{{ route('profile_consultation_upload') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <input name="id" type="hidden" value="{{ $consultation->id }}">
                            <p>Add new file</p>
                            <div class="form-group">
                                <input name="title" class="form-control @if($errors->has('title')) is-invalid @endif" placeholder="title" value="{{ old('title') }}">
                                @if($errors->has('title'))
                                    <span class="span-error">{{ $errors->first('title') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <textarea name="description" class="form-control" placeholder="Description">{{ old('description') }}</textarea>
                            </div>
                            <div class="input-group">
                                <div class="custom-file col-12">
                                    <input type="file" name="file" class="custom-file-input @if($errors->has('file')) is-invalid @endif" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">File selection</label>
                                </div>
                            </div>
                            <div>
                                @if($errors->has('file'))
                                    <span class="span-error">{{ $errors->first('file') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <!-- Modal -->
        <div id="Reject" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <form method="post" action="{{ route('profile_consultation_cancellation') }}">
                        @csrf
                        <div class="modal-body">
                            <input name="id" type="hidden" value="{{ $consultation->id }}">
                            <p>Are you sure you want to cancel the request?</p>
                            <div class="form-group">
                                <textarea name="description" class="form-control" placeholder="Description"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Yes</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

@endsection

@section('footer')

@endsection

@section('script')
    <script>

        @if($errors->has([]))
            $('#AddFile').modal('toggle');
        @endif

        // Set the date we're counting down to
        var countDownDate = new Date("{{ $consultation->date.' '.$consultation->visitStart }}").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("start_date").innerHTML = days + "d " + hours + "h "
        + minutes + "m " + seconds + "s ";

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            $('#Remaining').addClass('hidden');
            //document.getElementById("start_date").innerHTML = "EXPIRED";
        }
        }, 1000);

    </script>
@endsection
