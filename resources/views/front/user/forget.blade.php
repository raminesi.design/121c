@extends('layouts.page')

@section('content')
<main id="main">
<!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs">
      <div class="container">
          <div class="row">

            <div class="col-md-4"></div>
            <div class="col-md-4">
                <form action="{{ route('sendRecoveryLink') }}" method="post" role="form" class="login-form">
                    @csrf
                    <img class="logo-login" width="80px" src="{{ asset('assets/img/Logo-fav.svg') }}">
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Please enter a valid email" />
                        @if ($errors->has('email'))
                            <div class="validate">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <div class="mb-3">
                        @if ($message = Session::get('error'))
                            <span class="validate">{{ $message }}</span>
                        @endif
                        @if ($message = Session::get('success'))
                            <span class="span-success">{{ $message }}</span>
                        @endif
                    </div>
                    <div class="text-center"><button class="btn btn-info" type="submit">Password recovery</button></div>
                </form>
            </div>
            <div class="col-md-4"></div>

          </div>
      </div>
    </section>

</main>
@endsection
@section('script')

@endsection
