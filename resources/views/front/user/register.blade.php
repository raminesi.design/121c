@extends('layouts.page')

@section('header')
    <link rel="stylesheet" href="{{ asset('css/croppie.css') }}" media="all">
    <style>
        #pac-input:focus {
            border: 1px solid #193e72;
        }
        .croppie-container {
            height: auto;
            margin: 10px auto;
        }
        .croppie-container .cr-boundary {
            padding: 10px;
        }
        .cr-image {
            font-size: 0;
        }
        .cr-slider-wrap {
            display: none;
        }
        #upload-image {
            display: none;
        }
        #Div-image {
            text-align: center;
            overflow: hidden;
        }
        #style_switcher a {
            color: #193e72;
        }

        .uk-file-upload {
            padding: 0;
        }
        .box-max-200 {
            max-width: 120px;
        }
        .slider-image {
            max-width: 235px;
            margin: 0 auto;
            border: 1px solid #000000;
        }
        .slider-image img {
            max-width: 100%;
            height: auto;
            display: block;
            margin: 0 auto;
            object-fit: cover;
        }
        .card{
            direction: rtl;
            text-align: right;
        }
        #Select_photo_croper{
            color: #193e72;
            font-size: 15px;
            cursor: pointer;
        }
        .form-control.is-invalid{
            background-position: left calc(.375em + .1875rem) center !important;
        }
        .span-error{
            color: #F00;
            font-size: 10px;
        }
        .card{
            direction: rtl;
            text-align: right;
        }
        .cr-boundary{
            border-radius: 50%;
        }
        .form-control.is-invalid{
            background-position: left calc(.375em + .1875rem) center !important;
        }
        .span-error{
            color: #F00;
            font-size: 10px;
        }
    </style>

@endsection

@section('content')
<main id="main">
<!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs">
      <div class="container">
          <div class="row">

            <div class="col-md-4"></div>
            <div class="col-md-4">
                <form action="{{ route('register') }}" method="post" role="form" class="login-form">
                    @csrf
                    <div class="form-group">
                        <div id="Div-image">
                            <div id="upload-demo" class="max-width-300"></div>
                            <input type="hidden" id="image-code" name="image" value="">
                            <input class="Upload_Croper" type="file" id="upload-image">
                            <span id="Select_photo_croper">
                                Select the avatar
                                <i class="nav-icon fas fa-plus"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First name" value="{{ old('first_name') }}" />
                        @if ($errors->has('first_name'))
                            <div class="validate">{{ $errors->first('first_name') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last name" value="{{ old('last_name') }}" />
                        @if ($errors->has('last_name'))
                            <div class="validate">{{ $errors->first('last_name') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="date" name="birthdate" autocomplete="off" id="birthdate" value="{{ old('birthdate') }}" class="form-control">
                        @if ($errors->has('birthdate'))
                            <div class="validate">{{ $errors->first('birthdate') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <select name="country" class="form-control">
                                <option value="">- Choose your country -</option>
                                @foreach($country as $key => $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                        </select>
                        @if ($errors->has('country'))
                            <div class="validate">Choose your country</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{ old('email') }}" />
                        @if ($errors->has('email'))
                            <div class="validate">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password" data-rule="minlen:8" data-msg="Please enter at least 8 chars" />
                        @if ($errors->has('password'))
                            <div class="validate">{{ $errors->first('password') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="password" name="confirmPassword" class="form-control" id="confirmPassword" placeholder="Confirm password" />
                        @if ($errors->has('confirmPassword'))
                            <div class="validate">The minimum number of characters must be 8</div>
                        @endif
                    </div>
                    <div class="mb-3">
                        @if ($message = Session::get('error'))
                            <span class="validate">{{ $message }}</span>
                        @endif
                    </div>
                    <div class="text-center"><button class="btn btn-info" type="submit">Register</button></div>
                </form>
            </div>
            <div class="col-md-4"></div>

          </div>
      </div>
    </section>

</main>
@endsection
@section('script')
<script src="{{ asset('js/croppie.js') }}"></script>
        <script type="text/javascript">
            //start image 1
            $uploadCrop = $('#upload-demo').croppie({
                enableExif: true,
                viewport: {
                    width: 200,
                    height: 200,
                    type: 'circle'
                },
                boundary: {
                    width: 200,
                    height: 200
                }
            });

            $('#upload-image').on('change', function () {
                var Id = $(this).parent().attr('id');
                $('#'+Id+' .cr-viewport').css('background-image' , 'none');
                var reader = new FileReader();
                reader.onload = function (e) {
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function(){
                        GetCode();
                    });
                }
                reader.readAsDataURL(this.files[0]);
            });

            function GetCode() {
                $uploadCrop.croppie('result', {
                    type: 'canvas',
                    size: {
                        width: 200,
                        height: 200
                    },
                    format: "jpeg",
                    quality: '0.7'
                }).then(function (resp) {
                    $('#image-code').val(resp);
                });
            }

            $('#Select_photo_croper').click(function () {
                $('#upload-image').trigger('click');
            });

            $('.remove_photo_croper').on('click', function() {
                $('.cr-viewport').css('background-image' , 'none');
                $('.cr-image').attr('src', '');
                $('#upload-image').val('');
                $('#image-code').val('delete');
            });

            $('.cr-overlay').mouseup(function () {
                GetCode();
            });

            $('.cr-overlay').bind('mousewheel', function(e){
                GetCode();
            });
            setTimeout(function () {
                $('.cr-image').attr('src', '/media/user/avatar.png').css({'width': '200' , 'height': '200'});
            } , 300);
        </script>
@endsection
