@extends('layouts.panel')

@section('header')
    <style>
        .form-control.is-invalid{
            background-position: left calc(.375em + .1875rem) center !important;
        }
        .span-error{
            color: #F00;
            font-size: 10px;
        }
        .card{
            direction: rtl;
            text-align: right;
        }
        .cr-boundary{
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')
    <div class="header-panel">
        <label>Add insurance</label>
    </div>
    <form method="post" action="{{route('profile_insurance_save')}}">
        @csrf
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Name*</label>
                    <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" placeholder="Name">
                    @if ($errors->has('name'))
                        <span class="span-error">{{ $errors->first('name') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Code*</label>
                    <input type="text" name="code" class="form-control" id="code" value="{{ old('code') }}" placeholder="Code">
                    @if ($errors->has('code'))
                        <span class="span-error">{{ $errors->first('code') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Owner</label>
                    <select name="userId" class="form-control">
                        <option @if($user->id == old('userId')) selected @endif value="{{ $user->id }}">myself</option>
                        @foreach ($children as $value)
                            <option @if($value->user->id == old('userId')) selected @endif value="{{ $value->user->id }}">{{ $value->user->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group col-12">
                <button type="submit" class="btn btn-success">submit</button>
            </div>
        </div>
    </form>
@endsection

@section('footer')

@endsection

@section('script')
        <script>

        </script>
@endsection
