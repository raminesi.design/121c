@extends('layouts.panel')

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
        <div class="header-panel">
            <label>Latest consultation</label>
        </div>
        <table id="DataTable" class="table table-bordered table-hover">
            <thead>
                <tr>
                <th>#</th>
                <th>Title</th>
                <th>Message</th>
                <th>Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach ( $notifications as $notification)
                    <tr>
                        <td>{{  $loop->iteration + $notifications->firstItem() - 1}}</td>
                        <td>{{  $notification->title  }}</td>
                        <td>{{  $notification->description  }}</td>
                        <td>{{  $notification->created_at  }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="table-buttons">
            <div>
                {{ $notifications->appends(request()->except('page'))->links() }}
            </div>
        </div>
@endsection

@section('footer')
    <!-- DataTables -->
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('script')
<script>
    $(function () {
        $('#DataTable').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,
            "responsive": true,
        });

    });
  </script>
@endsection
