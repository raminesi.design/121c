@extends('layouts.page')

@section('content')
<main id="main">
<!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs">
      <div class="container">
          <div class="row">

            <div class="col-md-4"></div>
            <div class="col-md-4">
                <form action="{{ route('login') }}" method="post" role="form" class="login-form">
                    @csrf
                    <img class="logo-login" width="80px" src="{{ asset('assets/img/Logo-fav.svg') }}">
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Please enter a valid email" />
                        @if ($errors->has('email'))
                            <div class="validate">Invalid email entered</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password" data-rule="minlen:8" data-msg="Please enter at least 8 chars" />
                        @if ($errors->has('password'))
                            <div class="validate">The password entered must be more than 8 characters</div>
                        @endif
                    </div>
                    <div class="mb-3">
                        @if ($message = Session::get('error'))
                            <span class="validate">{{ $message }}</span>
                        @endif
                    </div>
                    <div class="text-center"><button class="btn btn-info" type="submit">Login</button></div>
                </form>
                <hr/>
                <strong>Do not have an account? </strong><a href="{{ route('register_form') }}"><span class="register">Register</span></a>
                <hr/>
                <a href="{{ route('forget') }}"><small>Forgot your password?</small></a>
            </div>
            <div class="col-md-4"></div>

          </div>
      </div>
    </section>

</main>
@endsection
@section('script')

@endsection
