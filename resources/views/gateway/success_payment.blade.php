@extends('layouts.gateway')

@section('content')
    <div class="alert alert-success" style="text-align: center; height: 100vh; padding-top: 150px; font-size: 20px;">
        <strong>Payment was successful</strong>
        <br/>
        <strong>Transaction number : {{$order->transactionCode}}</strong>
        <div class="row" style="padding: 20px; ">
            <a href="{{ route('profile') }}"><button style="font-size: 20px !important; padding: 20px; border-radius: 5px;">Return profile</button></a>
        </div>
    </div>
@endsection
