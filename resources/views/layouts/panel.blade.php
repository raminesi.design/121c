<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{ $pageName }}</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('assets/img/Logo-fav.svg') }}" rel="icon">
  <link href="{{ asset('assets/img/Logo-fav.svg') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap-toggle.min.css') }}" rel="stylesheet">
  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Medilab - v2.1.1
  * Template URL: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->


  @yield('header')

</head>

<body>

    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false">
        <div class="toast-header">
          <strong class="mr-auto" id="toast_title"></strong>
          <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="toast-body" id="toast_message"></div>
      </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      {{-- <h1 class="logo mr-auto"><a href="{{ route('index') }}">121CLINICIANS</a></h1> --}}
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="{{ route('index') }}" class="logo mr-auto"><img src="{{ asset('assets/img/Logo.svg') }}" alt="" class="img-fluid"></a>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
            <li><a href="{{ route('index') }}">Home</a></li>
            @if($user->role == 'sick')
                    <li @if($pageName == 'profile') class="active" @endif><a href="{{ route('profile') }}">Profile</a></li>
                    <li><a href="{{ route('consultation_expertise') }}">Reservation</a></li>
                    <li @if($pageName == 'family') class="active" @endif><a href="{{ route('profile_children') }}">Family</a></li>
                    <li @if($pageName == 'insurances') class="active" @endif><a href="{{ route('profile_insurances') }}">Insurances</a></li>
                    <li @if($pageName == 'banks') class="active" @endif><a href="{{ route('profile_banks') }}">Banks</a></li>
                    <li @if($pageName == 'settings') class="active" @endif><a href="{{ route('profile_settings') }}">Settings</a></li>
                    <li @if($pageName == 'password') class="active" @endif><a href="{{ route('profile_password') }}">Password</a></li>
                    <li @if($pageName == 'tickets') class="active" @endif><a href="{{ route('profile_tickets') }}">Support</a></li>
                    <li @if($pageName == 'notifications') class="active" @endif><a href="{{ route('profile_notifications') }}">Notifications</a></li>
            @endif
            @if($user->role == 'doctor')
                <li @if($pageName == 'profile') class="active" @endif><a href="{{ route('doctorProfile') }}">Profile</a></li>
                <li @if($pageName == 'banks') class="active" @endif><a href="{{ route('profile_banks') }}">Banks</a></li>
                <li @if($pageName == 'settings') class="active" @endif><a href="{{ route('doctorProfile_settings') }}">Settings</a></li>
                <li @if($pageName == 'schedule') class="active" @endif><a href="{{ route('doctorProfile_schedule') }}">Schedule</a></li>
                <li @if($pageName == 'expertise') class="active" @endif><a href="{{ route('doctorProfile_expertise') }}">Expertise</a></li>
                <li @if($pageName == 'password') class="active" @endif><a href="{{ route('doctorProfile_password') }}">Password</a></li>
                <li @if($pageName == 'tickets') class="active" @endif><a href="{{ route('profile_tickets') }}">Support</a></li>
                <li @if($pageName == 'notifications') class="active" @endif><a href="{{ route('doctorProfile_notifications') }}">Notifications</a></li>
            @endif
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">
    <!-- ======= Breadcrumbs Section ======= -->
        <section class="breadcrumbs">
          <div class="container">
            <div class="row">
                <div class="col-md-3 profile profile-side">
                    @if(@$doctor)
                        <div class="col-12"><img class="avatar" src="{{ url('media/doctor/' . $doctor->avatar) }}" /></div>
                    @else
                        <div class="col-12"><img class="avatar" src="{{ url('media/user/' . $user->avatar) }}" /></div>
                    @endif
                    <div class="col-12">
                        <h4>{{ $user->name }}</h4>
                        <p>{{ $user->code }}</p>
                        <p>{{ $user->email }}</p>
                        <hr/>
                        @if($user->role == 'doctor')
                            <input id="activityDoctor" type="checkbox" @if($doctor->active) checked @endif  data-toggle="toggle" data-on="Active" data-off="Inactive">
                        <hr/>
                        @endif
                        @if($user->role == 'sick')
                            <p><a href="{{ route('profile_wallet') }}">Walet: {{ number_format($user->wallet->credit) }} $ </a></p>
                        @else
                            <p><a href="{{ route('doctorProfile_checkout') }}">Walet: {{ number_format($user->wallet->credit) }} $ </a></p>
                        @endif
                        <hr/>
                    </div>
                    <div class="col-12 exit"><a href="{{ route('logOut') }}">Sign Out <i class="icofont-logout"></i></a></div>

                </div>
                <div class="col-md-9 profile profile-content">
                    @yield('content')
                </div>
            </div>
          </div>
        </section>
    </main>

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('assets/vendor/venobox/venobox.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/counterup/counterup.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap-toggle.min.js') }}"></script>
  <!-- Template Main JS File -->
  <script src="{{ asset('assets/js/main.js') }}"></script>
  <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
  @yield('footer')
  @yield('script')
    <script>
        var pusher = new Pusher('8d0e07272043cec239bd', {
            cluster: 'mt1'
        });
        @if(@$doctor)
            var PageUrl = '{{ route('doctorProfile_consultation_info') }}';
            var textPageUrl = '{{ route('doctorProfile_consultationText') }}';
            var avatarUrl = '{{ url("media/user") }}';
            var channel1 = pusher.subscribe('{{ md5("All_doctor") }}');
        @else
            var PageUrl = '{{ route('profile_consultation_info') }}';
            var textPageUrl = '{{ route('profile_consultationText') }}';
            var avatarUrl = '{{ url("media/doctor") }}';
            var channel1 = pusher.subscribe('{{ md5("All_user") }}');
        @endif
        var channel2 = pusher.subscribe('{{ md5($user->email.$user->id) }}');
        var eventName = 'notification';
        var callback = function(data) {
            console.log(data);
            var title = data.title;
            var message = data.message;
            var dataN = data.data;
            if(dataN['type'] == 'notifications'){
                $('.toast').toast('show');
                $('#toast_title').html(title);
                $('#toast_message').html(message);
                if(dataN['url'] !== null){
                    $('#toast_message').append(' <br/><a href="'+dataN['url']+'">CLICK</a>');
                }
            }else if(dataN['type'] == 'message'){
                var name = dataN['name'];
                var avatar = dataN['avatar'];
                var time = dataN['time'];
                var url = window.location.href;
                if(url == textPageUrl+'?id='+dataN['id']){
                    $('#chat-content').append('<div class="media media-chat"> <img class="avatar" src="'+avatarUrl+'/'+avatar+'" title="'+name+'"><div class="media-body"><p>'+message+'</p><p class="meta"><time>'+time+'</time></p></div></div>');
                    $("#chat-content").animate({ scrollTop: $('#chat-content').prop("scrollHeight")}, 1000);
                }else{
                    $('.toast').toast('show');
                    $('#toast_title').html(title);
                    $('#toast_message').html(message);
                    $('#toast_message').append(' <br/><a href="'+textPageUrl+'?id='+dataN['id']+'">CLICK</a>');
                }
            }else if(dataN['type'] == 'file'){
                var time = dataN['time'];
                var url = window.location.href;
                if(url == textPageUrl+'?id='+dataN['id']){
                    $('<div class="col-12 files"><a target="_blank" href="{{ url('media/consultation/') }}'+dataN['file']+'"><strong>'+dataN['title']+'</strong></a></br><p>'+dataN['description']+'</p><p>'+dataN['time']+'</p><hr/></div>').insertBefore(".files:first");
                }else{
                    $('.toast').toast('show');
                    $('#toast_title').html(title);
                    $('#toast_message').html(message);
                    $('#toast_message').append(' <br/><a href="'+PageUrl+'?id='+dataN['id']+'">CLICK</a>');
                }
            }
        };
        // listen for 'new-comment' event on channel 1, 2 and 3
        pusher.bind(eventName, callback);

        $('#activityDoctor').change(function(){
            $.ajax({
                type:'POST',
                url:'{{route('doctorProfile_activity_status')}}',
                data:{_token: "{{ csrf_token()}}" , userId: {{ $user->id }} },
                success: function( msg ) {
                    console.log(msg);
                }
            });
        });
     </script>
</body>

</html>
