<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Product" prefix="og: http://ogp.me/ns#" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{ $pageName }}</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('assets/img/Logo-fav.svg') }}" rel="icon">
  <link href="{{ asset('assets/img/Logo-fav.svg') }}" rel="apple-touch-icon">

</head>

<body>
    @yield('content')

</body>

</html>
