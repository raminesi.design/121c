<h3>{{ $emails['title'] }}</h3>
<p>Hello, <b>{{ $emails['name'] }}</b></p>
<p>{{ $emails['message'] }}. @if(!is_null($emails['url'])) <a href="{{ $emails['url'] }}">CLICK</a>@endif</p>
