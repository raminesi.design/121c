<?php

return [
    'More' => 'More',
    'Request' => 'Request',
    'Requests' => 'Requests',
    'All' => 'All',
    'Medical staff' => 'Medical staff',
    'User' => 'User',
    'Income' => 'Income',
    'Income for the last six months' => 'Income for the last six months',
    'Home service' => 'Home service',
    'Online visit' => 'Online visit',
    'Request status' => 'Request status',
    'Payment type' => 'Payment type',
    'Payment status' => 'Payment status',
    'Patient name' => 'Patient name',
    'Referral day' => 'Referral day',
    'Searching' => 'Searching',
    'Categories' => 'Categories',
];
