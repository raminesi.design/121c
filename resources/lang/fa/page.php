<?php

return [
    'More' => 'بیشتر',
    'Request' => 'درخواست',
    'Requests' => 'درخواست ها',
    'All' => 'همه',
    'Medical staff' => 'کادر درمان',
    'User' => 'کاربر',
    'Income' => 'درآمد',
    'Income for the last six months' => 'درآمد شش ماه اخیر',
    'Home service' => 'خدمات در منزل',
    'Online visit' => 'ویزیت آنلاین',
    'Request status' => 'وضعیت درخواست',
    'Payment type' => 'نوع پرداخت',
    'Payment status' => 'وضعیت پرداخت',
    'Patient name' => 'نام بیمار',
    'Referral day' => 'روز مراجعه',
    'Searching' => 'جستجو',
    'Categories' => 'دسته بندی',
];
