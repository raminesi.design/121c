<?php

namespace App\Http\Middleware;

use App\Models\Doctor;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if(is_null(Cookie::get('cookieName'))){
            $cookieName = time().random_int(00000000, 99999999);
            Cookie::queue('cookieName', $cookieName , 600);
        }else{
            $cookieName = Cookie::get('cookieName');
        }
        Cache::store('redis')->put('requestUri-'.$cookieName , $request->getRequestUri() , 600);
        if (! $request->expectsJson()) {
            return route('login_form');
        }
    }
}
