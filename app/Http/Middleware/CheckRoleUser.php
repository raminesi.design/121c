<?php

namespace App\Http\Middleware;

use App\Models\Doctor;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckRoleUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        $doctor = Doctor::where('userId' , $user->id)->first();
        $user->role = 'sick';
        if($doctor){
            $user->role = 'doctor';
        }
        if($user->hasRole(['admin'])){
            return redirect(route('dashboard'));
        }
        return $next($request);
    }
}
