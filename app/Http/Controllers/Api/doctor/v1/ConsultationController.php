<?php

namespace App\Http\Controllers\Api\doctor\v1;

use App\Http\Controllers\Controller;
use App\Models\Consultation;
use App\Models\ConsultationFile;
use App\Models\Doctor;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\PusherTrait;
use App\Traits\emailNotificationTrait;
use Carbon\Carbon;

class ConsultationController extends Controller
{
    public function consultations(Request $request)
    {
        $user  = User::where('id' , Auth::user()->id)->with('doctor')->first();
        $query = Consultation::with(['schedule' => function($query){
            $query->with(['day' , 'time']);
        } , 'user' , 'expertise'])
        ->where('doctorId' , $user->doctor->id)
        ->where('status' , '!=' , 'created')
        ->orderBy('date' , 'desc');
        if(!empty($request->status)){
            $query->where('status' , $request->status);
        }
        if(isset($request->start)){
            $query->whereDate('created_at' , '>=' , $request->start);
        }
        if(isset($request->end)){
            $query->whereDate('created_at' , '<=' , $request->end);
        }
        $consultation = $query->paginate(15);
        $consultations = $consultation->map(function($val){
            return [
                'id' => $val->id,
                'type' => $val->type,
                'day' => $val->schedule->day->title ,
                'date' => $val->date ,
                'time' => $val->schedule->time->start.' - '.$val->schedule->time->end ,
                'user' => array(
                    'id' => $val->user->id,
                    'name' => $val->user->name,
                    'code' => $val->user->code,
                    'avatar' => url('media/user/'.(is_null($val->user->avatar) ? 'avatar.png' : $val->user->avatar)),
                    'expertise' => $val->expertise->title ,
                ),
                'created_at' => $val->created_at ,
                'status' => $val->status
            ];
        });
        return response()->json([
            'message' => 'List of consultation',
            'body' => $consultations
        ] , 200);
    }
    public function consultation(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $user  = User::where('id' , Auth::user()->id)->with('doctor')->first();
        $consultation = Consultation::with(['schedule' => function($query){
            $query->with(['day' , 'time']);
        } , 'doctor' => function($query){
            $query->with(['user']);
        } , 'user' , 'expertise' , 'prescription' => function($query){
            $query->with('details');
        }])->where('doctorId' , $user->doctor->id)->where('id' , $request->id)->first();

        $files = ConsultationFile::whereHas('consultation' , function($query) use($consultation){
            $query->where('userId' , $consultation->userId)
                ->where('expertiseId' , $consultation->expertise->id);
        })->orderBy('id' , 'desc')->get();
        $files = $files->map(function($val){
            return [
                'id' => $val->id,
                'title' => $val->title,
                'description' => $val->description,
                'sender' => $val->sender,
                'file' => url('media/consultation/'.$val->file)
            ];
        });
        $prescription = null;
        if(!is_null($consultation->prescription)){
            $items = array();
            foreach($consultation->prescription->details as $val){
                array_push($items , array(
                    'id' => $val->id,
                    'name' => $val->name,
                    'description' => $val->description,
                ));
            }
            $prescription = array(
                'id' => $consultation->prescription->id,
                'items' => $items,
                'description' => $consultation->prescription->description,
            );
        }
        return response()->json([
            'message' => 'List of consultation',
            'body' => array(
                'id' => $consultation->id,
                'channel' => $consultation->channel,
                'channel' => $consultation->channel,
                'type' => $consultation->type,
                'date' => $consultation->date,
                'day' => $consultation->schedule->day->title ,
                'time' => $consultation->schedule->time->start.' - '.$consultation->schedule->time->end ,
                'rate' => $consultation->rate,
                'comment' => $consultation->comment,
                'description' => $consultation->description,
                'sessions' => $consultation->sessions,
                'start' => $consultation->start,
                'end' => $consultation->end,
                'endUser' => $consultation->endUser,
                'report' => $consultation->report,
                'report_date' => $consultation->report_date,
                'status' => $consultation->status,
                'doctor' => array(
                    'id' => $consultation->doctor->id,
                    'name' => $consultation->doctor->user->name,
                    'code' => $consultation->doctor->code,
                    'description' => $consultation->doctor->description,
                    'avatar' => url('media/doctor/'.$consultation->doctor->avatar),
                    'expertise' => $consultation->expertise->title ,
                ),
                'patient' => array(
                    'id' => $consultation->user->id,
                    'name' => $consultation->user->name,
                    'code' => $consultation->user->code,
                    'avatar' => url('media/user/'.$consultation->user->avatar),
                ),
                'files' => $files,
                'prescription' => $prescription
            )
        ] , 200);
    }
    public function uploadFile(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'title' => 'required',
            'file' => 'required|mimes:png,jpeg,jpg,doc,docx,pdf|max:5000'
        ]);
        $user  = User::where('id' , Auth::user()->id)->with('doctor')->first();
        $uploadedFile = $request->file('file');
        $ext = $uploadedFile->getClientOriginalExtension();
        $file = time().'_'.random_int(00000000000, 9999999999).'.'.$ext;
        $uploadedFile->move(public_path() . '/media/consultation/', $file);
        $file = ConsultationFile::create([
            'userId' => $user->id,
            'consultationId' => $request->id,
            'title' => $request->title,
            'sender' => 'doctor',
            'description' => $request->description,
            'file' => $file
        ]);
        $data = array(
            'type' => 'file',
            'fileId' => $file->id,
            'title' => $file->title,
            'description' => $file->description,
            'file' => $file->file,
            'time' => $file->created_at,
            'id' => intval($request->id)
        );
        $consultation = Consultation::find(intval($request->id));
        $receiver     = User::find($consultation->userId);
        PusherTrait::send(md5($receiver->email.$receiver->id) , 'File' , 'A file was sent' , $data);

        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => array(
                'id' => $file->id,
                'title' => $file->title,
                'description' => $file->description,
                'sender' => $file->sender,
                'file' => url('media/consultation/'.$file->file)
            )
        ] , 200);
    }

    public function report(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'report' => 'required'
        ]);
        $user   = Auth::user();
        $consultation =  $user->doctor->consultations()->where('id' , $request->id)->first();
        if($consultation){
            $consultation->report = $request->report;
            $consultation->report_date = Carbon::now();
            $consultation->save();
            return response()->json([
                'message' => 'Information successfully recorded'
            ] , 200);
        }else{
            return response()->json([
                'message' => 'not found',
            ] , 404);
        }
    }
}
