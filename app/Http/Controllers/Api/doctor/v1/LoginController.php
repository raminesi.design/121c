<?php

namespace App\Http\Controllers\Api\doctor\v1;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
        ]);
        $user = User::where('email' , $request->email)->first();
        $doctor = Doctor::where('userId' , $user->id)->where('status' , 'enable')->first();
        if($doctor){
            if(Auth::attempt(['email' => $request->email , 'password' => $request->password])){
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                $token->save();
                return response()->json([
                    'message' => 'Welcome',
                    'body' => array(
                        'access_token' => $tokenResult->accessToken,
                        'token_type' => 'Bearer',
                        'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
                    )
                ] , 200);
            }else{
                return response()->json([
                    'message' => 'Wrong username or password'
                ] , 401);
            }
        }else{
            return response()->json([
                'message' => 'Impossible access'
            ] , 403);
        }
    }
    public function profile(Request $request)
    {
        $user = User::where('id' , Auth::user()->id)->with(['wallet'])->first();
        return response()->json([
            'message' => 'Profile',
            'body' => array(
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'avatar' => url('media/user/'.$user->avatar),
                'birthdate' => $user->birthdate,
                'gender' => $user->gender,
                'email' => $user->email,
                'code' => $user->code,
                'address' => $user->address,
                'wallet' => $user->wallet->credit
            )
        ] , 200);
    }
}
