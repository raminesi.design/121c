<?php

namespace App\Http\Controllers\Api\doctor\v1;

use App\Http\Controllers\Controller;
use App\Models\Prescription;
use App\Models\PrescriptionDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PrescriptionController extends Controller
{
    public function add(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'description' => 'required',
        ]);
        $prescription = Prescription::create([
            'consultationId' => $request->id,
            'description' => $request->description
        ]);
        $insert = array();
        foreach($request->items as $val){
            array_push($insert , array(
                'prescriptionId' => $prescription->id,
                'name' => $val['name'],
                'description' => $val['consumption']
            ));
        }
        PrescriptionDetails::insert($insert);
        $items = PrescriptionDetails::where('prescriptionId' , $prescription->id)->select('id' , 'name' , 'description')->get();
        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => array(
                'id' => $prescription->id,
                'description' => $prescription->description,
                'items' => $items
            )
        ] , 200);
    }
    public function deleteItem(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $user   = Auth::user();
        $item =  $user->doctor->consultations()->whereHas('prescription' , function($query) use($request){
            $query->whereHas('details' , function($query) use($request){
                $query->where('id' , $request->id);
            });
        })->first();
        if($item){
            PrescriptionDetails::where('id' , $request->id)->delete();
        }
        return response()->json([
            'message' => 'Item successfully removed',
        ] , 200);
    }
    public function delete(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $user   = Auth::user();
        $item =  $user->doctor->consultations()->whereHas('prescription' , function($query) use($request){
            $query->where('id' , $request->id);
        })->first();
        if($item){
            Prescription::where('id' , $request->id)->delete();
            return response()->json([
                'message' => 'Item successfully removed',
            ] , 200);
        }else{
            return response()->json([
                'message' => 'not found',
            ] , 404);
        }

    }
}
