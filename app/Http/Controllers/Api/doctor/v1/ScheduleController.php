<?php

namespace App\Http\Controllers\Api\doctor\v1;

use App\Http\Controllers\Controller;
use App\Models\Consultation;
use App\Models\Days;
use App\Models\Schedule;
use App\Models\Times;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ScheduleController extends Controller
{
    public function days(Request $request)
    {
        $days = Days::select('id' , 'title')->get();
        return response()->json([
            'message' => 'List of day',
            'body' => $days
        ] , 200);
    }

    public function hours(){
        $hours = Times::select('id' , 'title' , 'start' , 'end')->get();
        return response()->json([
            'message' => 'List of hour',
            'body' => $hours
        ] , 200);
    }

    public function schedule(Request $request)
    {
        $user = User::with('doctor')->where('id' , Auth::user()->id)->first();
        $Schedule = $user->doctor->schedule()->with(['time' , 'day'])->orderBy('dayId' , 'asc')->orderBy('timeId' , 'asc')->get();
        $days = Days::select('id' , 'title')->get();
        $list = array();
        foreach($days as $day){
            $times = collect($Schedule)->where('dayId' , $day->id);
            $arrayTime = array();
            foreach($times as $time){
                array_push($arrayTime , array(
                    'id' => $time->time->id,
                    'title' => $time->time->title,
                ));
            }
            array_push($list , array(
                'id' => $day->id,
                'title' => $day->title,
                'times' => $arrayTime
            ));
        }
        return response()->json([
            'message' => 'Schedule',
            'body' => $list
        ] , 200);
    }
    public function add(Request $request)
    {
        $request->validate([
            'dayId' => 'required',
            'timeId' => 'required'
        ]);
        $user = User::with('doctor')->where('id' , Auth::user()->id)->first();
        $Schedule = $user->doctor->schedule()->where('dayId' , $request->dayId)->where('timeId' , $request->timeId)->first();
        if(!$Schedule){
            $Schedule = Schedule::create([
                'doctorId' => $user->doctor->id,
                'dayId' => $request->dayId,
                'timeId' => $request->timeId
            ]);
        }
        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => $Schedule
        ] , 200);
    }
    public function addAllTime(Request $request)
    {
        $request->validate([
            'dayId' => 'required'
        ]);
        $user = User::with('doctor')->where('id' , Auth::user()->id)->first();
        $user->doctor->schedule()->where('dayId' , $request->dayId)->delete();
        $hours = Times::select('id' , 'title' , 'start' , 'end')->get();
        $insert = array();
        foreach($hours as $hour){
            array_push($insert , array(
                'doctorId' => $user->doctor->id,
                'dayId' => $request->dayId,
                'timeId' => $hour->id
            ));
        }
        Schedule::insert($insert);
        return response()->json([
            'message' => 'Information successfully recorded'
        ] , 200);
    }
    public function addAllDay(Request $request)
    {
        $request->validate([
            'timeId' => 'required'
        ]);
        $user = User::with('doctor')->where('id' , Auth::user()->id)->first();
        $user->doctor->schedule()->where('timeId' , $request->timeId)->delete();
        $days = Days::select('id' , 'title')->get();
        $insert = array();
        foreach($days as $day){
            array_push($insert , array(
                'doctorId' => $user->doctor->id,
                'dayId' => $day->id,
                'timeId' => $request->timeId
            ));
        }
        Schedule::insert($insert);
        return response()->json([
            'message' => 'Information successfully recorded'
        ] , 200);
    }
    public function delete(Request $request)
    {
        $request->validate([
            'dayId' => 'required',
            'timeId' => 'required'
        ]);
        $user = User::with('doctor')->where('id' , Auth::user()->id)->first();
        $user->doctor->schedule()->where('dayId' , $request->dayId)->where('timeId' , $request->timeId)->delete();
        return response()->json([
            'message' => 'Item successfully removed',
        ] , 200);
    }
    public function deleteAllTime(Request $request)
    {
        $request->validate([
            'dayId' => 'required'
        ]);
        $user = User::with('doctor')->where('id' , Auth::user()->id)->first();
        $user->doctor->schedule()->where('dayId' , $request->dayId)->delete();
        return response()->json([
            'message' => 'Item successfully removed',
        ] , 200);
    }
    public function deleteAllDay(Request $request)
    {
        $request->validate([
            'timeId' => 'required'
        ]);
        $user = User::with('doctor')->where('id' , Auth::user()->id)->first();
        $user->doctor->schedule()->where('timeId' , $request->timeId)->delete();
        return response()->json([
            'message' => 'Item successfully removed',
        ] , 200);
    }
}
