<?php

namespace App\Http\Controllers\Api\doctor\v1;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function profile(Request $request)
    {
        $user = User::where('id' , Auth::user()->id)->with(['wallet' , 'doctor'])->first();
        return response()->json([
            'message' => 'Profile',
            'body' => array(
                'id' => $user->id,
                'drId' => $user->doctor->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'avatar' => url('media/doctor/'.$user->doctor->avatar),
                'birthdate' => $user->birthdate,
                'gender' => $user->gender,
                'email' => $user->email,
                'code' => $user->doctor->code,
                'address' => $user->address,
                'wallet' => $user->wallet->credit
            )
        ] , 200);
    }
    public function updateUser(Request $request)
    {
        $user   = Auth::user();
        $date = Carbon::now()->addYears(-25)->format('Y-m-d');
        $request->validate([
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z- ]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z- ]+)(\d+)?$)/u',
            'birthdate' => 'required|date|before:'.$date,
            'email' => 'required|email|unique:users,email,'.$user->id,
            'mobile' => 'required|unique:users,mobile,'.$user->id,
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'You must not be under 16 years old',
            'email.required' => 'Please enter email',
            'email.email' => 'Invalid email entered',
            'email.unique' => 'This email is available in the system',
            'mobile.required' => 'Please enter mobile',
            'mobile.unique' => 'This mobile is available in the system'
        ]);
        $user = User::find($user->id);
        if(!empty($request->image)){
            $doctor = Doctor::where('userId' , $user->id)->first();
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/doctor/' . $imageName, base64_decode($image));
            $doctor->avatar = $imageName;
            $doctor->save();
        }
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->address = $request->address;
        $user->gender = $request->gender;
        $user->birthdate = $request->birthdate;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->name = $request->first_name.' '.$request->last_name;
        $user->save();
        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => $user
        ] , 200);
    }
    public function password(Request $request)
    {
        $user   = Auth::user();
        $request->validate([
            'password' => 'required|string|min:8',
        ],
        [
            'password.required' => 'Please enter password',
            'password.min' => 'The minimum number of characters must be 8',
        ]);
        $user = User::find($user->id);
        $user->password =  bcrypt($request->password);
        $user->save();
        return response()->json([
            'message' => 'Information successfully recorded'
        ] , 200);
    }
}
