<?php

namespace App\Http\Controllers\Api\user\v1;

use App\Http\Controllers\Controller;
use App\Models\Insurance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InsurancesController extends Controller
{
    public function insurances(Request $request)
    {
        $user  = Auth::user();
        $child = $user->parents()->pluck('userId')->toArray();
        if($child){
            array_push($child , $user->id);
        }else{
            $child = array($user->id);
        }
        $insurances = Insurance::whereIn('userId' , $child)->where('status' , '!=' , 'delete')->with(['user'])->orderBy('id' , 'desc')->get();
        $list = $insurances->map(function($val){
            return [
                'id' => $val->id,
                'name' => $val->name,
                'code' => $val->code,
                'status' => $val->status,
                'user' => array(
                    'id' => $val->user->id,
                    'name' => $val->user->name,
                    'avatar' => url('media/user/'.(is_null($val->user->avatar) ? 'avatar.png' : $val->user->avatar))
                )
            ];
        });
        return response()->json([
            'message' => 'List of insurances',
            'body' => $list
        ] , 200);
    }
    public function add(Request $request)
    {
        $request->validate([
            'familyId' => 'required',
            'name' => 'required',
            'code' => 'required'
        ]);
        $insurance = Insurance::create([
            'userId' => $request->familyId,
            'name' => $request->name,
            'code' => $request->code
        ]);
        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => $insurance
        ] , 200);
    }
    public function status(Request $request)
    {
        $user  = Auth::user();
        $child = $user->parents()->pluck('userId')->toArray();
        if($child){
            array_push($child , $user->id);
        }else{
            $child = array($user->id);
        }
        $insurance = Insurance::whereIn('userId' , $child)->where('id' , $request->id)->with(['user'])->first();
        if($insurance){
            if($insurance->status == 'enable'){
                $insurance->status = 'disable';
            }else{
                $insurance->status = 'enable';
            }
            $insurance->save();
        }
        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => array(
                'id' => $insurance->id,
                'name' => $insurance->name,
                'code' => $insurance->code,
                'status' => $insurance->status,
                'user' => array(
                    'id' => $insurance->user->id,
                    'name' => $insurance->user->name,
                    'avatar' => url('media/user/'.(is_null($insurance->user->avatar) ? 'avatar.png' : $insurance->user->avatar))
                )
            )
        ] , 200);
    }
    public function delete(Request $request)
    {
        $user  = Auth::user();
        $child = $user->parents()->pluck('userId')->toArray();
        if($child){
            array_push($child , $user->id);
        }else{
            $child = array($user->id);
        }
        $insurance = Insurance::whereIn('userId' , $child)->where('id' , $request->id)->first();
        if($insurance){
            $insurance->status = 'delete';
            $insurance->save();
        }
        return response()->json([
            'message' => 'Item successfully removed',
        ] , 200);
    }
}
