<?php

namespace App\Http\Controllers\Api\user\v1;

use App\Http\Controllers\Controller;
use App\Models\Consultation;
use App\Models\ConsultationFile;
use App\Models\Doctor;
use App\Models\DoctorExpertise;
use App\Models\Expertise;
use App\Models\Notifications;
use App\Models\Orders;
use App\Models\Schedule;
use App\Models\Settings;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\Order;
use App\Traits\PusherTrait;
use App\Traits\emailNotificationTrait;

class ConsultationController extends Controller
{
    public function expertise(Request $request)
    {
        $query = Expertise::orderby('id' , 'desc')->where('status' , 'enable')->paginate(15);
        $expertise = $query->map(function($exp){
            return [
                'id' => $exp->id,
                'title' => $exp->title,
                'description' => $exp->description,
                'image' => url('media/expertise/'.$exp->image)
            ];
        });
        return response()->json([
            'message' => 'Specialties',
            'body' => $expertise
        ] , 200);
    }
    public function doctors(Request $request)
    {
        $request->validate([
            'exp_id' => 'required'
        ]);
        $query = Doctor::with(['user' , 'expertises' => function($query){
            $query->where('status' , 'enable')->with(['expertise']);
        }])
            ->where('status' , 'enable')
            ->where('active' , 1)
            ->orderBy('rate' , 'desc')->whereHas('expertises' , function($query) use($request){
                    $query->where('expertiseId' , $request->exp_id)->where('status' , 'enable');
            });
        $query = $query->paginate(20);
        $doctors = $query->map(function($doctor){
            return [
                'id' => $doctor->id,
                'code' => $doctor->code,
                'name' => $doctor->user->name,
                'description' => $doctor->description,
                'avatar' => url('media/doctor/'.$doctor->avatar),
                'rate' => $doctor->rate,
                'price_video' => ($doctor->expertises[0]->price_video > 0 ? $doctor->expertises[0]->price_video : null),
                'price_voice' => ($doctor->expertises[0]->price_voice > 0 ? $doctor->expertises[0]->price_voice : null),
                'price_text' => ($doctor->expertises[0]->price_text > 0 ? $doctor->expertises[0]->price_text : null),
            ];
        });
        return response()->json([
            'message' => 'Doctors',
            'body' => $doctors
        ] , 200);
    }
    public function schedule(Request $request)
    {
        $request->validate([
            'doctor_id' => 'required|integer',
        ]);

        $ConsultationOld = Consultation::where('doctorId' , $request->doctor_id)->where('status' , 'accepted')->whereDate('date' , '>=' , date('Y-m-d'))->select('scheduleId')->get();
        $scheduleIds = array();
        foreach($ConsultationOld as $valC){
            array_push($scheduleIds , $valC->scheduleId);
        }
        $Schedule = array();
        for($i = 0 ; $i < 7 ; $i++){
            $date    = Carbon::now()->addDay($i)->format('Y-m-d');
            $dayNum  = Carbon::now()->addDay($i)->dayOfWeek;
            $dayName = Carbon::now()->addDay($i)->dayName;
            if($dayNum == 0){$dayNum = 7;}
            $times = Schedule::join('times' , 'times.id' , '=' , 'schedule.timeId')
                    ->where('schedule.doctorId' , $request->doctor_id)
                    ->whereNotIn('schedule.id' , $scheduleIds)
                    ->where('times.status' , 'enable');
                    if($i == 0){
                        $times->where('schedule.timeId' , '>' , date('H'));
                    }
                    $times = $times->where('schedule.dayId' , $dayNum)
                    ->select('schedule.id' , 'times.title')
                    ->get();
            $arrayTimes = array();
            foreach($times as $val){
                array_push($arrayTimes , array(
                    'id' => $val->id,
                    'title' => $val->title,
                ));
            }
            if(count($arrayTimes) > 0){
                $array = array(
                    'date' => $date,
                    'id' => $dayNum,
                    'name' => $dayName,
                    'times' => $arrayTimes
                );
                array_push($Schedule , $array);
            }
        }
        return response()->json([
            'message' => 'Schedule',
            'body' => $Schedule
        ] , 200);
    }

    public function createOrder(Request $request)
    {
        $request->validate([
            'user_id' => 'required|integer',
            'doctor_id' => 'required|integer',
            'exp_id' => 'required|integer',
            'type' => 'required',
            'date' => 'required|date',
            'time_id' => 'required|integer',
            'sessions' => 'required|integer',
        ]);
        $consultation = Consultation::create([
            'channel' => time().random_int(0000000000, 9999999999),
            'userId' => $request->user_id,
            'doctorId' => $request->doctor_id,
            'expertiseId' => $request->exp_id,
            'type' => $request->type,
            'scheduleId' => $request->time_id,
            'sessions' => $request->sessions,
            'date' => $request->date
        ]);
        $user = Auth::user();
        $expertise = DoctorExpertise::with(['expertise'])->where('doctorId' , $consultation->doctorId)->where('expertiseId' , $consultation->expertiseId)->where('status' , 'enable')->first();
        if($consultation->type == 'video'){
            $price = $expertise->price_video;
        }elseif($consultation->type == 'voice'){
            $price = $expertise->price_video;
        }else{
            $price = $expertise->price_video;
        }
        $amount = round($price + ($price * $expertise->expertise->percent / 100 ));
        $discount_amount = null;
        $total_amount = $consultation->sessions * $amount;
        $payable = $total_amount;
        if($consultation->sessions > 2){
            $discount_amount = ($consultation->sessions * 1.5 * 100) / $price;
            $payable = round($total_amount - $discount_amount);
        }
        $order = Orders::create([
            'userId' => $user->id,
            'orderNumber' => time().random_int(0, 9),
            'reqId' => $consultation->id,
            'discount_amount' => $discount_amount,
            'total_amount' => $total_amount,
            'wallet_amount' => 0,
            'payable_amount' => $payable,
            'payable_doctor' => $consultation->sessions * $price
        ]);

        return response()->json([
            'message' => 'Your order has been registered',
            'body' => array(
                'num' => $order->orderNumber
            )
        ] , 200);
    }
    public function orderCheck(Request $request)
    {
        $request->validate([
            'num' => 'required',
            'wallet' => 'required'
        ]);
        $user  = Auth::user();
        $order = Orders::where('orderNumber' , $request->num)->where('userId' , $user->id)->where('status' , 'unpaid')->where('created_at' , '>' , Carbon::now()->subMinutes(30))->first();
        if($order){
            if($request->wallet == 1){
                if($order->wallet_amount == 0 || is_null($order->wallet_amount)){
                    $payable = $order->payable_amount;
                    $wallet  = $user->wallet->credit;
                    if($wallet > $payable){
                        $wallet_amount = $payable;
                        $payable = 0;
                    }else{
                        $wallet_amount = $wallet;
                        $payable = $payable - $wallet;
                    }
                    $order->wallet_amount = $wallet_amount;
                    $order->payable_amount = $payable;
                }
            }else{
                $order->payable_amount = $order->wallet_amount + $order->payable_amount;
                $order->wallet_amount = 0;
            }
            $order->save();

            return response()->json([
                'message' => 'Order information',
                'body' => array(
                    'id' =>  $order->id,
                    'num' =>  $order->orderNumber,
                    'total_amount' =>  $order->total_amount,
                    'wallet_amount' =>  $order->wallet_amount,
                    'discount_amount' =>  $order->discount_amount,
                    'payable_amount' =>  $order->payable_amount,
                    'date' =>  $order->consultation->date,
                    'time' =>  $order->consultation->schedule->time->title,
                    'sessions' =>  $order->consultation->sessions,
                    'gateway' =>  route('transaction').'?num='.$order->orderNumber,
                    'doctor' => array(
                        'id' => $order->consultation->doctor->id,
                        'name' => $order->consultation->doctor->user->name,
                        'avatar' => url('media/doctor/'.$order->consultation->doctor->avatar),
                        'expertise' => $order->consultation->expertise->title,
                    )
                )
            ] , 200);
        }else{
            return response()->json([
                'message' => 'not found',
            ] , 404);
        }
    }

    public function consultations(Request $request)
    {
        $user  = Auth::user();
        $query = Consultation::with(['schedule' => function($query){
            $query->with(['day' , 'time']);
        } , 'user' , 'expertise'])
        ->whereHas('order' , function($q) use($user){
            $q->where('userId' , $user->id);
        })
        ->where('status' , '!=' , 'created')
        ->orderBy('date' , 'desc');
        if(!empty($request->status)){
            $query->where('status' , $request->status);
        }
        if(isset($request->doctor)){
            $query->where('doctorId' , $request->doctor);
        }
        if(isset($request->start)){
            $query->whereDate('created_at' , '>=' , $request->start);
        }
        if(isset($request->end)){
            $query->whereDate('created_at' , '<=' , $request->end);
        }
        $consultation = $query->paginate(15);
        $consultations = $consultation->map(function($val){
            return [
                'id' => $val->id,
                'type' => $val->type,
                'day' => $val->schedule->day->title ,
                'date' => $val->date ,
                'time' => $val->schedule->time->start.' - '.$val->schedule->time->end ,
                'doctor' => array(
                    'id' => $val->doctor->id,
                    'name' => $val->doctor->user->name,
                    'code' => $val->doctor->code,
                    'avatar' => url('media/doctor/'.$val->doctor->avatar),
                    'expertise' => $val->expertise->title ,
                ),
                'created_at' => $val->created_at ,
                'status' => $val->status
            ];
        });
        return response()->json([
            'message' => 'List of consultation',
            'body' => $consultations
        ] , 200);
    }
    public function consultation(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $consultation = Consultation::with(['schedule' => function($query){
            $query->with(['day' , 'time']);
        } , 'doctor' => function($query){
            $query->with(['user']);
        } , 'user' , 'expertise' , 'prescription' => function($query){
            $query->with('details');
        }])->where('id' , $request->id)->first();
        $files = ConsultationFile::whereHas('consultation' , function($query) use($consultation){
            $query->where('userId' , $consultation->userId)
                ->where('expertiseId' , $consultation->expertise->id);
        })->orderBy('id' , 'desc')->get();
        $files = $files->map(function($val){
            return [
                'id' => $val->id,
                'title' => $val->title,
                'description' => $val->description,
                'sender' => $val->sender,
                'file' => url('media/consultation/'.$val->file)
            ];
        });
        $prescription = null;
        if(!is_null($consultation->prescription)){
            $items = array();
            foreach($consultation->prescription->details as $val){
                array_push($items , array(
                    'id' => $val->id,
                    'name' => $val->name,
                    'description' => $val->description,
                ));
            }
            $prescription = array(
                'id' => $consultation->prescription->id,
                'items' => $items,
                'description' => $consultation->prescription->description,
            );
        }
        return response()->json([
            'message' => 'List of consultation',
            'body' => array(
                'id' => $consultation->id,
                'channel' => $consultation->channel,
                'channel' => $consultation->channel,
                'type' => $consultation->type,
                'date' => $consultation->date,
                'day' => $consultation->schedule->day->title ,
                'time' => $consultation->schedule->time->start.' - '.$consultation->schedule->time->end ,
                'rate' => $consultation->rate,
                'comment' => $consultation->comment,
                'description' => $consultation->description,
                'sessions' => $consultation->sessions,
                'start' => $consultation->start,
                'end' => $consultation->end,
                'endUser' => $consultation->endUser,
                'report' => $consultation->report,
                'report_date' => $consultation->report_date,
                'status' => $consultation->status,
                'doctor' => array(
                    'id' => $consultation->doctor->id,
                    'name' => $consultation->doctor->user->name,
                    'code' => $consultation->doctor->code,
                    'description' => $consultation->doctor->description,
                    'avatar' => url('media/doctor/'.$consultation->doctor->avatar),
                    'expertise' => $consultation->expertise->title ,
                ),
                'patient' => array(
                    'id' => $consultation->user->id,
                    'name' => $consultation->user->name,
                    'code' => $consultation->user->code,
                    'avatar' => url('media/user/'.$consultation->user->avatar),
                ),
                'files' => $files,
                'prescription' => $prescription
            )
        ] , 200);
    }

    public function uploadFile(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'title' => 'required',
            'file' => 'required|mimes:png,jpeg,jpg,doc,docx,pdf|max:5000'
        ]);
        $user   = Auth::user();
        $uploadedFile = $request->file('file');
        $ext = $uploadedFile->getClientOriginalExtension();
        $file = time().'_'.random_int(00000000000, 9999999999).'.'.$ext;
        $uploadedFile->move(public_path() . '/media/consultation/', $file);
        $file = ConsultationFile::create([
            'userId' => $user->id,
            'consultationId' => $request->id,
            'title' => $request->title,
            'description' => $request->description,
            'file' => $file
        ]);
        $data = array(
            'type' => 'file',
            'fileId' => $file->id,
            'title' => $file->title,
            'description' => $file->description,
            'file' => $file->file,
            'time' => $file->created_at,
            'id' => intval($request->id)
        );
        $consultation = Consultation::find(intval($request->id));
        $doctor     = Doctor::find(intval($consultation->doctorId));
        $receiver   = User::find($doctor->userId);
        PusherTrait::send(md5($receiver->email.$receiver->id) , 'File' , 'A file was sent' , $data);

        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => array(
                'id' => $file->id,
                'title' => $file->title,
                'description' => $file->description,
                'sender' => $file->sender,
                'file' => url('media/consultation/'.$file->file)
            )
        ] , 200);
    }
    public function cancellation(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);
        $user   = Auth::user();
        $consultation = Consultation::where('id' , $request->id)->where('status' , 'pending')->first();
        $order   = Orders::where('reqId' , $consultation->id)->where('userId' , $user->id)->first();
        if($order){
            $consultation->status = 'canceled';
            $consultation->description = $request->description;
            $consultation->save();

            $doctor = Doctor::find($consultation->doctorId);
            $userDoctor = User::find($doctor->userId);

            $wallet  = Wallet::where('userId' , $user->id)->first();
            $wallet->credit = $wallet->credit + ( $order->payable_amount + $order->wallet_amount);
            $wallet->save();
            $title   = 'Request status';
            $message = 'Request canceled by user';
            $url = route('doctorProfile_consultation_info').'?id='.$consultation->id;
            $data = array(
                'type' => 'notifications',
                'url' => $url
            );
            Notifications::create([
                'userId' => $userDoctor->id,
                'consultationId' => $consultation->id,
                'title' => 'Counseling status',
                'description' => 'The consultation has been canceled by the user',
            ]);
            PusherTrait::send(md5($userDoctor->email.$userDoctor->id) , $title , $message , $data);
            emailNotificationTrait::send($userDoctor->email , $userDoctor->name ,  $title , $message , $url);
            return response()->json([
                'message' => 'Canceled successfully'
            ] , 200);
        }else{
            return response()->json([
                'message' => 'not found',
            ] , 404);
        }
    }
}
