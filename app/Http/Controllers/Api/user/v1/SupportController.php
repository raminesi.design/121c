<?php

namespace App\Http\Controllers\Api\user\v1;

use App\Http\Controllers\Controller;
use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupportController extends Controller
{
    public function tickets(Request $request)
    {
        $user       = Auth::user();
        $tickets    = $user->tickets()->orderBy('created_at' , 'desc')->whereNull('parentId')->paginate(15);
        $list = $tickets->map(function($val){
            return [
                'id' => $val->id,
                'subject' => $val->subject,
                'status' => $val->status,
                'created_at' => $val->created_at,
                'updated_at' => $val->updated_at,
            ];
        });
        return response()->json([
            'message' => 'List of messages',
            'body' => $list
        ] , 200);
    }
    public function ticket(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $user       = Auth::user();
        $ticket     = Ticket::where('id' , $request->id)->where('userId' , $user->id)->with(['tickets' => function($query){
            $query->orderBy('created_at' , 'desc');
        }])->first();
        $messages = array();
        foreach($ticket->tickets as $val){
            array_push($messages , array(
                    'id' => $val->id,
                    'text' => $val->text,
                    'side' => ($val->userId == $user->id ? 0 : 1),
                    'file' => (is_null($val->file) ? null : url('media/ticket/'.$val->file))
                )
            );
        }
        return response()->json([
            'message' => 'Messages',
            'body' => array(
                'id' => $ticket->id,
                'status' => $ticket->status,
                'subject' => $ticket->subject,
                'text' => $ticket->text,
                'file' => (is_null($ticket->file) ? null : url('media/ticket/'.$ticket->file)),
                'messages' => $messages
            )
        ] , 200);
    }
    public function save(Request $request)
    {
        $request->validate([
            'subject' => 'required',
            'text' => 'required',
            'file' => 'mimes:png,jpeg,jpg|max:5000'
        ]);
        $file = null;
        if(!empty($request->file)){
            $uploadedFile = $request->file('file');
            $ext = $uploadedFile->getClientOriginalExtension();
            $file = time().'_'.random_int(00000000000, 9999999999).'.'.$ext;
            $uploadedFile->move(public_path() . '/media/ticket/', $file);
        }
        $user       = Auth::user();
        $ticket     = Ticket::create([
            'userId' => $user->id,
            'subject' => $request->subject,
            'text' => $request->text,
            'file' => $file
        ]);
        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => array(
                'id' => $ticket->id,
                'subject' => $ticket->subject,
                'text' => $ticket->text,
                'file' => (is_null($ticket->file) ? null : url('media/ticket/'.$ticket->file))
            )
        ] , 200);
    }
    public function saveComment(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'text' => 'required',
            'file' => 'mimes:png,jpeg,jpg|max:5000'
        ]);
        $user       = Auth::user();
        $ticket     = Ticket::where('id' , $request->id)->where('userId' , $user->id)->where('status' , 'open')->first();
        if($ticket){
            Ticket::where('id' , $request->id)->update(['updated_at' => Carbon::now()]);
            $file = null;
            if(!empty($request->file)){
                $uploadedFile = $request->file('file');
                $ext = $uploadedFile->getClientOriginalExtension();
                $file = time().'_'.random_int(00000000000, 9999999999).'.'.$ext;
                $uploadedFile->move(public_path() . '/media/ticket/', $file);
            }
            $ticket = Ticket::create([
                'userId' => $user->id,
                'parentId' => $ticket->id,
                'text' => $request->text,
                'file' => $file
            ]);
            return response()->json([
                'message' => 'Information successfully recorded',
                'body' => array(
                    'text' => $ticket->text,
                    'file' => (is_null($ticket->file) ? null : url('media/ticket/'.$ticket->file))
                )
            ] , 200);
        }else{
            return response()->json([
                'message' => 'not found',
            ] , 404);
        }
    }
}
