<?php

namespace App\Http\Controllers\Api\user\v1;

use App\Http\Controllers\Controller;
use App\Models\Consultation;
use App\Models\ConsultationFile;
use App\Models\ConsultationText;
use App\Models\Doctor;
use App\Models\ParentModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use App\Traits\PusherTrait;

class ChatController extends Controller
{
    public function text(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);
        $user   = Auth::user();
        $consultation = Consultation::with(['schedule' => function($query){
            $query->with(['day' , 'time']);
        }, 'doctor' => function($query){
            $query->with(['user']);
        } , 'expertise' , 'user'])->where('id' , $request->id)->first();

        if($consultation){
            $parent = ParentModel::where('parentId' , $user->id)->where('userId' , $consultation->userId)->where('status' , 'enable')->count();
            if($consultation->userId == $user->id || $parent > 0){

                $channel = md5($consultation->channel);
                if(!Redis::hget('chatRoom:'.$channel , $channel)){
                    $messages = ConsultationText::where('consultationId' , $consultation->id)->orderBy('id' , 'asc')->get();
                    if(count($messages) > 0){
                        $msg = $messages->map(function($message){
                            return [
                                'id' => $message->id,
                                'userId' => $message->userId,
                                'message' => $message->message,
                                'status' => $message->status,
                                'date' => explode(' ' ,$message->created_at)[1]
                            ];
                        });
                        Redis::hmset('chatRoom:'.$channel , [$channel => json_encode($msg)]);
                        $messageList = json_decode(Redis::hget('chatRoom:'.$channel , $channel) , true);
                    }else{
                        $messageList = array();
                    }
                }else{
                    $messageList = json_decode(Redis::hget('chatRoom:'.$channel , $channel) , true);
                }
                $list = collect($messageList)->map(function($val) use($user , $consultation){
                    if($val['userId'] == $user->id){
                        $sender = array(
                            'name' => $consultation->user->name,
                            'avatar' => url('media/user/'.(is_null($consultation->user->avatar) ? 'avatar.png' : $consultation->user->avatar)),
                        );
                    }else{
                        $sender = array(
                            'name' => $consultation->doctor->user->name,
                            'avatar' => url('media/doctor/'.(is_null($consultation->doctor->avatar) ? 'avatar.png' : $consultation->doctor->avatar)),
                        );
                    }
                    return [
                        'id' => $val['id'],
                        'message' => $val['message'],
                        'side' => ($val['userId'] == $user->id ? 'left' : 'right'),
                        'sender' => $sender,
                        'date' => $val['date'],
                    ];
                });
                return response()->json([
                    'message' => 'Messages',
                    'body' => $list
                ] , 200);

            }else{
                return response()->json([
                    'message' => 'not found',
                ] , 404);
            }
        }else{
            return response()->json([
                'message' => 'not found',
            ] , 404);
        }
    }
    public function sendMessage(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'message' => 'required',
        ]);
        $user   = Auth::user();
        $consultation = Consultation::where('id' , intval($request->id))->with(['doctor' => function($q){
            $q->with('user');
        }])->first();
        if($consultation){
            $parent = ParentModel::where('parentId' , $user->id)->where('userId' , $consultation->userId)->where('status' , 'enable')->count();
            if($consultation->userId == $user->id || $parent > 0){

                $message = ConsultationText::create([
                    'consultationId' => intval($request->id),
                    'userId' => $user->id,
                    'message' => $request->message,
                ]);
                $channel = md5($consultation->channel);
                if(Redis::hget('chatRoom:'.$channel , $channel)){
                    $messageList = json_decode(Redis::hget('chatRoom:'.$channel , $channel) , true);
                }else{
                    $messageList = array();
                }
                array_push($messageList , [
                    'id' => $message->id,
                    'userId' => $message->userId,
                    'message' => $message->message,
                    'status' => $message->status,
                    'date' => $message->created_at
                ]);
                Redis::hmset('chatRoom:'.$channel , [$channel => json_encode($messageList)]);
                $data = array(
                    'type' => 'message',
                    'name' => $consultation->user->name,
                    'avatar' => url('media/user/'.(is_null($consultation->user->avatar) ? 'avatar.png' : $consultation->user->avatar)),
                    'time' => explode(' ' ,$message->created_at)[1],
                    'id' => intval($request->id)
                );
                PusherTrait::send(md5($consultation->doctor->user->email.$consultation->doctor->user->id) , 'New message' , $message->message , $data);
                return response()->json([
                    'message' => 'Message sent',
                    'body' => array(
                        'id' => $message->id,
                        'message' => $message->message,
                        'side' => 'left',
                        'sender' => array(
                            'name' => $consultation->user->name,
                            'avatar' => url('media/user/'.(is_null($consultation->user->avatar) ? 'avatar.png' : $consultation->user->avatar)),
                        ),
                        'date' => $message->created_at
                    )
                ] , 200);
            }else{
                return response()->json([
                    'message' => 'not found',
                ] , 404);
            }
        }else{
            return response()->json([
                'message' => 'not found',
            ] , 404);
        }
    }

    public function live(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $user   = Auth::user();
        $consultation = Consultation::where('id' , intval($request->id))->with(['doctor' => function($q){
            $q->with('user');
        } , 'user'])->first();
        if($consultation){
            $parent = ParentModel::where('parentId' , $user->id)->where('userId' , $consultation->userId)->where('status' , 'enable')->count();
            if($consultation->userId == $user->id || $parent > 0){
                if($consultation->type == 'text'){
                    $url = null;
                }else{
                    $url = route('live_call').'?channel='.$consultation->channel.'&type='.$consultation->type.'&userType=user&userCode='.$consultation->user->code;

                }
                return response()->json([
                    'message' => 'Live room',
                    'body' => array(
                        'type' => $consultation->type,
                        'url' => $url
                    )
                ] , 200);
            }else{
                return response()->json([
                    'message' => 'not found',
                ] , 404);
            }
        }else{
            return response()->json([
                'message' => 'not found',
            ] , 404);
        }
    }
}
