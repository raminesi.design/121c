<?php

namespace App\Http\Controllers\Api\user\v1;

use App\Http\Controllers\Controller;
use App\Models\Notifications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    public function notifications(Request $request)
    {
        $user   = Auth::user();
        $notifications = Notifications::where('userId' , $user->id)->orWhere('group' , 'user')->orderBy('id' , 'desc')->paginate(15);
        $list = $notifications->map(function($val){
            return [
                'id' => $val->id,
                'title' => $val->title,
                'description' => $val->description,
                'date' => $val->created_at,
                'group' => $val->group,
            ];
        });
        return response()->json([
            'message' => 'List of notifications',
            'body' => $list
        ] , 200);
    }
}
