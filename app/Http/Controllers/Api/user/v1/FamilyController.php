<?php

namespace App\Http\Controllers\Api\user\v1;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\ParentModel;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FamilyController extends Controller
{
    public function family(Request $request)
    {
        $familyQuery = User::where('id' , Auth::user()->id)->with(['parents' => function($query){
            $query->with('user')->where('status' , 'enable');
        }])->first();
        $family = array(
            array(
                'id' => $familyQuery->id,
                'name' => 'myself'
            )
        );
        foreach($familyQuery->parents as $val){
            array_push($family , array(
                'id' => $val->user->id,
                'name' => $val->user->name
            ));
        }
        return response()->json([
            'message' => 'Family',
            'body' => $family
        ] , 200);
    }

    public function familyInfo(Request $request)
    {
        $user = User::find($request->id);
        return response()->json([
            'message' => 'Family',
            'body' => array(
                'id' => $user->id,
                'name' => $user->name,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'birthdate' => $user->birthdate,
                'gender' => $user->gender,
                'code' => $user->code,
                'avatar' => url('media/user/'.(is_null($user->avatar) ? 'avatar.png' : $user->avatar)),
            )
        ] , 200);
    }

    public function add(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        $request->validate([
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'birthdate' => 'required|date|before:'.$date,
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'The date entered is incorrect',
        ]);
        $parent   = Auth::user();
        $country = Country::find($parent->country);
        $l1 = strtoupper(substr($request->first_name,0 , 1));
        $l2 = strtoupper(substr($request->last_name,0 , 1));
        $code = random_int(11111111, 99999999).$country->symbol.$l1.$l2;
        $code = $this->checkCode($code , $country->symbol.$l1.$l2);
        $imageName = 'avatar.png';
        if(!empty($request->image)){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/user/' . $imageName, base64_decode($image));
        }
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'name' => $request->first_name.' '.$request->last_name,
            'email' => $request->email,
            'birthdate' => $request->birthdate,
            'avatar' => $imageName,
            'code' => $code
        ]);
        Wallet::create([
            'userId' => $user->id
        ]);
        ParentModel::create([
            'parentId' => $parent->id,
            'userId' => $user->id
        ]);
        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => $user
        ] , 200);
    }

    public function edit(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        $request->validate([
            'id' => 'required',
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z- ]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z- ]+)(\d+)?$)/u',
            'birthdate' => 'required|date|before:'.$date,
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'The date entered is incorrect',
        ]);
        $user = User::find($request->id);
        if(!empty($request->image)){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/user/' . $imageName, base64_decode($image));
            $user->avatar = $imageName;
        }
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->name = $request->first_name.' '.$request->last_name;
        $user->birthdate = $request->birthdate;
        $user->gender = $request->gender;
        $user->save();

        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => $user
        ] , 200);
    }

    public function delete(Request $request){
        $request->validate([
            'id' => 'required'
        ]);
        $parent   = Auth::user();
        $user = ParentModel::where('userId' , $request->id)->where('parentId' , $parent->id)->first();
        if($user){
            $user->delete();
        }
        return response()->json([
            'message' => 'Item successfully removed',
        ] , 200);
    }

    function checkCode($code , $string)
    {
        $user = User::where('code' , $code)->first();
        if($user){
            $code = random_int(11111111, 99999999).$string;
            $this->checkCode($code , $string);
        }else{
            return $code;
        }
    }
}
