<?php

namespace App\Http\Controllers\Api\user\v1;

use App\Http\Controllers\Controller;
use App\Models\Banks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BankController extends Controller
{
    public function banks(Request $request)
    {
        $user     = Auth::user();
        $doctor   = $user->doctor;
        if(!is_null($doctor)){
            $user->code = $doctor->code;
        }
        $banks = $user->banks()->where('status' , '!=' , 'delete')->select('id' , 'name' , 'code' , 'status')->get();
        return response()->json([
            'message' => 'List of bank',
            'body' => $banks
        ] , 200);
    }
    public function add(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required'
        ]);
        $user  = Auth::user();
        $bank = Banks::create([
            'userId' => $user->id,
            'name' => $request->name,
            'code' => $request->code
        ]);
        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => array(
                'id' => $bank->id ,
                'name' => $bank->name ,
                'code' => $bank->code ,
                'status' => $bank->status
            )
        ] , 200);
    }
    public function status(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $user  = Auth::user();
        $bank  = Banks::where('userId' , $user->id)->where('id' , $request->id)->first();
        if($bank){
            if($bank->status == 'enable'){
                $bank->status = 'disable';
            }else{
                $bank->status = 'enable';
            }
            $bank->save();
        }
        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => array(
                'id' => $bank->id ,
                'name' => $bank->name ,
                'code' => $bank->code ,
                'status' => $bank->status
            )
        ] , 200);
    }
    public function delete(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $user  = Auth::user();
        $bank = Banks::where('userId' , $user->id)->where('id' , $request->id)->first();
        if($bank){
            $bank->status = 'delete';
            $bank->save();
        }
        return response()->json([
            'message' => 'Item successfully removed',
        ] , 200);
    }
}
