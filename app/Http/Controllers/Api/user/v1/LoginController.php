<?php

namespace App\Http\Controllers\Api\user\v1;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
        ]);
        if(Auth::attempt(['email' => $request->email , 'password' => $request->password])){
            $user = User::where('email' , $request->email)->first();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->save();
            return response()->json([
                'message' => 'Welcome',
                'body' => array(
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
                )
            ] , 200);
        }else{
            return response()->json([
                'message' => 'Wrong username or password'
            ] , 401);
        }
    }
    public function countries(Request $request)
    {
        $countries = Country::orderBy('sort' , 'asc')->select('id' , 'name')->get();
        return response()->json([
            'message' => 'Countries',
            'body' => $countries
        ] , 200);
    }
    public function register(Request $request)
    {
        $date = Carbon::now()->addYears(-16)->format('Y-m-d');
        $request->validate([
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z- ]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z- ]+)(\d+)?$)/u',
            'country' => 'required',
            'birthdate' => 'required|date|before:'.$date,
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:8|required_with:confirmPassword|same:confirmPassword',
            'confirmPassword' => 'required|string|min:8'
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'You are not allowed to register in the system. Please apply through your parents.',
            'email.required' => 'Please enter email',
            'email.email' => 'Invalid email entered',
            'email.unique' => 'This email is available in the system',
            'password.required' => 'Please enter password',
            'password.min' => 'The minimum number of characters must be 8',
            'password.required_with' => 'Passwords do not match',
        ]);

        $imageName = 'avatar.png';
        if(!empty($request->image)){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/user/' . $imageName, base64_decode($image));
        }

        $country = Country::find($request->country);
        $l1 = strtoupper(substr($request->first_name,0 , 1));
        $l2 = strtoupper(substr($request->last_name,0 , 1));
        $code = random_int(11111111, 99999999).$country->symbol.$l1.$l2;
        $code = $this->checkCode($code , $country->symbol.$l1.$l2);

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'name' => $request->first_name.' '.$request->last_name,
            'email' => $request->email,
            'birthdate' => $request->birthdate,
            'avatar' => $imageName,
            'country' => $request->country,
            'code' => $code,
            'password' => bcrypt($request->password)
        ]);
        Wallet::create([
            'userId' => $user->id
        ]);
        if(Auth::attempt(['email' => $request->email , 'password' => $request->password])){
            $user = User::where('email' , $request->email)->first();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->save();
            return response()->json([
                'message' => 'Welcome',
                'body' => array(
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
                )
            ] , 200);
        }else{
            return response()->json([
                'message' => 'Wrong username or password'
            ] , 401);
        }
    }
    public function profile(Request $request)
    {
        $user = User::where('id' , Auth::user()->id)->with(['wallet'])->first();
        return response()->json([
            'message' => 'Profile',
            'body' => array(
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'avatar' => url('media/user/'.$user->avatar),
                'birthdate' => $user->birthdate,
                'gender' => $user->gender,
                'email' => $user->email,
                'code' => $user->code,
                'address' => $user->address,
                'wallet' => $user->wallet->credit
            )
        ] , 200);
    }

    public function updateUser(Request $request)
    {
        $user   = Auth::user();
        $date = Carbon::now()->addYears(-16)->format('Y-m-d');
        $request->validate([
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z- ]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z- ]+)(\d+)?$)/u',
            'birthdate' => 'required|date|before:'.$date,
            'email' => 'required|email|unique:users,email,'.$user->id
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'You must not be under 16 years old',
            'email.required' => 'Please enter email',
            'email.email' => 'Invalid email entered',
            'email.unique' => 'This email is available in the system'
        ]);
        $user = User::find($user->id);
        if(!empty($request->image)){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/user/' . $imageName, base64_decode($image));
            $user->avatar = $imageName;
        }
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->address = $request->address;
        $user->gender = $request->gender;
        $user->birthdate = $request->birthdate;
        $user->email = $request->email;
        $user->name = $request->first_name.' '.$request->last_name;
        $user->save();
        return response()->json([
            'message' => 'Information successfully recorded',
            'body' => $user
        ] , 200);
    }
    public function password(Request $request)
    {
        $user   = Auth::user();
        $request->validate([
            'password' => 'required|string|min:8',
        ],
        [
            'password.required' => 'Please enter password',
            'password.min' => 'The minimum number of characters must be 8',
        ]);
        $user = User::find($user->id);
        $user->password =  bcrypt($request->password);
        $user->save();
        return response()->json([
            'message' => 'Information successfully recorded'
        ] , 200);
    }
    function checkCode($code , $string)
    {
        $user = User::where('code' , $code)->first();
        if($user){
            $code = random_int(11111111, 99999999).$string;
            $this->checkCode($code , $string);
        }else{
            return $code;
        }
    }
}
