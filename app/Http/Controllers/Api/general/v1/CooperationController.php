<?php

namespace App\Http\Controllers\Api\general\v1;

use App\Http\Controllers\Controller;
use App\Models\Cooperation;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CooperationController extends Controller
{
    public function cooperation(Request $request)
    {
        $date = Carbon::now()->addYears(-25)->format('Y-m-d');
        $request->validate([
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'birthdate' => 'required|date|before:'.$date,
            'mobile' => 'required|size:11|unique:users',
            'email' => 'required|email|unique:users',
            'image' => 'required',
            'expertise' => 'required',
            'gender' => 'required',
            'description' => 'required'
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'You are not allowed to register your information',
            'mobile.required' => 'Please enter mobile',
            'mobile.size' => 'Invalid mobile entered',
            'mobile.unique' => 'This mobile is available in the system',
            'email.required' => 'Please enter email',
            'email.email' => 'Invalid email entered',
            'email.unique' => 'This email is available in the system',
            'image.required' => 'Please select a profile picture',
            'expertise.required' => 'Please enter your specialty',
            'description.required' => 'Please enter a description'
        ]);

        $ip     = $request->ip();
        $cooperation = Cooperation::where('ip' , $ip)->count();
        if($cooperation < 4){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/doctor/' . $imageName, base64_decode($image));
            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'name' => $request->first_name.' '.$request->last_name,
                'avatar' => $imageName,
                'birthdate' => $request->birthdate,
                'gender' => $request->gender,
                'mobile' => $request->mobile,
                'email' => $request->email
            ]);
            Cooperation::create([
                'userId' => $user->id,
                'avatar' => $imageName,
                'expertise' => $request->expertise,
                'description' => $request->description,
                'ip' => $ip,
            ]);

            return response()->json([
                'message' => 'Your request has been sent successfully. Please wait for the call of our experts'
            ] , 200);

        }else{
            return response()->json([
                'message' => 'You can not send more than 3 requests'
            ] , 429);
        }
    }
}
