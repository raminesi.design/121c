<?php

namespace App\Http\Controllers\Api\general\v1;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Expertise;
use Illuminate\Http\Request;

class DoctorsController extends Controller
{
    public function expertises(Request $request)
    {
        $query = Expertise::where('status' , 'enable')->orderBy('sort' , 'asc')->paginate(20);
        $expertises = $query->map(function($expertise){
            return [
                'id' => $expertise->id,
                'title' => $expertise->title,
                'description' => $expertise->description,
                'image' => url('media/expertise/'.$expertise->image)
            ];
        });
        return response()->json([
            'message' => 'Specialties',
            'body' => $expertises
        ] , 200);
    }
    public function doctors(Request $request)
    {
        $query = Doctor::with(['user' , 'expertises' => function($query){
            $query->where('status' , 'enable')->with(['expertise']);
        }])
            ->where('status' , 'enable')
            ->where('active' , 1)
            ->orderBy('rate' , 'desc');
            if(!empty($request->exp_id)){
                $expId = $request->exp_id;
                $query->whereHas('expertises' , function($query) use($expId){
                    $query->where('expertiseId' , $expId)->where('status' , 'enable');
                });
            }
        $query = $query->paginate(20);
        $doctors = $query->map(function($doctor){
            return [
                'id' => $doctor->id,
                'code' => $doctor->code,
                'name' => $doctor->user->name,
                'description' => $doctor->description,
                'avatar' => url('media/doctor/'.$doctor->avatar),
                'rate' => $doctor->rate,
                'expertise' => $doctor->expertises[0]->expertise->title
            ];
        });
        return response()->json([
            'message' => 'Doctors',
            'body' => $doctors
        ] , 200);
    }
}
