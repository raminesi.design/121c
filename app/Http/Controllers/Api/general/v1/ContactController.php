<?php

namespace App\Http\Controllers\Api\general\v1;

use App\Http\Controllers\Controller;
use App\Models\ContactModel;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function contact(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required|max:250'
        ]);
        
        $ip     = $request->ip();
        $count = ContactModel::where('ip' , $ip)->whereDate('created_at', Carbon::today())->count();
        if($count < 6){
            ContactModel::create([
                'ip' => $ip,
                'name' => $request->name,
                'email' => $request->email,
                'subject' => $request->subject,
                'message' => $request->message
            ]);
            return response()->json([
                'message' => 'Your message has been successfully registered'
            ] , 200);
        }else{
            return response()->json([
                'message' => 'You can not send messages more than 5 times a day'
            ] , 429);
        }
    }
}
