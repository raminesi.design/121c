<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Consultation;
use App\Models\ConsultationText;
use App\Models\Doctor;
use App\Models\DoctorExpertise;
use App\Models\Expertise;
use App\Models\Orders;
use App\Models\Packages;
use App\Models\Schedule;
use App\Models\Settings;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redis;
use App\Traits\PusherTrait;

class ConsultationController extends Controller
{
    public function expertise(Request $request)
    {
        $expertise = Expertise::where('status' , 'enable')->orderBy('sort' , 'asc')->get();
        $setting = Settings::first();
        return view('front.consultation.expertise' , compact('request' , 'expertise' , 'setting'));
    }
    public function doctors(Request $request)
    {
        $request->validate([
            'id' => 'required|integer'
        ]);
        $cookieName = Cookie::get('cookieName');
        $array = array(
            'expertise' => $request->id,
            'doctor' => null,
            'type' => null,
            'sessions' => 1,
            'date' => null,
            'schedule' => null,
            'sick' => null,
        );
        Redis::hmset('Consultation:'.$cookieName , [$cookieName => json_encode($array)]);
        $drId = Schedule::groupBy('doctorId')->select('doctorId')->get();
        $ids = array();
        foreach($drId as $val){
            array_push($ids , $val->doctorId);
        }

        $doctors = Doctor::with(['user' , 'expertises' => function($query) use($request){
            $query->where('expertiseId' , $request->id)->with(['expertise']);
        }])
        ->whereIn('id' , $ids)
        ->where('status' , 'enable')
        ->where('active' , 1)
        ->whereHas('expertises' , function($query) use($request){
            $query->where('expertiseId' , $request->id)->where('status' , 'enable');
        })->get();

        $setting = Settings::first();
        return view('front.consultation.doctors' , compact('request' , 'doctors' , 'setting'));
    }
    public function schedule(Request $request)
    {
        $request->validate([
            'id' => 'required|integer',
            'type' => 'required',
        ]);
        $cookieName = Cookie::get('cookieName');
        if(!Redis::hget('Consultation:'.$cookieName , $cookieName)){
            return redirect(route('consultation_expertise'));
        }
        $Consultation = json_decode(Redis::hget('Consultation:'.$cookieName , $cookieName) , True);
        $doctor = Doctor::with(['user' ,'expertises' => function($query) use($Consultation){
            $query->where('expertiseId' , intval($Consultation['expertise']))->with(['expertise']);
        }])->whereHas('expertises' , function($query) use($Consultation){
            $query->where('expertiseId' , intval($Consultation['expertise']));
        })->where('id' , $request->id)->first();

        $price['video'] = round($doctor->expertises[0]->price_video + ($doctor->expertises[0]->price_video * $doctor->expertises[0]->expertise->percent / 100 ));
        $price['voice'] = round($doctor->expertises[0]->price_voice + ($doctor->expertises[0]->price_voice * $doctor->expertises[0]->expertise->percent / 100 ));
        $price['text'] = round($doctor->expertises[0]->price_text + ($doctor->expertises[0]->price_text * $doctor->expertises[0]->expertise->percent / 100 ));

        $Consultation = array(
            'expertise' => $Consultation['expertise'],
            'doctor' => $request->id,
            'type' => $request->type,
            'sessions' => 1,
            'date' => null,
            'schedule' => null,
            'sick' => null,
        );
        Redis::hmset('Consultation:'.$cookieName , [$cookieName => json_encode($Consultation)]);

        $Schedule = array();
        for($i = 0 ; $i < 7 ; $i++){
            $date    = Carbon::now()->addDay($i)->format('Y-m-d');
            $dayNum  = Carbon::now()->addDay($i)->dayOfWeek;
            $dayName = Carbon::now()->addDay($i)->dayName;
            if($dayNum == 0){$dayNum = 7;}
            $ConsultationCount = Consultation::where('doctorId' , $request->id)->whereIn('status' , ['pending','accepted','doing','done'])->whereDate('date' , $date)->count();
            $schCount = Schedule::where('doctorId' , $request->id)->where('dayId' , $dayNum)->count();
            if($ConsultationCount < 21 && $schCount > 0){
                $array = array(
                    'date' => $date,
                    'id' => $dayNum,
                    'name' => $dayName
                );
                array_push($Schedule , $array);
            }
        }

        $packages = $doctor->packeges()->with('package')->get();

        $user = Auth::user();
        $children = User::join('parent' , 'parent.userId' , '=' , 'users.id')
            ->where('parent.parentId' , $user->id)
            ->where('parent.status' , 'enable')
            ->select('users.*')
            ->get();

        $setting = Settings::first();
        return view('front.consultation.schedule' , compact('request' , 'Schedule' , 'Consultation' , 'children' , 'user' , 'doctor' , 'price' , 'packages' , 'setting'));
    }
    public function schedule1(Request $request)
    {
        $request->validate([
            'id' => 'required|integer',
            'type' => 'required',
        ]);
        $cookieName = Cookie::get('cookieName');
        if(!Redis::hget('Consultation:'.$cookieName , $cookieName)){
            return redirect(route('consultation_expertise'));
        }

        $Consultation = json_decode(Redis::hget('Consultation:'.$cookieName , $cookieName) , True);
        $doctor = Doctor::with(['user' ,'expertises' => function($query) use($Consultation){
            $query->where('expertiseId' , intval($Consultation['expertise']))->with(['expertise']);
        }])->whereHas('expertises' , function($query) use($Consultation){
            $query->where('expertiseId' , intval($Consultation['expertise']));
        })->where('id' , $request->id)->first();

        $price['video'] = round($doctor->expertises[0]->price_video + ($doctor->expertises[0]->price_video * $doctor->expertises[0]->expertise->percent / 100 ));
        $price['voice'] = round($doctor->expertises[0]->price_voice + ($doctor->expertises[0]->price_voice * $doctor->expertises[0]->expertise->percent / 100 ));
        $price['text'] = round($doctor->expertises[0]->price_text + ($doctor->expertises[0]->price_text * $doctor->expertises[0]->expertise->percent / 100 ));

        $Consultation = array(
            'expertise' => $Consultation['expertise'],
            'doctor' => $request->id,
            'type' => $request->type,
            'sessions' => 1,
            'date' => null,
            'schedule' => null,
            'sick' => null,
        );
        Redis::hmset('Consultation:'.$cookieName , [$cookieName => json_encode($Consultation)]);
        $ConsultationOld = Consultation::where('doctorId' , $request->id)->where('status' , 'accepted')->whereDate('date' , '>=' , date('Y-m-d'))->select('scheduleId')->get();
        $scheduleIds = array();
        foreach($ConsultationOld as $valC){
            array_push($scheduleIds , $valC->scheduleId);
        }
        $Schedule = array();
        for($i = 0 ; $i < 7 ; $i++){
            $date    = Carbon::now()->addDay($i)->format('Y-m-d');
            $dayNum  = Carbon::now()->addDay($i)->dayOfWeek;
            $dayName = Carbon::now()->addDay($i)->dayName;
            if($dayNum == 0){$dayNum = 7;}
            $times = Schedule::join('times' , 'times.id' , '=' , 'schedule.timeId')
                    ->where('schedule.doctorId' , $request->id)
                    ->whereNotIn('schedule.id' , $scheduleIds)
                    ->where('times.status' , 'enable');
                    if($i == 0){
                        $times->where('schedule.timeId' , '>' , date('H'));
                    }
                    $times = $times->where('schedule.dayId' , $dayNum)
                    ->select('schedule.id' , 'times.title')
                    ->get();
            $arrayTimes = array();
            foreach($times as $val){
                array_push($arrayTimes , array(
                    'id' => $val->id,
                    'title' => $val->title,
                ));
            }
            if(count($arrayTimes) > 0){
                $array = array(
                    'date' => $date,
                    'id' => $dayNum,
                    'name' => $dayName,
                    'times' => $arrayTimes
                );
                array_push($Schedule , $array);
            }
        }
        $user = Auth::user();
        $children = User::join('parent' , 'parent.userId' , '=' , 'users.id')
            ->where('parent.parentId' , $user->id)
            ->where('parent.status' , 'enable')
            ->select('users.*')
            ->get();

        $setting = Settings::first();
        return view('front.consultation.schedule' , compact('request' , 'Schedule' , 'Consultation' , 'children' , 'user' , 'doctor' , 'price' , 'setting'));
    }
    public function saveSchedule(Request $request)
    {
        $request->validate([
            'visitStart' => 'required',
            'date' => 'required|date',
        ]);

        $user = Auth::user();
        if(empty($request->sick)){
            $request->sick = $user->id;
        }
        $cookieName = Cookie::get('cookieName');
        if(!Redis::hget('Consultation:'.$cookieName , $cookieName)){
            return redirect(route('consultation_expertise'));
        }
        $Consultation = json_decode(Redis::hget('Consultation:'.$cookieName , $cookieName) , True);

        $package = Packages::find(intval($request->visitTime));
        $currentDate = strtotime($request->date.' '.$request->visitStart.':00');
        $futureDate = $currentDate + (60*$package->time);
        $HS = explode(':' , $request->visitStart)[0];
        $HE = date("H", $futureDate);
        $day = Carbon::parse($Consultation['date'])->dayOfWeek;
        if($day == 0){$day = 7;}
        $schStatus = false;
        if($HE == $HS){
            $schCount = Schedule::where('doctorId' , $Consultation['doctor'])->where('dayId' , $day)->whereHas('time' , function($q) use($HS){
                $q->where('start' , $HS)->where('end' , $HS + 1);
            })->count();
            if($schCount > 0){$schStatus = true;}
        }else{
            $schCount1 = Schedule::where('doctorId' , $Consultation['doctor'])->where('dayId' , $day)->whereHas('time' , function($q) use($HS){
                $q->where('start' , $HS)->where('end' , $HS + 1);
            })->count();
            $schCount2 = Schedule::where('doctorId' , $Consultation['doctor'])->where('dayId' , $day)->whereHas('time' , function($q) use($HS){
                $q->where('start' , $HS + 1)->where('end' , $HS + 2);
            })->count();
            if($schCount1 > 0 && $schCount2 > 0){$schStatus = true;}
        }
        if(!$schStatus){
            $request->validate([
                'schStatus' => 'required'
            ]);
        }
        $start = $request->visitStart.':00';
        $end = date("H:i:s", $futureDate);
        $consultationStatus = Consultation::where('doctorId' , $Consultation['doctor'])
            ->where('date' , $request->date)
            ->where('status' , 'accepted')
            ->where(function($query) use($start , $end){
                $query->where('visitStart' , '>=' , $start)
                ->where('visitStart' , '<=' , $end);
            })->orWhere(function($query) use($start , $end){
                $query->where('visitEnd' , '>=' , $start)
                ->where('visitEnd' , '<=' , $end);
            })
            ->count();
        if($consultationStatus > 0){
            $request->validate([
                'consultationStatus' => 'required'
            ]);
        }
        $array = array(
            'expertise' => $Consultation['expertise'],
            'doctor' => $Consultation['doctor'],
            'type' => $Consultation['type'],
            'sessions' => $request->sessions,
            'date' => $request->date,
            'visitStart' => $request->visitStart,
            'visitTime' => $request->visitTime,
            'sick' => $request->sick
        );
        Redis::hmset('Consultation:'.$cookieName , [$cookieName => json_encode($array)]);
        $order = $this->createOrder();
        return redirect(route('consultation_order').'?num='.$order->orderNumber);
    }
    public function order(Request $request)
    {
        $request->validate([
            'num' => 'required'
        ]);
        $user = Auth::user();
        $order = Orders::where('orderNumber' , $request->num)->where('userId' , $user->id)
            ->with(['consultation' => function($query){
                $query->with(['doctor' => function($query){
                    $query->with(['user']);
                } , 'day' , 'expertise']);
            }])->first();
        if(!$order){
            abort(404);
        }
        $setting = Settings::first();
        return view('front.consultation.order' , compact('request' , 'order' , 'user' , 'setting'));
    }
    function createOrder(){
        $cookieName = Cookie::get('cookieName');
        $Consultation = json_decode(Redis::hget('Consultation:'.$cookieName , $cookieName) , True);
        $package = Packages::find(intval($Consultation['visitTime']));

        $currentDate = strtotime($Consultation['date'].' '.$Consultation['visitStart'].':00');
        $futureDate = $currentDate + (60*$package->time);
        $day = Carbon::parse($Consultation['date'])->dayOfWeek;
        if($day == 0){$day = 7;}

        $c = Consultation::create([
            'channel' => time().random_int(0000000000, 9999999999),
            'userId' => $Consultation['sick'],
            'doctorId' => $Consultation['doctor'],
            'expertiseId' => $Consultation['expertise'],
            'type' => $Consultation['type'],
            'visitStart' => $Consultation['visitStart'],
            'visitEnd' => date("H:i:s", $futureDate),
            'visitTime' => $package->time,
            'day_id' => $day,
            'sessions' => $Consultation['sessions'],
            'date' => $Consultation['date'],
        ]);
        $user = Auth::user();
        $expertise = DoctorExpertise::with(['expertise'])->where('doctorId' , $c->doctorId)->where('expertiseId' , $c->expertiseId)->where('status' , 'enable')->first();
        if($Consultation['type'] == 'video'){
            $price = $expertise->price_video;
        }elseif($Consultation['type'] == 'voice'){
            $price = $expertise->price_video;
        }else{
            $price = $expertise->price_video;
        }
        $amount = round($price + ($price * $expertise->expertise->percent / 100 ));
        $discount_amount = null;
        $total_amount = $Consultation['sessions'] * $amount;
        $payable = $total_amount;
        if($Consultation['sessions'] > 2){
            $discount_amount = ($Consultation['sessions'] * 1.5 * 100) / $price;
            $payable = round($total_amount - $discount_amount);
        }

        $payable = round($payable + ($payable * $package->percent / 100));

        $wallet = $user->wallet->credit;
        if($wallet > $payable){
            $wallet_amount = $payable;
            $payable = 0;
        }else{
            $wallet_amount = $wallet;
            $payable = $payable - $wallet;
        }
        $order = Orders::create([
            'userId' => $user->id,
            'orderNumber' => time().random_int(0, 9),
            'reqId' => $c->id,
            'discount_amount' => $discount_amount,
            'total_amount' => $total_amount,
            'wallet_amount' => $wallet_amount,
            'payable_amount' => $payable,
            'payable_doctor' => $Consultation['sessions'] * $price
        ]);
        return $order;
    }

    public function walletUsed(Request $request)
    {
        $user  = Auth::user();
        $order = Orders::where('userId' , $user->id)->where('orderNumber' , $request->num)->first();
        if($order){
            if($order->wallet_amount > 0){
                $order->payable_amount = $order->payable_amount + $order->wallet_amount;
                $order->wallet_amount = 0;
            }else{
                $wallet = $user->wallet->credit;
                if($wallet > $order->payable_amount){
                    $order->wallet_amount = $order->payable_amount;
                    $order->payable_amount = 0;
                }else{
                    $order->wallet_amount = $wallet;
                    $order->payable_amount = $order->payable_amount - $wallet;
                }
            }
            $order->save();
            return number_format($order->payable_amount);
        }else{
            return null;
        }
    }

    public function sendMessage(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'userId' => 'required',
            'receiver' => 'required',
            'message' => 'required',
        ]);
        $user       = User::find($request->userId);
        $receiver   = User::find($request->receiver);
        $doctor     = Doctor::where('userId' , $user->id)->first();
        $consultation = Consultation::where('id' , intval($request->id));
        if($doctor){
            $consultation->where(function($query) use($user , $doctor) {
                $query->where('userId', $user->id)
                      ->orWhere('doctorId', $doctor->id);
            });
            $user->avatar = $doctor->avatar;
        }else{
            $consultation->where('userId', $user->id);
        }
        $consultation = $consultation->first();
        if($consultation){
            $message = ConsultationText::create([
                'consultationId' => intval($request->id),
                'userId' => $user->id,
                'message' => $request->message,
            ]);
            $channel = md5($consultation->channel);
            if(Redis::hget('chatRoom:'.$channel , $channel)){
                $messageList = json_decode(Redis::hget('chatRoom:'.$channel , $channel) , true);
            }else{
                $messageList = array();
            }
            array_push($messageList , [
                'id' => $message->id,
                'userId' => $message->userId,
                'message' => $message->message,
                'status' => $message->status,
                'date' => $message->created_at
            ]);
            Redis::hmset('chatRoom:'.$channel , [$channel => json_encode($messageList)]);
            $data = array(
                'type' => 'message',
                'name' => $user->name,
                'avatar' => $user->avatar,
                'time' => explode(' ' ,$message->created_at)[1],
                'id' => intval($request->id)
            );
            PusherTrait::send(md5($receiver->email.$receiver->id) , 'New message' , $message->message , $data);
            return explode(' ' ,$message->created_at)[1];
        }
    }

    public function endCall(Request $request)
    {
        $user = Auth::user();
        $consultation = Consultation::where('channel' , $request->channel);
        if($user->role == 'doctor'){
            $consultation->where('doctorId' , $user->doctor->id);
            $url = route('doctorProfile_consultation_info');
        }else{
            $consultation->whereHas('order' , function($query) use($user){
                $query->where('userId' , $user->id);
            });
            $url = route('profile_consultation_info');
        }
        $consultation = $consultation->first();
        if($consultation){
            $this->done($consultation->id , $user);
            return redirect($url.'?id='.$consultation->id);
        }else{
            abort(404);
        }
    }
    function done($id , $user)
    {
        $consultation = Consultation::find($id);
        if($consultation){
            $consultation->endUser = $user->id;
            $consultation->status = 'done';
            $consultation->end = date('H:i:s');
            $consultation->save();
            $order = Orders::where('reqId' , $id)->first();
            if($order){
                $doctor = Doctor::find($consultation->doctorId);
                $wallet = Wallet::where('userId' , $doctor->userId)->first();
                $wallet->credit = $wallet->credit + $order->payable_doctor;
                $wallet->save();
            }
        }
    }
    public function report(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $pageName = 'report';
        $user   = Auth::user();
        $doctor = $user->doctor;
        $user->code = $doctor->code;
        $consultation =  $doctor->consultations()->where('id' , $request->id)->first();
        return view('front.doctor.report' , compact('pageName' , 'user' , 'consultation' , 'doctor'));
    }
    public function reportSave(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'report' => 'required'
        ]);
        $user   = Auth::user();
        $consultation =  $user->doctor->consultations()->where('id' , $request->id)->first();
        if($consultation){
            $consultation->report = $request->report;
            $consultation->report_date = Carbon::now();
            $consultation->save();
            return redirect(route('doctorProfile_consultation_info').'?id='.$request->id);
        }else{
            abort(404);
        }
    }
}
