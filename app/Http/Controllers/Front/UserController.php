<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Mail\sendingEmail;
use App\Models\Consultation;
use App\Models\ConsultationFile;
use App\Models\ConsultationText;
use App\Models\Country;
use App\Models\Doctor;
use App\Models\Notifications;
use App\Models\Orders;
use App\Models\ParentModel;
use App\Models\Settings;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use App\Traits\PusherTrait;
use App\Traits\emailNotificationTrait;


class UserController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::check()) {
            return redirect(route('index'));
        }
        $setting = Settings::first();
        return view('front.user.login' , compact('request' , 'setting'));
    }
    public function loginUser(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
        ]);
        if(Auth::attempt(['email' => $request->email , 'password' => $request->password])){
            $cookieName = Cookie::get('cookieName');
            $uri = Cache::store('redis')->get('requestUri-'.$cookieName);
            $user = Auth::user();
            if($user->hasRole(['admin'])){
                return redirect(route('dashboard'));
            }else{
                return redirect(route('index').$uri);
            }
        }else{
            return back()->with('error' , 'Your email or password is incorrect');
        }
    }
    public function forgetPassword(Request $request)
    {
        if (Auth::check()) {
            return redirect(route('index'));
        }
        $setting = Settings::first();
        return view('front.user.forget' , compact('request' , 'setting'));
    }
    public function sendRecoveryLink(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);
        $user = User::where('email' , $request->email)->first();
        if($user){
            $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => substr(str_shuffle(str_repeat($pool, 5)), 0, 60),
                'created_at' => Carbon::now()
            ]);
            $tokenData = DB::table('password_resets')->where('email', $request->email)->first();
            if ($this->sendResetEmail($request->email, $tokenData->token)) {
                return back()->with('success' , 'A password recovery link was sent to your email');
            }else{
                return back()->with('error' , 'Your email is incorrect');
            }
        }else{
            return back()->with('error' , 'Your email is incorrect');
        }
    }
    private function sendResetEmail($email, $token)
    {
    //Retrieve the user from the database
    $user = DB::table('users')->where('email', $email)->select('name', 'email')->first();
    //Generate, the password reset link. The token generated is embedded in the link
    $link = route('loginWithToken' , [$token]) . '?email=' . urlencode($user->email);
    $data = array(
        'name' => $user->name,
        'message' => $link
    );
    $dd = Mail::to($user->email)->send(new sendingEmail($data));
        try {
        //Here send the link with CURL with an external email API
            $data = array(
                'name' => $user->name,
                'message' => $link
            );
            Mail::to($user->email)->send(new sendingEmail($data));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
    public function loginWithToken(Request $request , $token)
    {
        $tokenData = DB::table('password_resets')->where('token', $token)->first();
        if($tokenData){
            $user = User::where('email', $request->email)->first();
            Auth::login($user);
            DB::table('password_resets')->where('email', $request->email)->delete();
            if($user->hasRole(['admin'])){
                return redirect(route('dashboard'));
            }else{
                return redirect(route('profile_password'));
            }
        }else{
            return redirect(route('login_form'));
        }
    }
    public function logOut(Request $request)
    {
        Auth::logout();
        return redirect(route('index'));
    }
    public function register(Request $request)
    {
        if (Auth::check()) {
            return redirect(route('index'));
        }
        $country = Country::orderBy('sort' , 'asc')->get();
        $setting = Settings::first();
        return view('front.user.register' , compact('request' , 'country' , 'setting'));
    }
    public function registerUser(Request $request)
    {
        $date = Carbon::now()->addYears(-16)->format('Y-m-d');
        $request->validate([
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z- ]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z- ]+)(\d+)?$)/u',
            'country' => 'required',
            'birthdate' => 'required|date|before:'.$date,
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:8|required_with:confirmPassword|same:confirmPassword',
            'confirmPassword' => 'required|string|min:8'
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'You are not allowed to register in the system. Please apply through your parents.',
            'email.required' => 'Please enter email',
            'email.email' => 'Invalid email entered',
            'email.unique' => 'This email is available in the system',
            'password.required' => 'Please enter password',
            'password.min' => 'The minimum number of characters must be 8',
            'password.required_with' => 'Passwords do not match',
        ]);
        $imageName = 'avatar.png';
        if(!empty($request->image)){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/user/' . $imageName, base64_decode($image));
        }

        $country = Country::find($request->country);
        $l1 = strtoupper(substr($request->first_name,0 , 1));
        $l2 = strtoupper(substr($request->last_name,0 , 1));
        $code = random_int(11111111, 99999999).$country->symbol.$l1.$l2;
        $code = $this->checkCode($code , $country->symbol.$l1.$l2);

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'name' => $request->first_name.' '.$request->last_name,
            'email' => $request->email,
            'birthdate' => $request->birthdate,
            'avatar' => $imageName,
            'country' => $request->country,
            'code' => $code,
            'password' => bcrypt($request->password)
        ]);
        Wallet::create([
            'userId' => $user->id
        ]);
        if(Auth::attempt(['email' => $request->email , 'password' => $request->password])){
            $cookieName = Cookie::get('cookieName');
            $uri = Cache::store('redis')->get('requestUri-'.$cookieName);
            return redirect(route('index').$uri);
        }else{
            return redirect(route('index'));
        }
    }
    public function updateUser(Request $request)
    {
        $user   = Auth::user();
        $date = Carbon::now()->addYears(-16)->format('Y-m-d');
        $request->validate([
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z- ]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z- ]+)(\d+)?$)/u',
            'birthdate' => 'required|date|before:'.$date,
            'email' => 'required|email|unique:users,email,'.$user->id
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'You must not be under 16 years old',
            'email.required' => 'Please enter email',
            'email.email' => 'Invalid email entered',
            'email.unique' => 'This email is available in the system'
        ]);
        $user = User::find($user->id);
        if(!empty($request->image)){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/user/' . $imageName, base64_decode($image));
            $user->avatar = $imageName;
        }
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->address = $request->address;
        $user->gender = $request->gender;
        $user->birthdate = $request->birthdate;
        $user->email = $request->email;
        $user->name = $request->first_name.' '.$request->last_name;
        $user->save();
        return back();
    }
    function checkCode($code , $string)
    {
        $user = User::where('code' , $code)->first();
        if($user){
            $code = random_int(11111111, 99999999).$string;
            $this->checkCode($code , $string);
        }else{
            return $code;
        }
    }
    public function profile(Request $request)
    {
        $user   = Auth::user();
        if($user->role == 'doctor'){
            return redirect(route('doctorProfile'));
        }
        $pageName = 'profile';
        $consultation = Consultation::with(['day', 'parent' => function($query){
            $query->with(['order']);
        },  'doctor' => function($query){
            $query->with(['user']);
        } , 'expertise'])->where(function($query) use($user){
            $query->whereHas('order' , function($query) use($user){
                $query->where('userId' , $user->id);
            })->orWhere(function($query) use($user){
                $query->whereHas('parent' , function($query) use($user){
                    $query->where('userId' , $user->id);
                });
            });
        })->where('status' , '!=' , 'created')->orderBy('date' , 'desc')->paginate(15);
        return view('front.user.profile' , compact('user' , 'consultation' , 'request' , 'pageName' ));
    }
    public function children(Request $request)
    {
        $pageName = 'family';
        $user   = Auth::user();
        $children = $user->parents()->with('user')->where('status' , '!=' , 'delete')->get();
        foreach($children as $key => $val){
                $children[$key]->age = Carbon::parse($val->user->birthdate)->diff(Carbon::now())->format('%y');
        }
        return view('front.user.children' , compact('pageName' , 'user' , 'children'));
    }
    public function childrenEdit(Request $request)
    {
        $pageName = 'family';
        $user   = Auth::user();
        $child = $user->parents()->with(['user'])->where('userId' , $request->id)->first();
        if($child){
            return view('front.user.child' , compact('pageName' , 'user' , 'child'));
        }else{
            abort(404);
        }
    }
    public function childrenAdd(Request $request)
    {
        $pageName = 'family';
        $user   = Auth::user();
        return view('front.user.children_add' , compact('pageName' , 'user' ));
    }
    public function childrenSave(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        $request->validate([
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'birthdate' => 'required|date|before:'.$date,
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'The date entered is incorrect',
        ]);
        $parent   = Auth::user();
        $country = Country::find($parent->country);
        $l1 = strtoupper(substr($request->first_name,0 , 1));
        $l2 = strtoupper(substr($request->last_name,0 , 1));
        $code = random_int(11111111, 99999999).$country->symbol.$l1.$l2;
        $code = $this->checkCode($code , $country->symbol.$l1.$l2);

        $imageName = 'avatar.png';
        if(!empty($request->image)){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/user/' . $imageName, base64_decode($image));
        }
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'name' => $request->first_name.' '.$request->last_name,
            'email' => $request->email,
            'birthdate' => $request->birthdate,
            'avatar' => $imageName,
            'code' => $code
        ]);
        Wallet::create([
            'userId' => $user->id
        ]);
        ParentModel::create([
            'parentId' => $parent->id,
            'userId' => $user->id
        ]);
        return redirect(route('profile_children'));
    }
    public function updateChildren(Request $request , $id)
    {
        $parent   = Auth::user();
        $user   = User::find($id);
        $userParent = ParentModel::where('parentId' , $parent->id)->where('userId' , $user->id)->first();
        if($userParent){
            $date = Carbon::now()->format('Y-m-d');
            $request->validate([
                'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
                'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
                'birthdate' => 'required|date|before:'.$date
            ],
            [
                'first_name.required' => 'Please enter first name',
                'first_name.regex' => 'Please use English letters',
                'last_name.required' => 'Please enter last name',
                'last_name.regex' => 'Please use English letters',
                'birthdate.required' => 'Please enter birthdate',
                'birthdate.before' => 'You must not be under 16 years old'
            ]);
            $user = User::find($user->id);
            if(!empty($request->image)){
                $image = $request->image;  // your base64 encoded
                $image = str_replace('data:image/jpeg;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
                \File::put( public_path(). '/media/user/' . $imageName, base64_decode($image));
                $user->avatar = $imageName;
            }
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->gender = $request->gender;
            $user->birthdate = $request->birthdate;
            $user->name = $request->first_name.' '.$request->last_name;
            $user->save();
        }
        return redirect(route('profile_children'));
    }
    public function settings(Request $request)
    {
        $pageName = 'settings';
        $user   = Auth::user();
        return view('front.user.settings' , compact('pageName' , 'user' ));
    }
    public function password(Request $request)
    {
        $pageName = 'password';
        $user   = Auth::user();
        return view('front.user.password' , compact('pageName' , 'user'));
    }
    public function changePassword(Request $request)
    {
        $user   = Auth::user();
        $request->validate([
            'password' => 'required|string|min:8',
        ],
        [
            'password.required' => 'Please enter password',
            'password.min' => 'The minimum number of characters must be 8',
        ]);
        $user = User::find($user->id);
        $user->password =  bcrypt($request->password);
        $user->save();
        $user->password =  $request->password;
        return back()->withErrors(['msg' => 'Password changed successfully: Email: '.$user->email.' - password: '.$user->password]);
    }
    public function childrenStatus(Request $request){
        $parent   = Auth::user();
        $user = User::find($request->id);
        if($user){
            $parent = ParentModel::where('parentId' , $parent->id)->where('userId' , $user->id)->first();
            if($parent){
                if($user->status == 'enable'){
                    $user->status = 'disable';
                }else{
                    $user->status = 'enable';
                }
                $user->save();
            }
        }
        return back();
    }
    public function childrenDelete(Request $request){
        $parent   = Auth::user();
        $user = ParentModel::where('id' , $request->id)->where('parentId' , $parent->id)->first();
        if($user){
            $user->delete();
        }
        return back();
    }
    public function consultation(Request $request)
    {
        $pageName = 'Consultation';
        $user   = Auth::user();
        $consultation = Consultation::with(['day' , 'doctor' => function($query){
            $query->with(['user']);
        } , 'expertise' , 'prescription' , 'user'])->where(function($query) use($user){
            $query->whereHas('order' , function($query) use($user){
                $query->where('userId' , $user->id);
            })->orWhere(function($query) use($user){
                $query->whereHas('parent' , function($query) use($user){
                    $query->where('userId' , $user->id);
                });
            });
        })->where('id' , $request->id)->first();

        $files = ConsultationFile::whereHas('consultation' , function($query) use($consultation){
            $query->where('userId' , $consultation->userId)
                ->where('expertiseId' , $consultation->expertise->id);
        })->orderBy('id' , 'desc')->paginate(15);

        return view('front.user.consultation' , compact('pageName' , 'user' , 'consultation' , 'files'));
    }
    public function consultationCancellation(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);
        $user   = Auth::user();
        $consultation = Consultation::where('id' , $request->id)->where('status' , 'pending')->first();
        $order   = Orders::where('reqId' , $consultation->id)->where('userId' , $user->id)->first();
        if($order){
            $consultation->status = 'canceled';
            $consultation->description = $request->description;
            $consultation->save();

            $doctor = Doctor::find($consultation->doctorId);
            $userDoctor = User::find($doctor->userId);

            $wallet  = Wallet::where('userId' , $user->id)->first();
            $wallet->credit = $wallet->credit + ( $order->payable_amount + $order->wallet_amount);
            $wallet->save();
            $title   = 'Request status';
            $message = 'Request canceled by user';
            $url = route('doctorProfile_consultation_info').'?id='.$consultation->id;
            $data = array(
                'type' => 'notifications',
                'url' => $url
            );
            Notifications::create([
                'userId' => $userDoctor->id,
                'consultationId' => $consultation->id,
                'title' => 'Counseling status',
                'description' => 'The consultation has been canceled by the user',
            ]);
            PusherTrait::send(md5($userDoctor->email.$userDoctor->id) , $title , $message , $data);
            emailNotificationTrait::send($userDoctor->email , $userDoctor->name ,  $title , $message , $url);
        }
        return back();
    }
    public function consultationText(Request $request)
    {
        $pageName = 'Consultation';
        $user   = Auth::user();
        $consultation = Consultation::with(['day', 'doctor' => function($query){
            $query->with(['user']);
        } , 'expertise' , 'user'])->whereHas('order' , function($query) use($user){
            $query->where('userId' , $user->id);
        })->where('id' , $request->id)->first();

        $files = ConsultationFile::whereHas('consultation' , function($query) use($consultation){
            $query->where('userId' , $consultation->userId)
                ->where('expertiseId' , $consultation->expertise->id);
        })->orderBy('id' , 'desc')->paginate(15);

        $channel = md5($consultation->channel);
        if(!Redis::hget('chatRoom:'.$channel , $channel)){
            $messages = ConsultationText::where('consultationId' , $consultation->id)->orderBy('id' , 'asc')->get();
            if(count($messages) > 0){
                $msg = $messages->map(function($message){
                    return [
                        'id' => $message->id,
                        'userId' => $message->userId,
                        'message' => $message->message,
                        'status' => $message->status,
                        'date' => explode(' ' ,$message->created_at)[1]
                    ];
                });
                Redis::hmset('chatRoom:'.$channel , [$channel => json_encode($msg)]);
                $messageList = json_decode(Redis::hget('chatRoom:'.$channel , $channel) , true);
            }else{
                $messageList = array();
            }
        }else{
            $messageList = json_decode(Redis::hget('chatRoom:'.$channel , $channel) , true);
        }
        return view('front.user.consultation_text' , compact('pageName' , 'user' , 'consultation' , 'messageList' , 'files'));
    }
    public function startCall(Request $request)
    {
        $user   = Auth::user();
        $consultation = Consultation::where('id' , $request->id)->where('status' , 'doing')->first();
        $order   = Orders::where('reqId' , $consultation->id)->where('userId' , $user->id)->first();
        if($order){
            if($consultation->type == 'video'){
                return redirect(route('profile_call').'?channel='.$consultation->channel.'&type=video');
            }elseif($consultation->type == 'voice'){
                return redirect(route('profile_call').'?channel='.$consultation->channel.'&type=voice');
            }else{
                return redirect(route('profile_consultationText').'?id='.$request->id);
            }
        }else{
            abort(404);
        }
    }
    public function call(Request $request)
    {
        $pageName = 'Call';
        $user   = Auth::user();
        $consultation = Consultation::where('channel' , $request->channel)->whereHas('order' , function($query) use($user){
            $query->where('userId' , $user->id);
        })->first();
        if($consultation){
            return view('front.call' , compact('pageName' , 'user' , 'request'));
        }else{
            abort(404);
        }
    }
    public function uploadFile(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'title' => 'required',
            'file' => 'required|mimes:png,jpeg,jpg,doc,docx,pdf|max:5000'
        ]);
        $user   = Auth::user();
        $uploadedFile = $request->file('file');
        $ext = $uploadedFile->getClientOriginalExtension();
        $file = time().'_'.random_int(00000000000, 9999999999).'.'.$ext;
        $uploadedFile->move(public_path() . '/media/consultation/', $file);
        $file = ConsultationFile::create([
            'userId' => $user->id,
            'consultationId' => $request->id,
            'title' => $request->title,
            'description' => $request->description,
            'file' => $file
        ]);
        $data = array(
            'type' => 'file',
            'fileId' => $file->id,
            'title' => $file->title,
            'description' => $file->description,
            'file' => $file->file,
            'time' => $file->created_at,
            'id' => intval($request->id)
        );
        $consultation = Consultation::find(intval($request->id));
        $doctor = Doctor::find(intval($consultation->doctorId));
        $receiver = User::find($doctor->userId);
        PusherTrait::send(md5($receiver->email.$receiver->id) , 'File' , 'A file was sent' , $data);
        return back();
    }

    public function notifications(Request $request)
    {
        $pageName = 'notifications';
        $user   = Auth::user();
        $notifications = Notifications::where('userId' , $user->id)->orWhere('group' , 'user')->orderBy('id' , 'desc')->paginate(15);
        return view('front.user.notifications' , compact('pageName' , 'user' , 'notifications' ,'request' ));
    }
}
