<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    public function tickets(Request $request)
    {
        $pageName   = 'Support';
        $user       = Auth::user();
        $doctor     = $user->doctor;
        if(!is_null($doctor)){
            $user->code = $doctor->code;
        }
        $tickets    = $user->tickets()->orderBy('created_at' , 'desc')->whereNull('parentId')->paginate(15);
        return view('front.user.tickets' , compact('pageName' , 'user' , 'tickets' , 'doctor'));
    }
    public function ticket(Request $request)
    {
        $pageName   = 'Support';
        $user       = Auth::user();
        $doctor     = $user->doctor;
        if(!is_null($doctor)){
            $user->code = $doctor->code;
        }
        $ticket     = Ticket::where('id' , $request->id)->where('userId' , $user->id)->with(['tickets' => function($query){
            $query->orderBy('created_at' , 'desc');
        }])->first();
        return view('front.user.ticket' , compact('pageName' , 'user' , 'ticket' , 'doctor'));
    }
    public function add(Request $request)
    {
        $pageName   = 'Add message';
        $user       = Auth::user();
        $doctor     = $user->doctor;
        if(!is_null($doctor)){
            $user->code = $doctor->code;
        }
        return view('front.user.addTicket' , compact('pageName' , 'user' , 'doctor'));
    }
    public function save(Request $request)
    {
        $request->validate([
            'subject' => 'required',
            'text' => 'required',
            'file' => 'mimes:png,jpeg,jpg|max:5000'
        ]);
        $file = null;
        if(!empty($request->file)){
            $uploadedFile = $request->file('file');
            $ext = $uploadedFile->getClientOriginalExtension();
            $file = time().'_'.random_int(00000000000, 9999999999).'.'.$ext;
            $uploadedFile->move(public_path() . '/media/ticket/', $file);
        }
        $user       = Auth::user();
        $ticket     = Ticket::create([
            'userId' => $user->id,
            'subject' => $request->subject,
            'text' => $request->text,
            'file' => $file
        ]);
        return redirect(route('profile_ticket').'?id='.$ticket->id);
    }
    public function saveComment(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'text' => 'required',
            'file' => 'mimes:png,jpeg,jpg|max:5000'
        ]);
        $user       = Auth::user();
        $ticket     = Ticket::where('id' , $request->id)->where('userId' , $user->id)->where('status' , 'open')->first();
        if(!$ticket){
            abort(404);
        }
        Ticket::where('id' , $request->id)->update(['updated_at' => Carbon::now()]);
        $file = null;
        if(!empty($request->file)){
            $uploadedFile = $request->file('file');
            $ext = $uploadedFile->getClientOriginalExtension();
            $file = time().'_'.random_int(00000000000, 9999999999).'.'.$ext;
            $uploadedFile->move(public_path() . '/media/ticket/', $file);
        }
        Ticket::create([
            'userId' => $user->id,
            'parentId' => $ticket->id,
            'text' => $request->text,
            'file' => $file
        ]);
        return redirect(route('profile_ticket').'?id='.$ticket->id);
    }
}
