<?php

namespace App\Http\Controllers\Front;

use App\Exports\ReportCheckouts;
use App\Exports\ReportOrders;
use App\Http\Controllers\Controller;
use App\Models\Checkout;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class WalletController extends Controller
{
    public function index(Request $request)
    {
        $pageName = 'Financial settlement';
        $user   = Auth::user();
        $wallet = Wallet::where('userId' , $user->id)->first();
        $checkouts = $user->checkout()->paginate(15);
        $banks = $user->banks()->where('status' , 'enable')->get();
        return view('front.user.wallet' , compact('pageName' , 'user' , 'banks' , 'wallet' , 'checkouts'));
    }
    public function charge(Request $request)
    {
        $request->validate([
            'amount' => 'required|integer'
        ],
        [
            'amount.required' => 'Please enter amount',
            'amount.integer' => 'The amount must be an integer'
        ]);
    }
    public function checkoutNew(Request $request)
    {
        $user = Auth::user();
        $wallet = Wallet::where('userId' , $user->id)->first();
        if($wallet->credit > 0){
            Checkout::create([
                'userId' => $user->id,
                'bankId' => $request->bank,
                'userType' => $request->userType,
                'amount' => $wallet->credit
            ]);
            $wallet->credit = 0;
            $wallet->save();
        }
        return back();
    }
    public function report(Request $request)
    {
        $user   = Auth::user();
        $pageName = 'financial report';
        $doctor = $user->doctor;
        return view('front.doctor.financial_report' , compact('user' , 'doctor' , 'pageName' ));
    }
    public function reportCheckouts(Request $request)
    {
        $file = 'report_checkouts'.date('-Y_m_d-H_i_s').'.xlsx';
        return Excel::download(new ReportCheckouts($request), $file);
    }
    public function reportOrders(Request $request)
    {
        $file = 'report_orders'.date('-Y_m_d-H_i_s').'.xlsx';
        return Excel::download(new ReportOrders($request), $file);
    }
}
