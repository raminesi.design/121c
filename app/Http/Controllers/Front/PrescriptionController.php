<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Consultation;
use App\Models\Prescription;
use App\Models\PrescriptionDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PrescriptionController extends Controller
{
    public function prescription(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $pageName = 'prescription';
        $user   = Auth::user();
        $prescription = Consultation::where(function($query) use($user){
            $query->where('userId' , $user->id)
            ->orWhere(function($query) use($user){
                $query->whereHas('parent' , function($query) use($user){
                    $query->whereHas('order' , function($query) use($user){
                        $query->where('userId' , $user->id);
                    });
                });
            });
        })->where('id' , $request->id)->with(['prescription' => function($query){
            $query->with(['details']);
        }])->first();
        return view('front.user.prescription' , compact('pageName' , 'user' , 'prescription'));
    }
    public function prescriptionDoctor(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $pageName = 'prescription';
        $user   = Auth::user();
        $doctor = $user->doctor;
        $user->code = $doctor->code;
        $prescription =  $doctor->consultations()->where('id' , $request->id)->with(['prescription' => function($query){
            $query->with(['details']);
        }])->first();
        return view('front.doctor.prescription' , compact('pageName' , 'user' , 'prescription' , 'doctor'));
    }
    public function deleteItem(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $user   = Auth::user();
        $item =  $user->doctor->consultations()->whereHas('prescription' , function($query) use($request){
            $query->whereHas('details' , function($query) use($request){
                $query->where('id' , $request->id);
            });
        })->first();
        if($item){
            PrescriptionDetails::where('id' , $request->id)->delete();
        }
        return back();
    }
    public function delete(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $user   = Auth::user();
        $item =  $user->doctor->consultations()->whereHas('prescription' , function($query) use($request){
            $query->where('id' , $request->id);
        })->first();
        if($item){
            Prescription::where('id' , $request->id)->delete();
            return redirect(route('doctorProfile_consultation_info').'?id='.$item->id);
        }else{
            abort(404);
        }

    }
    public function add(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $pageName = 'add prescription';
        $user   = Auth::user();
        $doctor = $user->doctor;
        $user->code = $doctor->code;
        $consultation =  $user->doctor->consultations()->with(['prescription'])->where('id' , $request->id)->first();
        if(is_null($consultation->prescription)){
            return view('front.doctor.prescriptionAdd' , compact('pageName' , 'user' , 'doctor' , 'consultation'));
        }else{
            return redirect(route('doctorProfile_prescription').'?id='.$request->id);
        }
    }
    public function save(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'description' => 'required',
        ]);
        $prescription = Prescription::create([
            'consultationId' => $request->id,
            'description' => $request->description
        ]);
        $insert = array();
        for($i = 0 ; $i < count($request->name) ; $i++){
            array_push($insert , array(
                'prescriptionId' => $prescription->id,
                'name' => $request->name[$i],
                'description' => $request->consumption[$i],
            ));
        }
        PrescriptionDetails::insert($insert);
        return redirect(route('doctorProfile_prescription').'?id='.$request->id);
    }
    public function searchAjax(Request $request)
    {
        $items = PrescriptionDetails::where('name' , 'like' , '%' . $request->search . '%')->get();
        $result = [];
        foreach ($items as $item) {
            if (empty($item->id))
                continue;
            $temp = ["value" => $item->name ];
            array_push($result, $temp);
        }
        return response()->json($result);
    }
}
