<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Checkout;
use App\Models\Consultation;
use App\Models\ConsultationFile;
use App\Models\ConsultationText;
use App\Models\Days;
use App\Models\Doctor;
use App\Models\DoctorExpertise;
use App\Models\Expertise;
use App\Models\Notifications;
use App\Models\Orders;
use App\Models\Schedule;
use App\Models\Times;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use App\Traits\PusherTrait;
use App\Traits\emailNotificationTrait;

class DoctorController extends Controller
{
    public function profile(Request $request)
    {
        $user   = Auth::user();
        $pageName = 'profile';
        $doctor = $user->doctor;
        $consultation = $user->doctor->consultations()->with(['day' , 'parent' => function($query){
            $query->with(['order']);
        } , 'order' , 'user' , 'expertise'])->where('status' , '!=' , 'created')->orderBy('date' , 'desc')->paginate(15);
        $user->code = $doctor->code;
        return view('front.doctor.profile' , compact('user' , 'doctor' , 'consultation' , 'request' , 'pageName' ));
    }
    public function settings(Request $request)
    {
        $pageName = 'settings';
        $user   = Auth::user();
        $doctor = $user->doctor;
        $user->code = $doctor->code;
        return view('front.doctor.settings' , compact('pageName' , 'user' , 'doctor'));
    }
    public function password(Request $request)
    {
        $pageName = 'password';
        $user   = Auth::user();
        $doctor = $user->doctor;
        $user->code = $doctor->code;
        return view('front.doctor.password' , compact('pageName' , 'user' , 'doctor'));
    }
    public function checkout(Request $request)
    {
        $pageName = 'checkout';
        $user   = Auth::user();
        $doctor = $user->doctor;
        $checkouts = $user->checkout()->paginate(15);
        $user->code = $doctor->code;
        $banks = $user->banks()->where('status' , 'enable')->get();
        return view('front.doctor.checkout' , compact('pageName' , 'banks' , 'user' , 'doctor' , 'checkouts'));
    }

    public function changePassword(Request $request)
    {
        $user   = Auth::user();
        $request->validate([
            'password' => 'required|string|min:8',
        ],
        [
            'password.required' => 'Please enter password',
            'password.min' => 'The minimum number of characters must be 8',
        ]);
        $user = User::find($user->id);
        $user->password =  bcrypt($request->password);
        $user->save();
        $user->password =  $request->password;
        return back()->withErrors(['msg' => 'Password changed successfully: Email: '.$user->email.' - password: '.$user->password]);
    }
    public function updateUser(Request $request)
    {
        $user   = Auth::user();
        $date = Carbon::now()->addYears(-25)->format('Y-m-d');
        $request->validate([
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'birthdate' => 'required|date|before:'.$date,
            'email' => 'required|email|unique:users,email,'.$user->id,
            'mobile' => 'required|size:11|unique:users,mobile,'.$user->id,
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'You must not be under 16 years old',
            'email.required' => 'Please enter email',
            'email.email' => 'Invalid email entered',
            'email.unique' => 'This email is available in the system',
            'mobile.required' => 'Please enter mobile',
            'mobile.size' => 'Invalid mobile entered',
            'mobile.unique' => 'This mobile is available in the system'
        ]);
        $user = User::find($user->id);
        if(!empty($request->image)){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/user/' . $imageName, base64_decode($image));
            $user->avatar = $imageName;
        }
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->gender = $request->gender;
        $user->birthdate = $request->birthdate;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->name = $request->first_name.' '.$request->last_name;
        $user->save();
        return back();
    }
    public function activityStatus(Request $request)
    {
        $user   = Auth::user();
        $doctor = Doctor::find($user->doctor->id);
        if($doctor){
            if($doctor->active){
                $doctor->active = false;
            }else{
                $doctor->active = true;
            }
            $doctor->save();
        }
        return true;
    }
    public function consultation(Request $request)
    {
        $pageName = 'Consultation';
        $user   = Auth::user();
        $doctor = $user->doctor;
        $user->code = $doctor->code;
        $consultation = $user->doctor->consultations()->with(['day' , 'order' , 'user' , 'prescription' , 'expertise'])->where('id' , $request->id)->first();
        $consultation->age = Carbon::parse($consultation->user->birthdate)->diff(Carbon::now())->format('%y');

        $files = ConsultationFile::whereHas('consultation' , function($query) use($consultation){
            $query->where('userId' , $consultation->user->id)
                ->where('expertiseId' , $consultation->expertise->id);
        })->orderBy('id' , 'desc')->paginate(15);

        return view('front.doctor.consultation' , compact('pageName' , 'user' , 'consultation' , 'files' , 'doctor'));
    }
    public function consultationAccept(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);
        $user   = Auth::user();
        $consultation = Consultation::where('id' , $request->id)->where('doctorId' , $user->doctor->id)->where('status' , 'pending')->first();
        if($consultation){
            $consultation->status = 'accepted';
            $consultation->save();
        }
        $title = 'Request status';
        $message = 'Your request has been approved by your doctor';
        $url = route('profile_consultation_info').'?id='.$consultation->id;
        $data = array(
            'type' => 'notifications',
            'url' => $url
        );
        Notifications::create([
            'userId' => $consultation->order->user->id,
            'consultationId' => $consultation->id,
            'title' => 'Counseling status',
            'description' => 'Your consultation has been approved by your doctor',
        ]);
        PusherTrait::send(md5($consultation->order->user->email.$consultation->order->user->id) , $title , $message , $data);
        emailNotificationTrait::send($consultation->order->email , $consultation->order->name ,  $title , $message , $url);
        return back();
    }
    public function consultationReject(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);
        $user   = Auth::user();
        $consultation = Consultation::where('id' , $request->id)->where('doctorId' , $user->doctor->id)->where('status' , 'pending')->first();
        if($consultation){
            $consultation->status = 'rejected';
            $consultation->description = $request->description;
            $consultation->save();

            $wallet  = Wallet::where('userId' , $consultation->order->user->id)->first();
            $wallet->credit = $wallet->credit + $consultation->order->payable_amount;
            $wallet->save();
            $title   = 'Request status';
            $message = 'Your request has been denied by your doctor';
            $url = route('profile_consultation_info').'?id='.$consultation->id;
            $data = array(
                'type' => 'notifications',
                'url' => $url
            );
            Notifications::create([
                'userId' => $consultation->order->user->id,
                'consultationId' => $consultation->id,
                'title' => 'Counseling status',
                'description' => 'Your consultation has rejected',
            ]);
            PusherTrait::send(md5($consultation->order->user->email.$consultation->order->user->id) , $title , $message , $data);
            emailNotificationTrait::send($consultation->order->user->email , $consultation->order->user->name ,  $title , $message , $url);
        }
        return back();
    }
    public function consultationText(Request $request)
    {
        $pageName = 'Consultation';
        $user   = Auth::user();
        $doctor = $user->doctor;
        $user->code = $doctor->code;
        $consultation = $user->doctor->consultations()->with(['day' , 'order' , 'user' , 'expertise'])->where('id' , $request->id)->first();
        $files = ConsultationFile::whereHas('consultation' , function($query) use($consultation){
            $query->where('userId' , $consultation->user->id)
                ->where('expertiseId' , $consultation->expertise->id);
        })->orderBy('id' , 'desc')->paginate(15);
        $channel = md5($consultation->channel);
        if(!Redis::hget('chatRoom:'.$channel , $channel)){
            $messages = ConsultationText::where('consultationId' , $consultation->id)->orderBy('id' , 'asc')->get();
            if(count($messages) > 0){
                $msg = $messages->map(function($message){
                    return [
                        'id' => $message->id,
                        'userId' => $message->userId,
                        'message' => $message->message,
                        'status' => $message->status,
                        'date' => explode(' ' ,$message->created_at)[1]
                    ];
                });
                Redis::hmset('chatRoom:'.$channel , [$channel => json_encode($msg)]);
                $messageList = json_decode(Redis::hget('chatRoom:'.$channel , $channel) , true);
            }else{
                $messageList = array();
            }
        }else{
            $messageList = json_decode(Redis::hget('chatRoom:'.$channel , $channel) , true);
        }
        return view('front.doctor.consultation_text' , compact('pageName' , 'user' , 'consultation' , 'messageList' , 'doctor' , 'files' ));
    }
    public function startCall(Request $request)
    {
        $user   = Auth::user();
        $consultation = Consultation::where('id' , $request->id)->where('doctorId' , $user->doctor->id)->whereIn('status' , ['accepted' , 'doing'])->first();
        if($consultation){
            if($consultation->status == 'accepted'){
                $consultation->status = 'doing';
                $consultation->start = date('H:i:s');
                $consultation->save();

                $title   = 'The consultation began';
                $message = 'Your consultation has started';
                $url = route('profile_consultation_info').'?id='.$consultation->id;
                $data = array(
                    'type' => 'notifications',
                    'url' => $url
                );
                if(is_null($consultation->order)){
                    $user = $consultation->parent->order->user;
                }else{
                    $user = $consultation->order->user;
                }
                Notifications::create([
                    'userId' => $user->id,
                    'consultationId' => $consultation->id,
                    'title' => 'Counseling status',
                    'description' => 'Your consultation has started',
                ]);
                PusherTrait::send(md5($user->email.$user->id) , $title , $message , $data);
                emailNotificationTrait::send($user->email , $user->name ,  $title , $message , $url);
            }

            if($consultation->type == 'video'){
                return redirect(route('doctorProfile_call').'?channel='.$consultation->channel.'&type=video');
            }elseif($consultation->type == 'voice'){
                return redirect(route('doctorProfile_call').'?channel='.$consultation->channel.'&type=voice');
            }else{
                return redirect(route('doctorProfile_consultationText').'?id='.$request->id);
            }
        }else{
            abort(404);
        }
    }
    public function call(Request $request)
    {
        $pageName = 'Call';
        $user   = Auth::user();
        $consultation = Consultation::where('channel' , $request->channel)->where('doctorId' , $user->doctor->id)->first();
        if($consultation){
            return view('front.call' , compact('pageName' , 'user' , 'request'));
        }else{
            abort(404);
        }
    }

    public function schedule(Request $request)
    {
        $pageName = 'schedule';
        $user   = Auth::user();
        $doctor = $user->doctor;
        $user->code = $doctor->code;
        $schedule       = Schedule::where('doctorId' , $doctor->id)->get();
        $times          = Times::get();
        $arraySchedule  = array();
        foreach($schedule as $val){
            array_push($arraySchedule , $val->dayId.'-'.$val->timeId);
        }
        return view('front.doctor.schedule' , compact('pageName' , 'user' , 'doctor' , 'times' , 'arraySchedule'));
    }
    public function doctorTime(Request $request){
        $schedule = Schedule::where('doctorId' , $request->doctor_id )->where('dayId' , $request->day_id )->where('timeId' , $request->time_id )->first();
        if($schedule){
            $schedule->delete();
        }else{
            $schedule = Schedule::create([
                'doctorId' => $request->doctor_id,
                'dayId' => $request->day_id,
                'timeId' => $request->time_id
                ]);
        }
        return $schedule;
    }
    public function doctorAllTime(Request $request){
        $schedule = Schedule::where('doctorId' , $request->doctor_id )->where('dayId' , $request->day_id )->delete();
        $data = array();
        if($request->status == 0){
            $times = Times::get();
            foreach($times as $time){
                $schedule = array(
                    'doctorId' => $request->doctor_id,
                    'dayId' => $request->day_id,
                    'timeId' => $time->id,
                );
                array_push($data , $schedule);
            }
            Schedule::insert($data);
        }
        return $data;
    }
    public function doctorAllDay(Request $request){
        $schedule = Schedule::where('doctorId' , $request->doctor_id )->where('timeId' , $request->time_id )->delete();
        $data = array();
        if($request->status == 0){
            $days = Days::get();
            foreach($days as $day){
                $schedule = array(
                    'doctorId' => $request->doctor_id,
                    'dayId' => $day->id,
                    'timeId' => $request->time_id,
                );
                array_push($data , $schedule);
            }
            Schedule::insert($data);
        }
        return $data;
    }
    public function expertise(Request $request)
    {
        $pageName = 'expertise';
        $user   = Auth::user();
        $doctor = $user->doctor;
        $user->code = $doctor->code;
        $allExpertise   = Expertise::where('status' , 'enable')->get();
        $expertise      = DoctorExpertise::join('expertise' , 'expertise.id' , '=' , 'doctor_expertise.expertiseId')->where('doctor_expertise.doctorId' ,$doctor->id)->where('doctor_expertise.status' , 'enable')->select('expertise.id' , 'doctor_expertise.price_video' , 'doctor_expertise.price_voice' , 'doctor_expertise.price_text' )->first();
        if(!empty($request->id)){
            $priceRange = collect($allExpertise)->where('id' , $request->id)->first();
            $exp = array(
                'id' => $priceRange->id,
                'price_video' => $priceRange->price_video_min,
                'price_voice' => $priceRange->price_voice_min,
                'price_text' => $priceRange->price_text_min,
                'price_video_status' => false,
                'price_voice_status' => false,
                'price_text_status' => false
            );
        }elseif($expertise){
            $exp = array(
                'id' => $expertise->id,
                'price_video' => $expertise->price_video,
                'price_voice' => $expertise->price_voice,
                'price_text' => $expertise->price_text,
                'price_video_status' => ($expertise->price_video > 0 ? true : false),
                'price_voice_status' => ($expertise->price_voice > 0 ? true : false),
                'price_text_status' => ($expertise->price_text > 0 ? true : false)
            );
            $priceRange = collect($allExpertise)->where('id' , $expertise->id)->first();
        }else{
            $priceRange = $allExpertise[0];
            $exp = array(
                'id' => $priceRange->id,
                'price_video' => $priceRange->price_video_min,
                'price_voice' => $priceRange->price_voice_min,
                'price_text' => $priceRange->price_text_min,
                'price_video_status' => false,
                'price_voice_status' => false,
                'price_text_status' => false
            );
        }
        return view('front.doctor.expertise' , compact('pageName' , 'user' , 'doctor' , 'exp' , 'priceRange' , 'allExpertise' ,'request' ));
    }
    public function expertiseSave(Request $request)
    {
        $expertise = Expertise::find($request->expertise);
        if($expertise){
            $user   = Auth::user();
            $doctor = Doctor::where('userId' , $user->id)->first();
            DoctorExpertise::where('doctorId' , $doctor->id)->update(['status' => 'disable']);
            $expDoctor = DoctorExpertise::where('doctorId' , $doctor->id)->where('expertiseId' , $expertise->id)->first();
            if($expDoctor){
                $expDoctor->status = 'enable';
                if(!empty($request->price_video_status)){
                    $expDoctor->price_video = $request->price_video;
                }else{
                    $expDoctor->price_video = -1;
                }
                if(!empty($request->price_voice_status)){
                    $expDoctor->price_voice = $request->price_voice;
                }else{
                    $expDoctor->price_voice = -1;
                }
                if(!empty($request->price_text_status)){
                    $expDoctor->price_text = $request->price_text;
                }else{
                    $expDoctor->price_text = -1;
                }
                $expDoctor->save();
            }else{
                DoctorExpertise::create([
                    'doctorId' => $doctor->id,
                    'expertiseId' => $expertise->id,
                    'price_video' => (!empty($request->price_video_status) ? $request->price_video : -1),
                    'price_voice' => (!empty($request->price_voice_status) ? $request->price_voice : -1),
                    'price_text' => (!empty($request->price_text_status) ? $request->price_text : -1)
                ]);
            }
        }
        return redirect(route('doctorProfile_expertise'));
    }

    public function uploadFile(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'title' => 'required',
            'file' => 'required|mimes:png,jpeg,jpg,doc,docx,pdf|max:5000'
        ]);
        $user   = Auth::user();
        $uploadedFile = $request->file('file');
        $ext = $uploadedFile->getClientOriginalExtension();
        $file = time().'_'.random_int(00000000000, 9999999999).'.'.$ext;
        $uploadedFile->move(public_path() . '/media/consultation/', $file);
        $file = ConsultationFile::create([
            'userId' => $user->id,
            'sender' => 'doctor',
            'consultationId' => $request->id,
            'title' => $request->title,
            'description' => $request->description,
            'file' => $file
        ]);
        $data = array(
            'type' => 'file',
            'fileId' => $file->id,
            'title' => $file->title,
            'description' => $file->description,
            'file' => $file->file,
            'time' => $file->created_at,
            'id' => intval($request->id)
        );

        $consultation = Consultation::find($request->id);
        if(is_null($consultation->order)){
            $receiver = $consultation->parent->order->user;
        }else{
            $receiver = $consultation->order->user;
        }
        PusherTrait::send(md5($receiver->email.$receiver->id) , 'File' , 'A file was sent' , $data);
        return back();
    }

    public function notifications(Request $request)
    {
        $pageName = 'notifications';
        $user   = Auth::user();
        $doctor = $user->doctor;
        $user->code = $doctor->code;
        $notifications = Notifications::where('userId' , $user->id)->orWhere('group' , 'doctor')->orderBy('id' , 'desc')->paginate(15);
        return view('front.doctor.notifications' , compact('pageName' , 'user' , 'doctor' , 'notifications' ,'request' ));
    }
    public function consultationNextSession(Request $request)
    {
        $pageName = 'Next session';
        $user     = Auth::user();
        $doctor   = $user->doctor;
        $user->code = $doctor->code;
        $Consultation = Consultation::find($request->id);
        $ConsultationOld = Consultation::where('doctorId' , $Consultation->doctorId)->where('status' , 'accepted')->whereDate('date' , '>=' , date('Y-m-d'))->select('scheduleId')->get();
        $scheduleIds = array();
        foreach($ConsultationOld as $valC){
            array_push($scheduleIds , $valC->scheduleId);
        }
        $Schedule = array();
        for($i = 0 ; $i < 7 ; $i++){
            $date    = Carbon::now()->addDay($i)->format('Y-m-d');
            $dayNum  = Carbon::now()->addDay($i)->dayOfWeek;
            $dayName = Carbon::now()->addDay($i)->dayName;
            if($dayNum == 0){$dayNum = 7;}
            $times = Schedule::join('times' , 'times.id' , '=' , 'schedule.timeId')
                    ->where('schedule.doctorId' , $Consultation->doctorId)
                    ->whereNotIn('schedule.id' , $scheduleIds)
                    ->where('times.status' , 'enable');
                    if($i == 0){
                        $times->where('schedule.timeId' , '>' , date('H'));
                    }
                    $times = $times->where('schedule.dayId' , $dayNum)
                    ->select('schedule.id' , 'times.title')
                    ->get();
            $arrayTimes = array();
            foreach($times as $val){
                array_push($arrayTimes , array(
                    'id' => $val->id,
                    'title' => $val->title,
                ));
            }
            if(count($arrayTimes) > 0){
                $array = array(
                    'date' => $date,
                    'id' => $dayNum,
                    'name' => $dayName,
                    'times' => $arrayTimes
                );
                array_push($Schedule , $array);
            }
        }
        return view('front.doctor.session' , compact('pageName' , 'user' , 'doctor' , 'Schedule' ,'request' ));
    }
    public function consultationNextSessionSave(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'date' => 'required',
            'time' => 'required'
        ]);
        $user     = Auth::user();
        $Consultation = Consultation::where('id' , $request->id)->where('doctorId' , $user->doctor->id)->whereNull('childeId')->where('sessions' , '>' , 1)->first();
        if($Consultation){
            $session = Consultation::create([
                'channel' => time().random_int(0000000000, 9999999999),
                'userId' => $Consultation->userId,
                'doctorId' => $Consultation->doctorId,
                'expertiseId' => $Consultation->expertiseId,
                'type' => $Consultation->type,
                'scheduleId' => $request->time,
                'sessions' => $Consultation->sessions - 1,
                'date' => $request->date,
                'parentId' => (is_null($Consultation->parentId) ? $Consultation->id : $Consultation->parentId),
                'status' => 'accepted',
            ]);
            $Consultation->childeId = $session->id;
            $Consultation->save();

            $cons = Consultation::find($session->parentId);
            $title   = 'Next session';
            $message = 'Your next meeting is on '.$request->date.' from '.$session->schedule->time->start.' to '.$session->schedule->time->end;
            $url = route('profile_consultation_info').'?id='.$cons->id;
            $data = array(
                'type' => 'notifications',
                'url' => $url
            );
            Notifications::create([
                'userId' => $cons->order->user->id,
                'consultationId' => $cons->id,
                'title' => $title,
                'description' => $message
            ]);
            PusherTrait::send(md5($cons->order->user->email.$cons->order->user->id) , $title , $message , $data);
            emailNotificationTrait::send($cons->order->user->email , $cons->order->user->name ,  $title , $message , $url);
            return redirect(route('doctorProfile_consultation_info').'?id='.$session->id);
        }else{
            abort(404);
        }
    }
}
