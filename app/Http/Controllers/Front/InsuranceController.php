<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Insurance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InsuranceController extends Controller
{
    public function Insurances(Request $request)
    {
        $pageName = 'Insurances';
        $user  = Auth::user();
        $child = $user->parents()->pluck('userId')->toArray();
        if($child){
            array_push($child , $user->id);
        }else{
            $child = array($user->id);
        }
        $insurances = Insurance::whereIn('userId' , $child)->where('status' , '!=' , 'delete')->with(['user'])->orderBy('id' , 'desc')->get();
        return view('front.user.insurances' , compact('user' , 'insurances' , 'request' , 'pageName' ));
    }
    public function add(Request $request)
    {
        $pageName = 'Add insurance';
        $user  = Auth::user();
        $children = $user->parents()->with(['user'])->get();
        return view('front.user.addInsurance' , compact('user' , 'pageName' , 'children'));

    }
    public function save(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required'
        ]);
        Insurance::create([
            'userId' => $request->userId,
            'name' => $request->name,
            'code' => $request->code
        ]);
        return redirect(route('profile_insurances'));
    }
    public function status(Request $request)
    {
        $user  = Auth::user();
        $child = $user->parents()->pluck('userId')->toArray();
        if($child){
            array_push($child , $user->id);
        }else{
            $child = array($user->id);
        }
        $insurance = Insurance::whereIn('userId' , $child)->where('id' , $request->id)->first();
        if($insurance){
            if($insurance->status == 'enable'){
                $insurance->status = 'disable';
            }else{
                $insurance->status = 'enable';
            }
            $insurance->save();
        }
        return back();
    }
    public function delete(Request $request)
    {
        $user  = Auth::user();
        $child = $user->parents()->pluck('userId')->toArray();
        if($child){
            array_push($child , $user->id);
        }else{
            $child = array($user->id);
        }
        $insurance = Insurance::whereIn('userId' , $child)->where('id' , $request->id)->first();
        if($insurance){
            $insurance->status = 'delete';
            $insurance->save();
        }
        return back();
    }
}
