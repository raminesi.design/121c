<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Banks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BankController extends Controller
{
    public function Banks(Request $request)
    {
        $pageName = 'banks';
        $user     = Auth::user();
        $doctor   = $user->doctor;
        if(!is_null($doctor)){
            $user->code = $doctor->code;
        }
        $banks = $user->banks()->where('status' , '!=' , 'delete')->get();
        return view('front.user.banks' , compact('user' , 'doctor' , 'banks' , 'pageName'));
    }
    public function add(Request $request)
    {
        $pageName = 'Add bank';
        $user  = Auth::user();
        $doctor   = $user->doctor;
        if(!is_null($doctor)){
            $user->code = $doctor->code;
        }
        return view('front.user.addBank' , compact('user' , 'doctor' , 'pageName'));

    }
    public function save(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required'
        ]);
        $user  = Auth::user();
        Banks::create([
            'userId' => $user->id,
            'name' => $request->name,
            'code' => $request->code
        ]);
        return redirect(route('profile_banks'));
    }
    public function status(Request $request)
    {
        $user  = Auth::user();
        $bank = Banks::where('userId' , $user->id)->where('id' , $request->id)->first();
        if($bank){
            if($bank->status == 'enable'){
                $bank->status = 'disable';
            }else{
                $bank->status = 'enable';
            }
            $bank->save();
        }
        return back();
    }
    public function delete(Request $request)
    {
        $user  = Auth::user();
        $bank = Banks::where('userId' , $user->id)->where('id' , $request->id)->first();
        if($bank){
            $bank->status = 'delete';
            $bank->save();
        }
        return back();
    }
}
