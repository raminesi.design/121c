<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\ContactModel;
use App\Models\Doctor;
use App\Models\Settings;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $doctors = Doctor::with(['user' , 'expertises' => function($query){
            $query->with(['expertise'])->where('status' , 'enable');
        }])
        ->orderBy('rate' , 'desc')->where('status' , 'enable')->limit(4)->get();
        $setting = Settings::first();
        return view('front.index' , compact('request' , 'doctors' , 'setting'));
    }

    public function contact(Request $request)
    {
        if(empty($request->name)){return 'Please enter at least 4 chars';}
        if(empty($request->email)){return 'Please enter a valid email';}
        if(empty($request->subject)){return 'Please enter at least 8 chars of subject';}
        if(empty($request->message)){return 'Please write something for us';}
        if(strlen($request->message) > 250){return 'Your text length should not exceed 250 characters';}
        $ip     = $request->ip();
        $count = ContactModel::where('ip' , $ip)->whereDate('created_at', Carbon::today())->count();
        if($count < 6){
            ContactModel::create([
                'ip' => $ip,
                'name' => $request->name,
                'email' => $request->email,
                'subject' => $request->subject,
                'message' => $request->message
            ]);
            return 'OK';
        }else{
            return 'You can not send messages more than 5 times a day';
        }
    }
}
