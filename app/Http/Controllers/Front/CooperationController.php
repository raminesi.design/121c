<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Cooperation;
use App\Models\Settings;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CooperationController extends Controller
{
    public function index(Request $request)
    {
        $setting = Settings::first();
        return view('front.cooperation.index' , compact('request' , 'setting'));
    }
    public function save(Request $request)
    {
        $date = Carbon::now()->addYears(-25)->format('Y-m-d');
        $request->validate([
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'birthdate' => 'required|date|before:'.$date,
            'mobile' => 'required|size:11|unique:users',
            'email' => 'required|email|unique:users',
            'image' => 'required',
            'expertise' => 'required',
            'description' => 'required',
            'g-recaptcha-response' => 'required',
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'You are not allowed to register your information',
            'mobile.required' => 'Please enter mobile',
            'mobile.size' => 'Invalid mobile entered',
            'mobile.unique' => 'This mobile is available in the system',
            'email.required' => 'Please enter email',
            'email.email' => 'Invalid email entered',
            'email.unique' => 'This email is available in the system',
            'image.required' => 'Please select a profile picture',
            'expertise.required' => 'Please enter your specialty',
            'description.required' => 'Please enter a description',
            'g-recaptcha-response.required' => 'Please enter the captcha'
        ]);
        $input  = $request->all();
        $ip     = $request->ip();
        $secret = '6LfeqVsaAAAAAPGkIJj9bw0OkykRqhe6EesFZhv5';
        $verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$input['g-recaptcha-response']."&remoteip=".$ip);
        $captcha_success = json_decode($verify);
        if ($captcha_success->success==false) {
            $request->validate([
                'g-recaptcha-response-1' => 'required',
                ]);
        }
        $cooperation = Cooperation::where('ip' , $ip)->count();
        if($cooperation < 4){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/doctor/' . $imageName, base64_decode($image));
            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'name' => $request->first_name.' '.$request->last_name,
                'avatar' => $imageName,
                'birthdate' => $request->birthdate,
                'gender' => $request->gender,
                'mobile' => $request->mobile,
                'email' => $request->email
            ]);
            Cooperation::create([
                'userId' => $user->id,
                'avatar' => $imageName,
                'expertise' => $request->expertise,
                'description' => $request->description,
                'ip' => $ip,
            ]);
            return back()->withErrors(['msg' => 'Your request has been sent successfully. Please wait for the call of our experts']);
        }else{
            $request->validate([
                'request_max' => 'required',
            ]);
        }
    }
}
