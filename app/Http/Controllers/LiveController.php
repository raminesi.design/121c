<?php

namespace App\Http\Controllers;

use App\Models\Consultation;
use App\Models\Doctor;
use App\Models\User;
use Illuminate\Http\Request;

class LiveController extends Controller
{
    public function call(Request $request)
    {
        $pageName = 'Call';
        if($request->userType == 'user'){
            $user = User::where('code' , $request->userCode)->first();
            if($user){
                $consultation = Consultation::where('channel' , $request->channel)->where('userId' , $user->id)->first();
            }
        }else{
            $doctor = Doctor::where('code' , $request->userCode)->first();
            if($doctor){
                $consultation = Consultation::where('channel' , $request->channel)->where('doctorId' , $doctor->id)->first();
            }
        }
        if($consultation){
            return view('front.call' , compact('pageName' , 'user' , 'request'));
        }else{
            abort(404);
        }
    }
}
