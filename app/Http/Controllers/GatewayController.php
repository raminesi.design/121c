<?php

namespace App\Http\Controllers;

use App\Models\Consultation;
use App\Models\Doctor;
use App\Models\Orders;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Stripe\Stripe;
use App\Traits\PusherTrait;
use App\Traits\emailNotificationTrait;
use Carbon\Carbon;
use Stripe\Charge;

class GatewayController extends Controller
{
    public function transaction(Request $request)
    {
        $request->validate([
            'num' => 'required'
        ]);
        $order = Orders::where('orderNumber' , $request->num)->where('status' , 'unpaid')->whereDate('created_at', Carbon::today())->first();
        if($order){

            $this->paid($order->id);
            $wallet = Wallet::where('userId' , $order->userId)->first();
            $wallet->credit = $wallet->credit - $order->wallet_amount;
            $wallet->save();
            return redirect(route('transaction_successful').'?id='.$order->id);

            if($order->payable_amount == 0){
                $this->paid($order->id);
                $wallet = Wallet::where('userId' , $order->userId)->first();
                $wallet->credit = $wallet->credit - $order->wallet_amount;
                $wallet->save();
                return redirect(route('transaction_successful').'?id='.$order->id);
            }else{
                return view('stripe' , compact('order'));
            }
        }else{
            abort(404);
        }
    }
    public function cancel(Request $request)
    {
        $request->validate([
            'num' => 'required'
        ]);
        $order = Orders::where('orderNumber' , $request->num)->where('status' , 'unpaid')->whereDate('created_at', Carbon::today())->first();
        if($order){
            $order->status = 'cancelled';
            $order->save();
        }
        return redirect(route('index'));
    }
    public function stripePost(Request $request)
    {
        $order = Orders::where('orderNumber' , $request->num)->where('status' , 'unpaid')->whereDate('created_at', Carbon::today())->first();
        Stripe::setApiKey(env('STRIPE_SECRET'));
        Charge::create ([
                "amount" => $order->payable_amount,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from itsolutionstuff.com."
        ]);
        Session::flash('success', 'Payment successful!');
        return back();
    }

    function paid($id)
    {
        $order = Orders::find($id);
        $consultation = Consultation::find($order->reqId);
        $order->status = 'successful';
        $consultation->status = 'pending';
        $order->save();
        $consultation->save();
        $doctor = Doctor::find($consultation->doctorId);
        $userDoctor = User::find($doctor->userId);
        $title = 'New request';
        $message = 'You have a new request';
        $url = route('doctorProfile_consultation_info').'?id='.$consultation->id;
        $data = array(
            'type' => 'notifications',
            'url' => $url
        );
        PusherTrait::send(md5($userDoctor->email.$userDoctor->id) , $title , $message , $data);
        emailNotificationTrait::send($userDoctor->email , $userDoctor->name ,  $title , $message , $url);
    }

    public function successful(Request $request)
    {
        $order = Orders::where('id' , $request->id)->first();
        return view('gateway.success_payment' , compact('order' , 'request'));
    }
}
