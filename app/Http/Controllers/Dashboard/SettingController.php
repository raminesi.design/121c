<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Settings;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    public function information(Request $request)
    {
        $setting = Settings::first();
        return view('admin.setting.information' , compact('setting'));
    }
    public function infoSave(Request $request)
    {
        $setting = Settings::first();
        if(!$setting){
            $setting = new Settings();
        }
        if(!empty($request->name)){
            $setting->name = $request->name;
        }
        if(!empty($request->keywords)){
            $setting->keywords = $request->keywords;
        }
        if(!empty($request->description)){
            $setting->description = $request->description;
        }
        if(!empty($request->twitter)){
            $setting->twitter = $request->twitter;
        }
        if(!empty($request->facebook)){
            $setting->facebook = $request->facebook;
        }
        if(!empty($request->linkedin)){
            $setting->linkedin = $request->linkedin;
        }
        if(!empty($request->phone)){
            $setting->phone = $request->phone;
        }
        if(!empty($request->email)){
            $setting->email = $request->email;
        }
        if(!empty($request->address)){
            $setting->address = $request->address;
        }
        if(!empty($request->skype)){
            $setting->skype = $request->skype;
        }
        if(!empty($request->instagram)){
            $setting->instagram = $request->instagram;
        }
        if(!empty($request->about)){
            $setting->about = $request->about;
        }
        $setting->save();
        return back();
    }
    public function password(Request $request)
    {
        return view('admin.setting.password' , compact('request'));
    }
    public function changePassword(Request $request)
    {
        $user   = Auth::user();
        $request->validate([
            'password' => 'required|string|min:8',
        ],
        [
            'password.required' => 'Please enter password',
            'password.min' => 'The minimum number of characters must be 8',
        ]);
        $user = User::find($user->id);
        $user->password =  bcrypt($request->password);
        $user->save();
        $user->password =  $request->password;
        return back()->withErrors(['msg' => 'Password changed successfully: Email: '.$user->email.' - password: '.$user->password]);
    }
}
