<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function createRole(Request $request)
    {
        Role::create(['name' => $request->role]);
    }
    public function createPermission(Request $request)
    {
        Permission::create(['name' => $request->permission]);
    }
    public function permissionToRole(Request $request)
    {
        $role = Role::find($request->roleId);
        $role->givePermissionTo($request->permissionId);
    }
    public function RoleToUser(Request $request)
    {
        $role = Role::find($request->roleId);
        $user = User::find($request->userId);
        $user->assignRole($role->name);
    }
}
