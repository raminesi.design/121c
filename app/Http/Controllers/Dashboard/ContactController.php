<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\ContactModel;
use App\Models\Notifications;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\PusherTrait;

class ContactController extends Controller
{
    public function messages(Request $request)
    {
        $messages = ContactModel::orderBy('id' , 'desc')->paginate(15);
        return view('admin.contact.index' , compact('messages'));
    }
    public function notifications(Request $request)
    {
        $notifications = Notifications::with(['user'])->orderBy('id' , 'desc')
        ->paginate(15);
        return view('admin.contact.notifications' , compact('notifications'));
    }
    public function sesndNotif(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'message' => 'required'
        ]);
        $receiver = null;
        if(is_null($request->user)){
            $notification = Notifications::create([
                'group' => $request->group,
                'title' => $request->title,
                'description' => $request->message
            ]);
            $receiver = 'All_'.$request->group;
        }else{
            $notification = Notifications::create([
                'userId' => $request->uaserId,
                'title' => $request->title,
                'description' => $request->message
            ]);
            $user = User::find(intval($request->uaserId));
            $receiver = $user->email.$user->id;
        }
        $data = array(
            'type' => 'notifications',
            'url' => null
        );
        PusherTrait::send(md5($receiver) , $notification->title , $notification->description , $data);
        return back();
    }
}
