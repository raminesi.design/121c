<?php

namespace App\Http\Controllers\Dashboard;

use App\Exports\DoctorExport;
use App\Http\Controllers\Controller;
use App\Models\Consultation;
use App\Models\Days;
use App\Models\Doctor;
use App\Models\DoctorExpertise;
use App\Models\Expertise;
use App\Models\Schedule;
use App\Models\Times;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class DoctorController extends Controller
{
    public function doctors(Request $request)
    {
        $doctors = Doctor::join('users' , 'users.id' , '=' , 'doctor.userId')
            ->select('doctor.id' , 'users.name' , 'users.mobile' , 'users.email' , 'doctor.avatar' , 'doctor.status' , 'doctor.countService' , 'doctor.countSuccessful' , 'doctor.countFailed' , 'doctor.rate' , 'doctor.active');
            if(!empty($request->search)){
                $doctors->where('users.name', 'like', '%' . $request->search . '%');
            }
            $doctors = $doctors->paginate(15);
        return view('admin.doctor.doctors' , compact('doctors' , 'request'));
    }
    public function doctor(Request $request){
        $id = $request->id;
        $doctor         = Doctor::find($id);
        $user           = User::find($doctor->userId);
        $allExpertise   = Expertise::get();
        $expertise      = DoctorExpertise::join('expertise' , 'expertise.id' , '=' , 'doctor_expertise.expertiseId')->where('doctor_expertise.doctorId' ,$id)->select('expertise.id' , 'expertise.title' , 'doctor_expertise.price_video' , 'doctor_expertise.price_voice' , 'doctor_expertise.price_text'  , 'doctor_expertise.status')->get();
        $schedule       = Schedule::where('doctorId' , $id)->get();
        $times          = Times::get();
        $allRequest     = Consultation::where('doctorId' , $id)->count();
        $doneRequest    = Consultation::where('doctorId' , $id)->where('status' , 'done')->count();
        $rejectRequest  = Consultation::where('doctorId' , $id)->where('status' , 'rejected')->count();
        $cancelRequest  = Consultation::where('doctorId' , $id)->where('status' , 'canceled')->count();
        $pendingRequest = Consultation::where('doctorId' , $id)->where('status' , 'pending')->count();
        $arrayExpertise  = array();
        $arrayExpertisePrice  = array();
        foreach($expertise as $exp){
            if($exp->status == 'enable'){
                array_push($arrayExpertise , $exp->id);
            }
            $arrayExpertisePrice[$exp->id] = $exp->price;
        }
        $arraySchedule  = array();
        foreach($schedule as $val){
            array_push($arraySchedule , $val->dayId.'-'.$val->timeId);
        }
        return view('admin.doctor.doctor' , compact('doctor' , 'user' , 'expertise' , 'allExpertise' , 'arrayExpertise' , 'times' , 'arraySchedule' , 'allRequest' , 'doneRequest' , 'rejectRequest' , 'cancelRequest' , 'pendingRequest' , 'arrayExpertisePrice'));
    }
    public function doctorAdd(){
        return view('admin.doctor.add');
    }
    public function doctorSave(Request $request){

        $request->validate([
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'mobile' => 'required|size:11|unique:users',
            'email' => 'required|email|unique:users',
            'image' => 'required'
        ],
        [
            'email.required' => 'Please enter email',
            'email.email' => 'Invalid email entered',
            'email.unique' => 'This email is available in the system',
            'mobile.required' => 'Please enter mobile',
            'mobile.size' => 'Invalid mobile entered',
            'mobile.unique' => 'This mobile is available in the system',
        ]);
        $mobile = $this->convert2english($request->mobile);

        $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $code = 'P'.random_int(111111, 999999).substr(str_shuffle(str_repeat($pool, 5)), 0, 1);
        $code = $this->checkCode($code);

        $image = $request->image;  // your base64 encoded
        $image = str_replace('data:image/jpeg;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
        \File::put( public_path(). '/media/doctor/' . $imageName, base64_decode($image));

        $user = User::create([
            'first_name' => $request->firstname,
            'last_name' => $request->lastname,
            'name' => $request->firstname.' '.$request->lastname,
            'mobile' => $mobile,
            'email' => $request->email,
            'birthdate' => $request->birthdate
        ]);

        Wallet::create([
            'userId' => $user->id,
            'credit' => 0,
        ]);

        $doctor = Doctor::create([
            'userId' => $user->id,
            'avatar' => $imageName,
            'description' => $request->description,
            'code' => $code
        ]);

        return view('admin.doctor.infoAdd' , compact('user' , 'doctor'));
    }
    function checkCode($code)
    {
        $doctor = Doctor::where('code' , $code)->first();
        if($doctor){
            $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = 'P'.random_int(111111, 999999).substr(str_shuffle(str_repeat($pool, 5)), 0, 1);
            $this->checkCode($code);
        }else{
            return $code;
        }
    }
    public function editInfo(Request $request , $id){
        $doctor   = Doctor::find($id);
        $request->validate([
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'mobile' => 'required|size:11|unique:users,mobile,'.$doctor->userId,
            'email' => 'required|email|unique:users,email,'.$doctor->userId
        ],
        [
            'email.required' => 'Please enter email',
            'email.email' => 'Invalid email entered',
            'email.unique' => 'This email is available in the system',
            'mobile.required' => 'Please enter mobile',
            'mobile.size' => 'Invalid mobile entered',
            'mobile.unique' => 'This mobile is available in the system',
        ]);
        $mobile = $this->convert2english($request->mobile);
        if(!empty($request->image)){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/doctor/' . $imageName, base64_decode($image));
            $doctor->avatar = $imageName;
        }
        $doctor->description = $request->description;
        $doctor->save();

        $user     = User::find($doctor->userId);
        $user->first_name = $request->firstname;
        $user->last_name = $request->lastname;
        $user->gender = $request->gender;
        $user->birthdate = $request->birthdate;
        $user->mobile = $mobile;
        $user->email = $request->email;
        $user->name = $request->firstname.' '.$request->lastname;
        $user->save();

        return back();
    }
    public function status(Request $request){
        $doctor = Doctor::find($request->id);
        if($doctor){
            if($doctor->status == 'enable'){
                $doctor->status = 'disable';
            }else{
                $doctor->status = 'enable';
            }
            $doctor->save();
        }
        return back();
    }
    public function doctorTime(Request $request){
        $schedule = Schedule::where('doctorId' , $request->doctor_id )->where('dayId' , $request->day_id )->where('timeId' , $request->time_id )->first();
        if($schedule){
            $schedule->delete();
        }else{
            $schedule = Schedule::create([
                'doctorId' => $request->doctor_id,
                'dayId' => $request->day_id,
                'timeId' => $request->time_id
                ]);
        }
        return $schedule;
    }
    public function doctorAllTime(Request $request){
        $schedule = Schedule::where('doctorId' , $request->doctor_id )->where('dayId' , $request->day_id )->delete();
        $data = array();
        if($request->status == 0){
            $times = Times::get();
            foreach($times as $time){
                $schedule = array(
                    'doctorId' => $request->doctor_id,
                    'dayId' => $request->day_id,
                    'timeId' => $time->id,
                );
                array_push($data , $schedule);
            }
            Schedule::insert($data);
        }
        return $data;
    }
    public function doctorAllDay(Request $request){
        $schedule = Schedule::where('doctorId' , $request->doctor_id )->where('timeId' , $request->time_id )->delete();
        $data = array();
        if($request->status == 0){
            $days = Days::get();
            foreach($days as $day){
                $schedule = array(
                    'doctorId' => $request->doctor_id,
                    'dayId' => $day->id,
                    'timeId' => $request->time_id,
                );
                array_push($data , $schedule);
            }
            Schedule::insert($data);
        }
        return $data;
    }
    public function doctorExpertise(Request $request){
        DoctorExpertise::where('doctorId' , $request->doctor_id )->update(['status' => 'disable']);
        $expertise = DoctorExpertise::where('doctorId' , $request->doctor_id )->where('expertiseId' , $request->expertise_id )->first();
        if($expertise){
            if($expertise->status == 'enable'){
                $expertise->status = 'disable';
            }else{
                $expertise->status = 'enable';
            }
            $expertise->save();
        }else{
            $exp = Expertise::find($request->expertise_id);
            if($exp){
                $expertise = DoctorExpertise::create([
                    'doctorId' => $request->doctor_id,
                    'expertiseId' => $request->expertise_id,
                    'price_video' => $exp->price_video_min,
                    'price_voice' => $exp->price_voice_min,
                    'price_text' => $exp->price_text_min,
                    'price' => 0
                    ]);
            }
        }
        return $expertise;
    }
    public function changePassword(Request $request , $id)
    {
        $request->validate([
            'password' => 'required|string|min:8',
        ],
        [
            'password.required' => 'Please enter password',
            'password.min' => 'The minimum number of characters must be 8',
        ]);
        $doctor = Doctor::find($id);
        $user = User::find($doctor->userId);
        $user->password =  bcrypt($request->password);
        $user->save();
        $user->password =  $request->password;
        return view('admin.doctor.infoAdd' , compact('user' , 'doctor'));
    }
    public function accountNumber(Request $request , $id)
    {
        $request->validate([
            'account_number' => 'required',
        ],
        [
            'account_number.required' => 'Please enter account number',
        ]);
        $doctor = Doctor::find($id);
        $doctor->account_number = $request->account_number;
        $doctor->save();
        return back();
    }
    function convert2english($string) {
        $newNumbers = range(0, 9);
        // 1. Persian HTML decimal
        $persianDecimal = array('&#1776;', '&#1777;', '&#1778;', '&#1779;', '&#1780;', '&#1781;', '&#1782;', '&#1783;', '&#1784;', '&#1785;');
        // 2. Arabic HTML decimal
        $arabicDecimal = array('&#1632;', '&#1633;', '&#1634;', '&#1635;', '&#1636;', '&#1637;', '&#1638;', '&#1639;', '&#1640;', '&#1641;');
        // 3. Arabic Numeric
        $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        // 4. Persian Numeric
        $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');

        $string =  str_replace($persianDecimal, $newNumbers, $string);
        $string =  str_replace($arabicDecimal, $newNumbers, $string);
        $string =  str_replace($arabic, $newNumbers, $string);
        return str_replace($persian, $newNumbers, $string);
    }
    public function export(Request $request)
    {
        $file = 'doctors'.date('-Y_m_d-H_i_s').'.xlsx';
        return Excel::download(new DoctorExport($request), $file);
    }
}
