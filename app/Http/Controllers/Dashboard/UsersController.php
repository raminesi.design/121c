<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Consultation;
use App\Models\Country;
use App\Models\Doctor;
use App\Models\ParentModel;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Exports\UserExport;
use Maatwebsite\Excel\Facades\Excel;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $users = User::join('wallet' , 'wallet.userId' , '=' , 'users.id')->whereNotNull('code');
        if(!empty($request->search)){
            $users->where('users.name', 'like', '%' . $request->search . '%');
        }
        $users = $users->select('users.*')
        ->paginate(15);
        return view('admin.user.index' , compact('users' , 'request'));
    }
    public function userAdd(){
        $country = Country::orderBy('sort' , 'asc')->get();
        return view('admin.user.add' , compact('country'));
    }
    public function saveUser(Request $request)
    {
        $date = Carbon::now()->addYears(-16)->format('Y-m-d');
        $request->validate([
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'birthdate' => 'required|date|before:'.$date,
            'email' => 'required|email|unique:users'
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'You are not allowed to register in the system. Please apply through your parents.',
            'email.required' => 'Please enter email',
            'email.email' => 'Invalid email entered',
            'email.unique' => 'This email is available in the system'
        ]);
        $country = Country::find($request->country);
        $l1 = strtoupper(substr($request->first_name,0 , 1));
        $l2 = strtoupper(substr($request->last_name,0 , 1));
        $code = random_int(11111111, 99999999).$country->symbol.$l1.$l2;
        $code = $this->checkCode($code , $country->symbol.$l1.$l2);

        $imageName = 'avatar.png';
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'name' => $request->first_name.' '.$request->last_name,
            'email' => $request->email,
            'birthdate' => $request->birthdate,
            'avatar' => $imageName,
            'country' => $request->country,
            'code' => $code
        ]);
        Wallet::create([
            'userId' => $user->id
        ]);
        return redirect(route('dashboard_user').'?id='.$user->id);
    }
    function checkCode($code , $string)
    {
        $user = User::where('code' , $code)->first();
        if($user){
            $code = random_int(11111111, 99999999).$string;
            $this->checkCode($code , $string);
        }else{
            return $code;
        }
    }
    public function updateUser(Request $request , $id)
    {
        $date = Carbon::now()->addYears(-16)->format('Y-m-d');
        $request->validate([
            'first_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'last_name' => 'required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'birthdate' => 'required|date|before:'.$date,
            'email' => 'required|email|unique:users,email,'.$id
        ],
        [
            'first_name.required' => 'Please enter first name',
            'first_name.regex' => 'Please use English letters',
            'last_name.required' => 'Please enter last name',
            'last_name.regex' => 'Please use English letters',
            'birthdate.required' => 'Please enter birthdate',
            'birthdate.before' => 'You are not allowed to register in the system. Please apply through your parents.',
            'email.required' => 'Please enter email',
            'email.email' => 'Invalid email entered',
            'email.unique' => 'This email is available in the system'
        ]);
        $user = User::find($id);
        if(!empty($request->image)){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.random_int(00000000000, 9999999999).'.'.'jpeg';
            \File::put( public_path(). '/media/user/' . $imageName, base64_decode($image));
            $user->avatar = $imageName;
        }
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->gender = $request->gender;
        $user->birthdate = $request->birthdate;
        $user->email = $request->email;
        $user->name = $request->first_name.' '.$request->last_name;
        $user->save();
        return back();
    }
    public function user(Request $request){
        $id = $request->id;
        $user = User::find($id);
        $wallet = Wallet::where('userId' , $user->id)->first();
        $allRequest     = Consultation::where('userId' , $id)->count();
        $doneRequest    = Consultation::where('userId' , $id)->where('status' , 'done')->count();
        $rejectRequest  = Consultation::where('userId' , $id)->where('status' , 'reject')->count();
        $cancelRequest  = Consultation::where('userId' , $id)->where('status' , 'cancel')->count();
        $pendingRequest = Consultation::where('userId' , $id)->where('status' , 'pending')->count();
        $children = User::join('parent' , 'parent.userId' , '=' , 'users.id')
                ->where('parent.parentId' , $user->id)
                ->where('parent.status' , '!=' , 'delete')
                ->select('users.*' , 'parent.id as parent')
                ->get();
        return view('admin.user.user' , compact('user' , 'wallet' , 'children' , 'allRequest' , 'doneRequest' , 'rejectRequest' , 'cancelRequest' , 'pendingRequest'));
    }
    public function status(Request $request){
        $user = User::find($request->id);
        if($user){
            if($user->status == 'enable'){
                $user->status = 'disable';
            }else{
                $user->status = 'enable';
            }
            $user->save();
        }
        return back();
    }
    public function changePassword(Request $request , $id)
    {
        $request->validate([
            'password' => 'required|string|min:8',
        ],
        [
            'password.required' => 'Please enter password',
            'password.min' => 'The minimum number of characters must be 8',
        ]);
        $user = User::find($id);
        $user->password =  bcrypt($request->password);
        $user->save();
        $user->password =  $request->password;
        return view('admin.user.infoAdd' , compact('user'));
    }
    public function delete(Request $request){
        $user = ParentModel::find($request->id);
        $user->delete();
        return back();
    }
    public function deleteUser(Request $request){
        $user = User::find($request->id);
        if($user){
            $user->status = 'delete';
            $user->save();
        }
        return back();
    }

    public function searchAjax(Request $request)
    {
        $users = User::where('status' , 'enable')->where('name' , 'like' , '%' . $request->search . '%')->get();
        $result = [];
        foreach ($users as $user) {
            if (empty($user->id))
                continue;
            $temp = ["value" => $user->id, 'label' => $user->name.' : '.$user->email ];
            array_push($result, $temp);
        }
        return response()->json($result);
    }
    public function export(Request $request)
    {
        $file = 'users'.date('-Y_m_d-H_i_s').'.xlsx';
        return Excel::download(new UserExport($request), $file);
    }
}
