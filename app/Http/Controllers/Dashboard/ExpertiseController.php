<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Expertise;
use Illuminate\Http\Request;

class ExpertiseController extends Controller
{
    public function expertise(Request $request)
    {
        $expertise = Expertise::orderby('id' , 'desc');
        if(!empty($request->search)){
            $expertise->where('title', 'like', '%' . $request->search . '%');
        }
        $expertise = $expertise->paginate(15);
        return view('admin.expertise.expertise' , compact('expertise' , 'request'));
    }
    public function add()
    {
        return view('admin.expertise.add');
    }
    public function save(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'required',
            'percent' => 'required',
        ]);
        $price_video = explode(',' , $request->price_video);
        $price_voice = explode(',' , $request->price_voice);
        $price_text  = explode(',' , $request->price_text);

        $uploadedFile = $request->file('image');
        $ext = $uploadedFile->getClientOriginalExtension();
        $file = time().'_'.random_int(00000000000, 9999999999).'.'.$ext;
        $uploadedFile->move(public_path() . '/media/expertise/', $file);
        Expertise::create([
            'title' => $request->title,
            'price_video_min' => $price_video[0],
            'price_video_max' => $price_video[1],
            'price_voice_min' => $price_voice[0],
            'price_voice_max' => $price_voice[1],
            'price_text_min' => $price_text[0],
            'price_text_max' => $price_text[1],
            'percent' => $request->percent,
            'description' => $request->description,
            'image' => $file
            ]);
        return redirect(route('dashboard_expertise'));
    }
    public function edit(Request $request)
    {
        $expertise = Expertise::find($request->id);
        return view('admin.expertise.edit' , compact('expertise'));
    }
    public function update(Request $request , $id)
    {
        $request->validate([
            'title' => 'required',
            'percent' => 'required',
        ]);
        $expertise = Expertise::find($id);
        $price_video = explode(',' , $request->price_video);
        $price_voice = explode(',' , $request->price_voice);
        $price_text  = explode(',' , $request->price_text);
        if(!empty($request->image)){
            $uploadedFile = $request->file('image');
            $ext = $uploadedFile->getClientOriginalExtension();
            $file = time().'_'.random_int(00000000000, 9999999999).'.'.$ext;
            $uploadedFile->move(public_path() . '/media/expertise/', $file);
            $expertise->image = $file;
        }
        $expertise->title = $request->title;
        $expertise->price_video_min = $price_video[0];
        $expertise->price_video_max = $price_video[1];
        $expertise->price_voice_min = $price_voice[0];
        $expertise->price_voice_max = $price_voice[1];
        $expertise->price_text_min = $price_text[0];
        $expertise->price_text_max = $price_text[1];
        $expertise->percent = $request->percent;
        $expertise->description = $request->description;
        $expertise->save();
        return redirect(route('dashboard_expertise'));
    }
    public function status(Request $request){
        $expertise = Expertise::find($request->id);
        if($expertise){
            if($expertise->status == 'enable'){
                $expertise->status = 'disable';
            }else{
                $expertise->status = 'enable';
            }
            $expertise->save();
        }
        return back();
    }
    public function sort(Request $request){
        $expertise = Expertise::find($request->id);
        $expertise->sort = $request->sort;
        $expertise->save();
        return $expertise;
    }
}
