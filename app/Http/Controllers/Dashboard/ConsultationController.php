<?php

namespace App\Http\Controllers\Dashboard;

use App\Exports\ConsultationExport;
use App\Http\Controllers\Controller;
use App\Models\Consultation;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ConsultationController extends Controller
{
    public function index(Request $request){

            $query = Consultation::with(['day' , 'user' , 'expertise'])->where('status' , '!=' , 'created')->orderBy('date' , 'desc');
            if(!empty($request->status)){
                $query->where('status' , $request->status);
            }
            if(isset($request->doctor)){
                $query->where('doctorId' , $request->doctor);
            }
            if(isset($request->start)){
                $query->whereDate('created_at' , '>=' , $request->start);
            }
            if(isset($request->end)){
                $query->whereDate('created_at' , '<=' , $request->end);
            }
            if(isset($request->user)){
                $query->where('userId' , $request->user);
            }
            $consultation = $query->paginate(15);

        return view('admin.consultation.index' , compact('consultation' , 'request'));
    }
    public function consultation(Request $request){
        $consultation = Consultation::with(['schedule' => function($query){
            $query->with(['day' , 'time']);
        } , 'doctor' => function($query){
            $query->with(['user']);
        } , 'order' , 'user' , 'expertise'])->where('id' , $request->id)->first();
        return view('admin.consultation.consultation' , compact('consultation'));
    }
    public function export(Request $request)
    {
        $file = 'consultations'.date('-Y_m_d-H_i_s').'.xlsx';
        return Excel::download(new ConsultationExport($request), $file);
    }
}
