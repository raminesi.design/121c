<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Consultation;
use App\Models\Doctor;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $data['allUsers']   = User::whereNotNull('code')->count();
        $data['todayUsers'] = User::whereNotNull('code')->whereDate('created_at', Carbon::today())->count();
        $data['allDoctors'] = Doctor::count();
        $data['allConsultation'] = Consultation::where('status' , '!=' , 'created')->count();
        $data['consultation'] = Consultation::whereIn('status' , ['pending' , 'accepted'])->with(['user' , 'doctor' => function($query){
            $query->with(['user']);
        } , 'day' ])->orderBy('date' , 'asc')->limit(10)->get();
        return view('admin.home' , compact('request' , 'data'));
    }
}
