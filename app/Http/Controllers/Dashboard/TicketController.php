<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Notifications;
use App\Models\Settings;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\PusherTrait;
use App\Traits\emailNotificationTrait;
use Carbon\Carbon;

class TicketController extends Controller
{
    public function tickets(Request $request)
    {
        $setting = Settings::first();
        $tickets = Ticket::orderBy('created_at' , 'desc')->whereNull('parentId')->paginate(15);
        return view('admin.ticket.tickets' , compact('setting' , 'tickets'));
    }
    public function ticket(Request $request)
    {
        $setting = Settings::first();
        $ticket  = Ticket::where('id' , $request->id)->with(['tickets' => function($query){
            $query->orderBy('created_at' , 'desc');
        } , 'user'])->first();
        return view('admin.ticket.ticket' , compact('setting' , 'ticket'));
    }
    public function saveComment(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'text' => 'required',
            'file' => 'mimes:png,jpeg,jpg|max:5000'
        ]);
        $user       = Auth::user();
        $ticket     = Ticket::where('id' , $request->id)->where('status' , 'open')->first();
        if(!$ticket){
            abort(404);
        }
        Ticket::where('id' , $request->id)->update(['updated_at' => Carbon::now()]);
        $userTicket = User::find($ticket->userId);
        $file = null;
        if(!empty($request->file)){
            $uploadedFile = $request->file('file');
            $ext = $uploadedFile->getClientOriginalExtension();
            $file = time().'_'.random_int(00000000000, 9999999999).'.'.$ext;
            $uploadedFile->move(public_path() . '/media/ticket/', $file);
        }
        Ticket::create([
            'userId' => $user->id,
            'parentId' => $ticket->id,
            'text' => $request->text,
            'file' => $file
        ]);
        $title   = 'Ticket';
        $message = 'Ticket answer';
        $url = route('profile_ticket').'?id='.$ticket->id;
        $data = array(
            'type' => 'notifications',
            'url' => $url
        );
        Notifications::create([
            'userId' => $userTicket->id,
            'title' => $title,
            'description' => $message
        ]);
        PusherTrait::send(md5($userTicket->email.$userTicket->id) , $title , $message , $data);
        emailNotificationTrait::send($userTicket->email , $userTicket->name ,  $title , $message , $url);
        return back();
    }
    public function close(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $ticket     = Ticket::where('id' , $request->id)->where('status' , 'open')->first();
        $ticket->status = 'close';
        $ticket->save();

        $userTicket = User::find($ticket->userId);
        $title   = 'Ticket';
        $message = 'The ticket was closed';
        $url = route('profile_ticket').'?id='.$ticket->id;
        $data = array(
            'type' => 'notifications',
            'url' => $url
        );
        Notifications::create([
            'userId' => $userTicket->id,
            'title' => $title,
            'description' => $message
        ]);
        PusherTrait::send(md5($userTicket->email.$userTicket->id) , $title , $message , $data);
        emailNotificationTrait::send($userTicket->email , $userTicket->name ,  $title , $message , $url);

        return back();
    }
}
