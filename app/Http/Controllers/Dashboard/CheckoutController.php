<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Checkout;
use App\Models\Notifications;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\PusherTrait;
use App\Traits\emailNotificationTrait;
use Carbon\Carbon;

class CheckoutController extends Controller
{
    public function index(Request $request)
    {
        $checkouts = Checkout::join('users' , 'users.id' , '=' , 'checkout.userId')
                    ->orderBy('checkout.id' , 'desc')
                    ->select('checkout.*' , 'users.name')
                    ->paginate(15);
        return view('admin.checkout.index' , compact('checkouts' , 'request'));
    }
    public function checkout(Request $request)
    {
        $checkout = Checkout::where('id' , $request->id)->with(['user' => function($query){
            $query->with(['doctor']);
        } , 'bank'])->first();
        if($checkout->status == 'pending'){
            $checkout->status = 'processing';
            $checkout->save();
        }
        return view('admin.checkout.checkout' , compact('checkout' , 'request'));
    }
    public function status(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $checkout = Checkout::find($request->id);
        if(!empty($request->file)){
            $uploadedFile = $request->file('file');
            $ext = $uploadedFile->getClientOriginalExtension();
            $file = time().'_'.random_int(00000000000, 9999999999).'.'.$ext;
            $uploadedFile->move(public_path() . '/media/checkout/', $file);
            $checkout->file = $file;
        }
        $checkout->status = $request->status;
        $checkout->date = Carbon::now();
        $checkout->description = $request->description;
        $checkout->save();

        $user = User::find($checkout->userId);
        $title = 'Checkout status';
        if($request->status == 'paid'){
            $message = 'Your request has been processed and the amount has been credited to your account';
        }else{
            $message = 'Your settlement request has been rejected. Please refer to your panel';
        }
        $url = route('doctorProfile_checkout');
        $data = array(
            'type' => 'notifications',
            'url' => $url
        );
        Notifications::create([
            'userId' => $user->id,
            'title' => $title,
            'description' => $message,
        ]);
        PusherTrait::send(md5($user->email.$user->id) , $title , $message , $data);
        emailNotificationTrait::send($user->email , $user->name ,  $title , $message , $url);
        return back();
    }
}
