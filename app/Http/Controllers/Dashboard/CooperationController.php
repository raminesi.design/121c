<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Cooperation;
use App\Models\Doctor;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CooperationController extends Controller
{
    public function index(Request $request)
    {
        $cooperation = Cooperation::join('users' , 'users.id' , '=' , 'cooperation.userId')
            ->select('users.*' , 'cooperation.status' , 'cooperation.id as cId')
            ->orderBy('cooperation.id' , 'desc')
            ->paginate(15);
        return view('admin.cooperation.index' , compact('cooperation' , 'request'));
    }
    public function cooperation(Request $request)
    {
        $cooperation = Cooperation::join('users' , 'users.id' , '=' , 'cooperation.userId')
            ->select('users.*' , 'cooperation.expertise' , 'cooperation.description' , 'cooperation.ip' , 'cooperation.date' , 'cooperation.status' , 'cooperation.id as cId')
            ->where('cooperation.id' , $request->id)
            ->first();
        $cooperation->age = Carbon::parse($cooperation->birthdate)->diff(Carbon::now())->format('%y');
        return view('admin.cooperation.cooperation' , compact('cooperation' , 'request'));
    }
    public function cooperation_update(Request $request)
    {
        $request->validate([
            'comment' => 'required'
        ]);
        $cooperation = Cooperation::find($request->id);
        $cooperation->comment = $request->comment;
        $cooperation->status = $request->status;
        $cooperation->save();
        if($cooperation->status == 'accepted'){
            $user = User::find($cooperation->userId);
            Wallet::create([
                'userId' => $user->id
            ]);
            $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = 'P'.random_int(111111, 999999).substr(str_shuffle(str_repeat($pool, 5)), 0, 1);
            $code = $this->checkCode($code);
            $doctor = Doctor::create([
                'userId' => $user->id,
                'avatar' => $cooperation->avatar,
                'description' => $cooperation->description,
                'code' => $code
            ]);
            return view('admin.doctor.infoAdd' , compact('user' , 'doctor'));
        }
        return redirect(route('dashboard_cooperation'));
    }
    function checkCode($code)
    {
        $doctor = Doctor::where('code' , $code)->first();
        if($doctor){
            $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = 'P'.random_int(111111, 999999).substr(str_shuffle(str_repeat($pool, 5)), 0, 1);
            $this->checkCode($code);
        }else{
            return $code;
        }
    }
}
