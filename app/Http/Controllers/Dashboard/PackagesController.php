<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Packages;
use Illuminate\Http\Request;

class PackagesController extends Controller
{
    public function packages(Request $request)
    {
        $packages = Packages::get();
        return view('admin.packages.packages' , compact('packages' , 'request'));
    }
}
