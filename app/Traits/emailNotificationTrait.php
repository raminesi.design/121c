<?php

namespace App\Traits;

use App\Mail\emailNotification;
use Illuminate\Support\Facades\Mail;

trait emailNotificationTrait
{
    static function send($email , $name , $title , $message , $url = null){
        return true;
        $data = array(
            "name" => $name,
            "title" => $title,
            "message" => $message,
            "url" => $url,
        );
        Mail::to($email)->send(new emailNotification($data));
        try {
            Mail::to($email)->send(new emailNotification($data));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
