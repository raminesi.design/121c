<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;

    protected $table = 'doctor';
    protected $fillable = ['userId' , 'avatar' , 'account_number' , 'code' , 'description' , 'countService' , 'countSuccessful' , 'countFailed' , 'active' , 'rate' , 'status'];

    public function user()
    {
        return $this->belongsTo('App\Models\User' , 'userId');
    }

    public function expertises(){
        return $this->hasMany('App\Models\DoctorExpertise' , 'doctorId');
    }

    public function packeges(){
        return $this->hasMany('App\Models\DoctorPackage' , 'doctor_id');
    }

    public function consultations(){
        return $this->hasMany('App\Models\Consultation' , 'doctorId');
    }

    public function schedule(){
        return $this->hasMany('App\Models\Schedule' , 'doctorId');
    }
}
