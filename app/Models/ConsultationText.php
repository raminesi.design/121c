<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConsultationText extends Model
{
    use HasFactory;
    protected $table = 'consultation_text';
    protected $fillable = ['consultationId' , 'userId' , 'message' , 'status'];
}
