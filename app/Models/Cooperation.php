<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cooperation extends Model
{
    use HasFactory;
    protected $table = 'cooperation';
    protected $fillable = ['userId' , 'avatar' , 'expertise' , 'description' , 'comment' , 'ip' , 'date' , 'status'];

    public function user()
    {
        return $this->belongsTo('App\Models\User' , 'userId');
    }
}
