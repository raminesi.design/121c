<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;

    protected $table = 'schedule';
    protected $fillable = ['doctorId' , 'dayId' , 'timeId' ];

    public function consultations(){
        return $this->hasMany('App\Models\Consultation' , 'scheduleId');
    }

    public function day()
    {
        return $this->belongsTo('App\Models\Days' , 'dayId');
    }

    public function time()
    {
        return $this->belongsTo('App\Models\Times' , 'timeId');
    }
}
