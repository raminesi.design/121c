<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banks extends Model
{
    use HasFactory;
    protected $table = 'banks';
    protected $fillable = ['userId' , 'name' , 'code' , 'status'];

    public function user()
    {
        return $this->belongsTo('App\Models\User' , 'userId');
    }
}
