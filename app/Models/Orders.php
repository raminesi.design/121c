<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;
    protected $table = 'orders';
    protected $fillable = ['userId' , 'orderNumber' , 'reqId' , 'consultationId' , 'total_amount' , 'discount_amount' , 'wallet_amount' , 'payable_amount' , 'transactionCode' , 'information' , 'payable_doctor' , 'status'];

    public function user()
    {
        return $this->belongsTo('App\Models\User' , 'userId');
    }

    public function consultation()
    {
        return $this->belongsTo('App\Models\Consultation' , 'reqId');
    }
}
