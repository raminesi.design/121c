<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    protected $table = 'ticket';
    protected $fillable = ['userId' , 'parentId' , 'subject' , 'text' , 'status' , 'file'];

    public function tickets(){
        return $this->hasMany('App\Models\Ticket' , 'parentId');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User' , 'userId');
    }
}
