<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    use HasFactory;
    protected $table = 'prescription';
    protected $fillable = ['consultationId' , 'description'];

    public function details(){
        return $this->hasMany('App\Models\PrescriptionDetails' , 'prescriptionId');
    }
}
