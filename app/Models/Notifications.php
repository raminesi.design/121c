<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    use HasFactory;
    protected $table = 'notifications';
    protected $fillable = ['userId' , 'title' , 'description', 'consultationId' , 'group'];

    public function user()
    {
        return $this->belongsTo('App\Models\User' , 'userId');
    }

    public function consultation()
    {
        return $this->belongsTo('App\Models\Consultation' , 'consultationId');
    }
}
