<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParentModel extends Model
{
    use HasFactory;
    protected $table = 'parent';
    protected $fillable = ['parentId' , 'userId' , 'status'];

    public function user()
    {
        return $this->belongsTo('App\Models\User' , 'userId');
    }
}
