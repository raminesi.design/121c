<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConsultationFile extends Model
{
    use HasFactory;
    protected $table = 'consultation_file';
    protected $fillable = ['consultationId' , 'userId' , 'title' , 'description' , 'file' , 'sender'];

    public function consultation()
    {
        return $this->belongsTo('App\Models\Consultation' , 'consultationId');
    }
}
