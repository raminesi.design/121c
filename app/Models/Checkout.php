<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    use HasFactory;
    protected $table = 'checkout';
    protected $fillable = ['userId' , 'userType' , 'amount' , 'file' , 'description' , 'date' , 'status' , 'bankId'];

    public function user()
    {
        return $this->belongsTo('App\Models\User' , 'userId');
    }

    public function bank()
    {
        return $this->belongsTo('App\Models\Banks' , 'bankId');
    }
}
