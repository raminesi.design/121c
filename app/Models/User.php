<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable , HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'avatar',
        'gender',
        'status',
        'mobile',
        'email',
        'password',
        'birthdate',
        'code',
        'country',
        'address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function wallet(){
        return $this->hasOne('App\Models\Wallet' , 'userId');
    }

    public function doctor(){
        return $this->hasOne('App\Models\Doctor' , 'userId');
    }

    public function orders(){
        return $this->hasMany('App\Models\Orders' , 'userId');
    }

    public function consultations(){
        return $this->hasMany('App\Models\Consultation' , 'userId');
    }

    public function parents(){
        return $this->hasMany('App\Models\ParentModel' , 'parentId');
    }

    public function checkout(){
        return $this->hasMany('App\Models\Checkout' , 'userId');
    }

    public function consultationFile(){
        return $this->hasMany('App\Models\ConsultationFile' , 'userId');
    }

    public function consultationText(){
        return $this->hasMany('App\Models\ConsultationText' , 'userId');
    }

    public function notifications(){
        return $this->hasMany('App\Models\Notifications' , 'userId');
    }

    public function tickets(){
        return $this->hasMany('App\Models\Ticket' , 'userId');
    }

    public function insurances(){
        return $this->hasMany('App\Models\Insurance' , 'userId');
    }

    public function banks(){
        return $this->hasMany('App\Models\Banks' , 'userId');
    }
}
