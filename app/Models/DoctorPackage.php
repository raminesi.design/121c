<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorPackage extends Model
{
    use HasFactory;
    protected $table = 'doctor_package';
    protected $fillable = ['package_id' , 'doctor_id'];

    public function package()
    {
        return $this->belongsTo('App\Models\Packages' , 'package_id');
    }
}
