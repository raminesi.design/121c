<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expertise extends Model
{
    use HasFactory;

    protected $table = 'expertise';
    protected $fillable = ['title' , 'image' , 'price_video_min', 'price_video_max' , 'price_voice_min' , 'price_voice_max' , 'price_text_min' , 'price_text_max' , 'description' , 'sort' , 'status' , 'percent'];

    function doctorExpertise(){
        return $this->hasMany('App\Models\DoctorExpertise' , 'expertiseId');
    }
}
