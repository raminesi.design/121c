<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorExpertise extends Model
{
    use HasFactory;

    protected $table = 'doctor_expertise';
    protected $fillable = ['doctorId' , 'expertiseId' , 'price_video' , 'price_voice' , 'price_text' , 'status'];

    public function expertise(){
        return $this->belongsTo('App\Models\Expertise' , 'expertiseId');
    }
}
