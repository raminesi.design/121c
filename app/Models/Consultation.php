<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
    use HasFactory;
    protected $table = 'consultation';
    protected $fillable = ['channel' , 'userId' , 'doctorId' , 'expertiseId' , 'type' , 'date' , 'status' , 'rate' , 'comment' , 'description' , 'sessions' , 'start' , 'end' , 'endUser' , 'parentId' , 'childeId' , 'report' , 'report_date' , 'visitStart' , 'visitEnd' , 'visitTime' , 'day_id'];

    public function order(){
        return $this->hasOne('App\Models\Orders' , 'reqId');
    }

    public function day(){
        return $this->belongsTo('App\Models\Days' , 'day_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Consultation' , 'parentId');
    }

    public function childe()
    {
        return $this->belongsTo('App\Models\Consultation' , 'childeId');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User' , 'userId');
    }

    public function doctor()
    {
        return $this->belongsTo('App\Models\Doctor' , 'doctorId');
    }

    public function expertise()
    {
        return $this->belongsTo('App\Models\Expertise' , 'expertiseId');
    }

    public function prescription(){
        return $this->hasOne('App\Models\Prescription' , 'consultationId');
    }
}
