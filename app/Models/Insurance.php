<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    use HasFactory;
    protected $table = 'insurance';
    protected $fillable = ['userId' , 'name' , 'code' , 'status'];

    public function user()
    {
        return $this->belongsTo('App\Models\User' , 'userId');
    }
}
