<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class DoctorExport implements FromQuery , WithHeadings , WithMapping
{
    use Exportable;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function query()
    {
        return DB::table('users')
            ->join('doctor' , 'doctor.userId' , '=' , 'users.id')
            ->orderBy('users.id' , 'asc')
            ->select('users.name' , 'users.email' , 'users.birthdate' , 'users.gender' , 'doctor.code');
    }
    public function headings(): array
    {
        return [
            'Name','Email','Birthdate','Gender','Code'
        ];
    }
    public function map($user): array
    {
        return [
            $user->name , $user->email , $user->birthdate , $user->gender , $user->code
        ];
    }
}
