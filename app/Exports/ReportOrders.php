<?php

namespace App\Exports;

use App\Models\Orders;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ReportOrders implements FromQuery , WithHeadings , WithMapping
{
    use Exportable;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function query()
    {
        $user = Auth::user();
        return Orders::whereHas('consultation' , function($query) use($user){
            $query->where('doctorId' , $user->doctor->id)->where('status' , 'done');
        })->with(['consultation'])->where('status' , 'successful')->orderBy('id' , 'desc');
    }
    public function headings(): array
    {
        return [
            'Order number','Amount ($)','Date'
        ];
    }
    public function map($item): array
    {
        return [
            $item->orderNumber,$item->payable_doctor,$item->consultation->date
        ];
    }
}
