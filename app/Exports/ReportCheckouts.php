<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ReportCheckouts implements FromQuery , WithHeadings , WithMapping
{
    use Exportable;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function query()
    {
        $user = Auth::user();
        return DB::table('checkout')->orderBy('id' , 'desc')->where('userId' , $user->id);
    }
    public function headings(): array
    {
        return [
            'Amount ($)','Creation date','Status','Date of payment','Description'
        ];
    }
    public function map($item): array
    {
        return [
            $item->amount , $item->created_at , $item->status , $item->date , $item->description
        ];
    }
}
