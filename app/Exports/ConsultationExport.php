<?php

namespace App\Exports;

use App\Models\Consultation;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ConsultationExport implements FromQuery , WithHeadings , WithMapping
{
    use Exportable;
    public function __construct($request)
    {
        $this->request = $request;
    }
    public function query()
    {
        $request = $this->request;
        $query = Consultation::with(['doctor' => function($query){
            $query->with(['user']);
        } ,'schedule' => function($query){
            $query->with(['day' , 'time']);
        } , 'user' , 'expertise'])->where('status' , '!=' , 'created')->orderBy('date' , 'desc');
        if(!empty($request->status)){
            $query->where('status' , $request->status);
        }
        if(isset($request->doctor)){
            $query->where('doctorId' , $request->doctor);
        }
        if(isset($request->user)){
            $query->where('userId' , $request->user);
        }
        if(isset($request->start)){
            $query->whereDate('created_at' , '>=' , $request->start);
        }
        if(isset($request->end)){
            $query->whereDate('created_at' , '<=' , $request->end);
        }
        return $query;
    }
    public function headings(): array
    {
        return [
            'User' , 'User code' ,'Day','Date','Time' , 'Doctor' , 'Doctor code' ,'Create date','Status'
        ];
    }
    public function map($order): array
    {
        return [
            $order->user->name , $order->user->code , $order->schedule->day->title , $order->date , $order->schedule->time->start.' - '.$order->schedule->time->end , $order->doctor->user->name , $order->doctor->code , $order->created_at , $order->status
        ];
    }
}
