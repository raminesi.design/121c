<?php

namespace App\Console\Commands;

use App\Models\Consultation;
use App\Models\Notifications;
use App\Traits\PusherTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;

class sendNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Consultation::with(['doctor' => function($query){
            $query->with(['user']);
        } , 'schedule' => function($query){
            $query->with(['time']);
        } , 'order' => function($query){
            $query->with(['user']);
        }])->whereDate('date' , '>=' , date('Y-m-d'))->whereIn('status' , ['pending' , 'accepted'])
        ->chunkById('50' , function ($consultations){
            foreach($consultations as $val){
                $date = Carbon::parse($val->date);
                $now  = Carbon::now();
                $diff = $date->diffInDays($now);
                $this->sendNotification($val , $diff);
            }
        });
    }

    function sendNotification($consultation , $diff){
        if($diff == 3){
            $description = 'Three days left until your consultation';
        }elseif($diff == 2){
            $description = 'Two days left until your consultation';
        }elseif($diff == 1){
            $description = 'One days left until your consultation';
        }elseif($diff == 0){
            $description = 'You have a consultation today at '.$consultation->schedule->time->start.':00';
        }

        $notif = array(
            [
                'userId' => $consultation->order->user->id,
                'consultationId' => $consultation->id,
                'title' => 'Advice reminder',
                'description' => $description,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'userId' => $consultation->doctor->user->id,
                'consultationId' => $consultation->id,
                'title' => 'Advice reminder',
                'description' => $description,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );
        Notifications::insert($notif);
        $data = array(
            'type' => 'notifications',
            'url' => route('profile_consultation_info').'?id='.$consultation->id
        );
        PusherTrait::send(md5($consultation->order->user->email.$consultation->order->user->id) , $notif[0]['title'] , $notif[0]['description'] , $data);
        //emailNotificationTrait::send($user->email , $user->name ,  $notif[0]['title'] , $notif[0]['description'] , $data['url']);
        $data = array(
            'type' => 'notifications',
            'url' => route('doctorProfile_consultation_info').'?id='.$consultation->id
        );
        PusherTrait::send(md5($consultation->doctor->user->email.$consultation->doctor->user->id) , $notif[1]['title'] , $notif[1]['description'] , $data);
        //emailNotificationTrait::send($doctor->email , $doctor->name ,  $notif[1]['title'] , $notif[1]['description'] , $data['url']);
    }
}
