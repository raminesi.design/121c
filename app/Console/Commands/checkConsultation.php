<?php

namespace App\Console\Commands;

use App\Models\Consultation;
use App\Models\Notifications;
use App\Models\Wallet;
use App\Traits\PusherTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;

class checkConsultation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:consultation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check consultation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Consultation::with(['doctor' => function($query){
            $query->with(['user']);
        } , 'order' => function($query){
            $query->with(['user']);
        }])->whereDate('date' , date('Y-m-d'))->whereIn('status' , ['pending' , 'accepted'])
        ->whereHas('schedule' , function($query){
            $query->whereHas('time' , function($query){
                $time = Carbon::now()->addMinutes(-10)->format('H');
                $query->where('start' , '<' , intval($time));
            });
        })->chunkById('50' , function ($consultations){
            $Ids = array();
            foreach($consultations as $val){
                array_push($Ids , $val->id);
                $this->sendNotification($val->id , $val->order->user , $val->doctor->user);
                $wallet = Wallet::where('userId' , $val->order->userId)->first();
                $wallet->credit = $wallet->credit + $val->order->payable_amount;
                $wallet->save();
            }
            Consultation::whereIn('id' , $Ids)->update(['status' => 'rejected']);
        });
    }
    function sendNotification($consultationId , $user , $doctor )
    {
        $notif = array(
            [
                'userId' => $user->id,
                'consultationId' => $consultationId,
                'title' => 'The request was rejected',
                'description' => 'Your request will be rejected by the system due to the expiration time and the fee will be refunded to your wallet',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'userId' => $doctor->id,
                'consultationId' => $consultationId,
                'title' => 'The request was rejected',
                'description' => 'This request was rejected by the system due to expiration',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );
        Notifications::insert($notif);
        $data = array(
            'type' => 'notifications',
            'url' => route('profile_consultation_info').'?id='.$consultationId
        );
        PusherTrait::send(md5($user->email.$user->id) , $notif[0]['title'] , $notif[0]['description'] , $data);
        //emailNotificationTrait::send($user->email , $user->name ,  $notif[0]['title'] , $notif[0]['description'] , $data['url']);
        $data = array(
            'type' => 'notifications',
            'url' => route('doctorProfile_consultation_info').'?id='.$consultationId
        );
        PusherTrait::send(md5($doctor->email.$doctor->id) , $notif[1]['title'] , $notif[1]['description'] , $data);
        //emailNotificationTrait::send($doctor->email , $doctor->name ,  $notif[1]['title'] , $notif[1]['description'] , $data['url']);
    }
}
